# Профиль пользователя е2е4 #

Актуальная версия по адресу:
http://testyoursite.hol.es/e2e4/profile.html

Git работает из корневой папки.

Gulp из папки ./Development
```
cd ./Development
gulp
```
Правда, надо будет инициализировать npm сперва, но package.json там же, в ./Development. Должен всё сам подтянуть.

После запуска gulp connect поднимает сервак по адресу http://localhost:2334/profile.html, где реализован livereload.
Верстай и смотри как страница сама перезагружается при каждом изменении файлов.

Всяческие файлы и ход выполнения здесь: https://docs.google.com/spreadsheets/d/1Dj_RqtxUu9UXGEQ_N3q5pXg7_h3ln2sBdmePLAiW84A/