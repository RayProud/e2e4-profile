(function() {
  var app = angular.module('profile', []);
  app.controller('menuTabsController', function() {
    this.currentTab = 0;
    this.getTab = function(clicked) {
      this.currentTab = clicked;
    };
    this.showTab = function(needed) {
      return this.currentTab === needed;
    }
  });
  app.controller('formController', function() {
    this.currentForm = 0;
    this.getForm = function(clicked) {
      this.currentForm = clicked;
    }
    this.showForm = function(needed) {
      return this.currentForm === needed;
    }
  });
  app.controller('questionController', function() {
    this.ansewers = false;
    this.writeAnswer = false;
    this.toggleAnswers = function() {
      this.ansewers = (this.ansewers === false ? true : false);
    };
    this.togglewriteAnswer = function() {
      this.writeAnswer = (this.writeAnswer === false ? true : false);
    };
  });
  app.controller('profileInputContriller', function() {
    this.shownInput = false;
    this.toggleAnswers = function() {
      this.shownInput = (this.shownInput === false ? true : false);
    };
  });

  app.controller('menuSlideToggle', function($scope) {
      this.isActive = false;
      this.classChange = function() {
        this.isActive = (this.isActive === false ? true : false);
      };
      $scope.toggle = function() {
          $scope.$broadcast('event:toggle');
      };
  });
  app.directive('toggle', function() {
      return function(scope, elem, attrs) {
          scope.$on('event:toggle', function() {
              elem.stop().slideToggle();
          });
      };
  });
})();

//Часть с jQuery. надо вынести, но пока так быстрее.

$(function() {
  var handlerTester = function() {
    if(window.innerWidth <= 915) {
      $('.profile-menu__list-mobile-wrap').on('click', '.profile-menu__list-item', slideUpFunc);
    } else {
      $('.profile-menu__list-mobile-wrap').off('click', '.profile-menu__list-item', slideUpFunc);
    }
  };

  var slideUpFunc = function() {
    $('.profile-menu__list-mobile-wrap').slideUp();
  };

  $(window).resize(function() {
    handlerTester();
  });

  handlerTester();

  $(document).on('click', '._like-btn, ._dislike-btn', function() {
    var parent = $(this).parent();
    if(parent.hasClass('_clicked')) {
      if($(this).find('.button').hasClass('like-clicked')) {
        $(this).find('.button')
               .removeClass('like-clicked')
               .addClass('grey')
               .closest(parent)
               .removeClass('_clicked');
      } else {
        return false;
      }
    } else {
      parent.addClass('_clicked');
      $(this).find('.button')
             .removeClass('grey')
             .addClass('like-clicked');
    }
  });
});















