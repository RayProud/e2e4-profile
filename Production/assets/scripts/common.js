function InfoBox(a) {
    a = a || {}, google.maps.OverlayView.apply(this, arguments), this.content_ = a.content || "", 
    this.disableAutoPan_ = a.disableAutoPan || !1, this.maxWidth_ = a.maxWidth || 0, 
    this.pixelOffset_ = a.pixelOffset || new google.maps.Size(0, 0), this.position_ = a.position || new google.maps.LatLng(0, 0), 
    this.zIndex_ = a.zIndex || null, this.boxClass_ = a.boxClass || "infoBox", this.boxStyle_ = a.boxStyle || {}, 
    this.closeBoxMargin_ = a.closeBoxMargin || "2px", this.closeBoxURL_ = a.closeBoxURL || "http://www.google.com/intl/en_us/mapfiles/close.gif", 
    "" === a.closeBoxURL && (this.closeBoxURL_ = ""), this.infoBoxClearance_ = a.infoBoxClearance || new google.maps.Size(1, 1), 
    this.isHidden_ = a.isHidden || !1, this.alignBottom_ = a.alignBottom || !1, this.pane_ = a.pane || "floatPane", 
    this.enableEventPropagation_ = a.enableEventPropagation || !1, this.div_ = null, 
    this.closeListener_ = null, this.eventListener1_ = null, this.eventListener2_ = null, 
    this.eventListener3_ = null, this.moveListener_ = null, this.contextListener_ = null, 
    this.fixedWidthSet_ = null;
}

(function() {
    var a, b, c, d, e, f = {}.hasOwnProperty, g = function(a, b) {
        function c() {
            this.constructor = a;
        }
        for (var d in b) f.call(b, d) && (a[d] = b[d]);
        return c.prototype = b.prototype, a.prototype = new c(), a.__super__ = b.prototype, 
        a;
    };
    d = function() {
        function a() {
            this.options_index = 0, this.parsed = [];
        }
        return a.prototype.add_node = function(a) {
            return "OPTGROUP" === a.nodeName.toUpperCase() ? this.add_group(a) : this.add_option(a);
        }, a.prototype.add_group = function(a) {
            var b, c, d, e, f, g;
            for (b = this.parsed.length, this.parsed.push({
                array_index: b,
                group: !0,
                label: this.escapeExpression(a.label),
                children: 0,
                disabled: a.disabled
            }), f = a.childNodes, g = [], d = 0, e = f.length; e > d; d++) c = f[d], g.push(this.add_option(c, b, a.disabled));
            return g;
        }, a.prototype.add_option = function(a, b, c) {
            return "OPTION" === a.nodeName.toUpperCase() ? ("" !== a.text ? (null != b && (this.parsed[b].children += 1), 
            this.parsed.push({
                array_index: this.parsed.length,
                options_index: this.options_index,
                value: a.value,
                text: a.text,
                html: a.innerHTML,
                selected: a.selected,
                disabled: c === !0 ? c : a.disabled,
                group_array_index: b,
                classes: a.className,
                style: a.style.cssText
            })) : this.parsed.push({
                array_index: this.parsed.length,
                options_index: this.options_index,
                empty: !0
            }), this.options_index += 1) : void 0;
        }, a.prototype.escapeExpression = function(a) {
            var b, c;
            return null == a || a === !1 ? "" : /[\&\<\>\"\'\`]/.test(a) ? (b = {
                "<": "&lt;",
                ">": "&gt;",
                '"': "&quot;",
                "'": "&#x27;",
                "`": "&#x60;"
            }, c = /&(?!\w+;)|[\<\>\"\'\`]/g, a.replace(c, function(a) {
                return b[a] || "&amp;";
            })) : a;
        }, a;
    }(), d.select_to_array = function(a) {
        var b, c, e, f, g;
        for (c = new d(), g = a.childNodes, e = 0, f = g.length; f > e; e++) b = g[e], c.add_node(b);
        return c.parsed;
    }, b = function() {
        function a(b, c) {
            this.form_field = b, this.options = null != c ? c : {}, a.browser_is_supported() && (this.is_multiple = this.form_field.multiple, 
            this.set_default_text(), this.set_default_values(), this.setup(), this.set_up_html(), 
            this.register_observers());
        }
        return a.prototype.set_default_values = function() {
            var a = this;
            return this.click_test_action = function(b) {
                return a.test_active_click(b);
            }, this.activate_action = function(b) {
                return a.activate_field(b);
            }, this.active_field = !1, this.mouse_on_container = !1, this.results_showing = !1, 
            this.result_highlighted = null, this.allow_single_deselect = null != this.options.allow_single_deselect && null != this.form_field.options[0] && "" === this.form_field.options[0].text ? this.options.allow_single_deselect : !1, 
            this.disable_search_threshold = this.options.disable_search_threshold || 0, this.disable_search = this.options.disable_search || !1, 
            this.enable_split_word_search = null != this.options.enable_split_word_search ? this.options.enable_split_word_search : !0, 
            this.group_search = null != this.options.group_search ? this.options.group_search : !0, 
            this.search_contains = this.options.search_contains || !1, this.single_backstroke_delete = null != this.options.single_backstroke_delete ? this.options.single_backstroke_delete : !0, 
            this.max_selected_options = this.options.max_selected_options || 1/0, this.inherit_select_classes = this.options.inherit_select_classes || !1, 
            this.display_selected_options = null != this.options.display_selected_options ? this.options.display_selected_options : !0, 
            this.display_disabled_options = null != this.options.display_disabled_options ? this.options.display_disabled_options : !0;
        }, a.prototype.set_default_text = function() {
            return this.default_text = this.form_field.getAttribute("data-placeholder") ? this.form_field.getAttribute("data-placeholder") : this.is_multiple ? this.options.placeholder_text_multiple || this.options.placeholder_text || a.default_multiple_text : this.options.placeholder_text_single || this.options.placeholder_text || a.default_single_text, 
            this.results_none_found = this.form_field.getAttribute("data-no_results_text") || this.options.no_results_text || a.default_no_result_text;
        }, a.prototype.mouse_enter = function() {
            return this.mouse_on_container = !0;
        }, a.prototype.mouse_leave = function() {
            return this.mouse_on_container = !1;
        }, a.prototype.input_focus = function() {
            var a = this;
            if (this.is_multiple) {
                if (!this.active_field) return setTimeout(function() {
                    return a.container_mousedown();
                }, 50);
            } else if (!this.active_field) return this.activate_field();
        }, a.prototype.input_blur = function() {
            var a = this;
            return this.mouse_on_container ? void 0 : (this.active_field = !1, setTimeout(function() {
                return a.blur_test();
            }, 100));
        }, a.prototype.results_option_build = function(a) {
            var b, c, d, e, f;
            for (b = "", f = this.results_data, d = 0, e = f.length; e > d; d++) c = f[d], b += c.group ? this.result_add_group(c) : this.result_add_option(c), 
            (null != a ? a.first : void 0) && (c.selected && this.is_multiple ? this.choice_build(c) : c.selected && !this.is_multiple && this.single_set_selected_text(c.text));
            return b;
        }, a.prototype.result_add_option = function(a) {
            var b, c;
            return a.search_match && this.include_option_in_results(a) ? (b = [], a.disabled || a.selected && this.is_multiple || b.push("active-result"), 
            !a.disabled || a.selected && this.is_multiple || b.push("disabled-result"), a.selected && b.push("result-selected"), 
            null != a.group_array_index && b.push("group-option"), "" !== a.classes && b.push(a.classes), 
            c = document.createElement("li"), c.className = b.join(" "), c.style.cssText = a.style, 
            c.setAttribute("data-option-array-index", a.array_index), c.innerHTML = a.search_text, 
            this.outerHTML(c)) : "";
        }, a.prototype.result_add_group = function(a) {
            var b;
            return (a.search_match || a.group_match) && a.active_options > 0 ? (b = document.createElement("li"), 
            b.className = "group-result", b.innerHTML = a.search_text, this.outerHTML(b)) : "";
        }, a.prototype.results_update_field = function() {
            return this.set_default_text(), this.is_multiple || this.results_reset_cleanup(), 
            this.result_clear_highlight(), this.results_build(), this.results_showing ? this.winnow_results() : void 0;
        }, a.prototype.reset_single_select_options = function() {
            var a, b, c, d, e;
            for (d = this.results_data, e = [], b = 0, c = d.length; c > b; b++) a = d[b], e.push(a.selected ? a.selected = !1 : void 0);
            return e;
        }, a.prototype.results_toggle = function() {
            return this.results_showing ? this.results_hide() : this.results_show();
        }, a.prototype.results_search = function() {
            return this.results_showing ? this.winnow_results() : this.results_show();
        }, a.prototype.winnow_results = function() {
            var a, b, c, d, e, f, g, h, i, j, k, l, m;
            for (this.no_results_clear(), e = 0, g = this.get_search_text(), a = g.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), 
            d = this.search_contains ? "" : "^", c = new RegExp(d + a, "i"), j = new RegExp(a, "i"), 
            m = this.results_data, k = 0, l = m.length; l > k; k++) b = m[k], b.search_match = !1, 
            f = null, this.include_option_in_results(b) && (b.group && (b.group_match = !1, 
            b.active_options = 0), null != b.group_array_index && this.results_data[b.group_array_index] && (f = this.results_data[b.group_array_index], 
            0 === f.active_options && f.search_match && (e += 1), f.active_options += 1), (!b.group || this.group_search) && (b.search_text = b.group ? b.label : b.html, 
            b.search_match = this.search_string_match(b.search_text, c), b.search_match && !b.group && (e += 1), 
            b.search_match ? (g.length && (h = b.search_text.search(j), i = b.search_text.substr(0, h + g.length) + "</em>" + b.search_text.substr(h + g.length), 
            b.search_text = i.substr(0, h) + "<em>" + i.substr(h)), null != f && (f.group_match = !0)) : null != b.group_array_index && this.results_data[b.group_array_index].search_match && (b.search_match = !0)));
            return this.result_clear_highlight(), 1 > e && g.length ? (this.update_results_content(""), 
            this.no_results(g)) : (this.update_results_content(this.results_option_build()), 
            this.winnow_results_set_highlight());
        }, a.prototype.search_string_match = function(a, b) {
            var c, d, e, f;
            if (b.test(a)) return !0;
            if (this.enable_split_word_search && (a.indexOf(" ") >= 0 || 0 === a.indexOf("[")) && (d = a.replace(/\[|\]/g, "").split(" "), 
            d.length)) for (e = 0, f = d.length; f > e; e++) if (c = d[e], b.test(c)) return !0;
        }, a.prototype.choices_count = function() {
            var a, b, c, d;
            if (null != this.selected_option_count) return this.selected_option_count;
            for (this.selected_option_count = 0, d = this.form_field.options, b = 0, c = d.length; c > b; b++) a = d[b], 
            a.selected && (this.selected_option_count += 1);
            return this.selected_option_count;
        }, a.prototype.choices_click = function(a) {
            return a.preventDefault(), this.results_showing || this.is_disabled ? void 0 : this.results_show();
        }, a.prototype.keyup_checker = function(a) {
            var b, c;
            switch (b = null != (c = a.which) ? c : a.keyCode, this.search_field_scale(), b) {
              case 8:
                if (this.is_multiple && this.backstroke_length < 1 && this.choices_count() > 0) return this.keydown_backstroke();
                if (!this.pending_backstroke) return this.result_clear_highlight(), this.results_search();
                break;

              case 13:
                if (a.preventDefault(), this.results_showing) return this.result_select(a);
                break;

              case 27:
                return this.results_showing && this.results_hide(), !0;

              case 9:
              case 38:
              case 40:
              case 16:
              case 91:
              case 17:
                break;

              default:
                return this.results_search();
            }
        }, a.prototype.clipboard_event_checker = function() {
            var a = this;
            return setTimeout(function() {
                return a.results_search();
            }, 50);
        }, a.prototype.container_width = function() {
            return null != this.options.width ? this.options.width : "" + this.form_field.offsetWidth + "px";
        }, a.prototype.include_option_in_results = function(a) {
            return this.is_multiple && !this.display_selected_options && a.selected ? !1 : !this.display_disabled_options && a.disabled ? !1 : a.empty ? !1 : !0;
        }, a.prototype.search_results_touchstart = function(a) {
            return this.touch_started = !0, this.search_results_mouseover(a);
        }, a.prototype.search_results_touchmove = function(a) {
            return this.touch_started = !1, this.search_results_mouseout(a);
        }, a.prototype.search_results_touchend = function(a) {
            return this.touch_started ? this.search_results_mouseup(a) : void 0;
        }, a.prototype.outerHTML = function(a) {
            var b;
            return a.outerHTML ? a.outerHTML : (b = document.createElement("div"), b.appendChild(a), 
            b.innerHTML);
        }, a.browser_is_supported = function() {
            return "Microsoft Internet Explorer" === window.navigator.appName ? document.documentMode >= 8 : /iP(od|hone)/i.test(window.navigator.userAgent) ? !1 : /Android/i.test(window.navigator.userAgent) && /Mobile/i.test(window.navigator.userAgent) ? !1 : !0;
        }, a.default_multiple_text = "Выберите", a.default_single_text = "Выберите", a.default_no_result_text = "Поиск не дал результатов", 
        a;
    }(), a = jQuery, a.fn.extend({
        chosen: function(d) {
            return b.browser_is_supported() ? this.each(function() {
                var b, e;
                b = a(this), e = b.data("chosen"), "destroy" === d && e ? e.destroy() : e || b.data("chosen", new c(this, d));
            }) : this;
        }
    }), c = function(b) {
        function c() {
            return e = c.__super__.constructor.apply(this, arguments);
        }
        return g(c, b), c.prototype.setup = function() {
            return this.form_field_jq = a(this.form_field), this.current_selectedIndex = this.form_field.selectedIndex, 
            this.is_rtl = this.form_field_jq.hasClass("chosen-rtl");
        }, c.prototype.set_up_html = function() {
            var b, c;
            return b = [ "chosen-container" ], b.push("chosen-container-" + (this.is_multiple ? "multi" : "single")), 
            this.inherit_select_classes && this.form_field.className && b.push(this.form_field.className), 
            this.is_rtl && b.push("chosen-rtl"), c = {
                "class": b.join(" "),
                style: "width: " + this.container_width() + ";",
                title: this.form_field.title
            }, this.form_field.id.length && (c.id = this.form_field.id.replace(/[^\w]/g, "_") + "_chosen"), 
            this.container = a("<div />", c), this.container.html(this.is_multiple ? '<ul class="chosen-choices"><li class="search-field"><input type="text" value="' + this.default_text + '" class="default" autocomplete="off" style="width:25px;" /></li></ul><div class="chosen-drop"><ul class="chosen-results"></ul></div>' : '<a class="chosen-single chosen-default" tabindex="-1"><span>' + this.default_text + '</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off" /></div><ul class="chosen-results"></ul></div>'), 
            this.form_field_jq.hide().after(this.container), this.dropdown = this.container.find("div.chosen-drop").first(), 
            this.search_field = this.container.find("input").first(), this.search_results = this.container.find("ul.chosen-results").first(), 
            this.search_field_scale(), this.search_no_results = this.container.find("li.no-results").first(), 
            this.is_multiple ? (this.search_choices = this.container.find("ul.chosen-choices").first(), 
            this.search_container = this.container.find("li.search-field").first()) : (this.search_container = this.container.find("div.chosen-search").first(), 
            this.selected_item = this.container.find(".chosen-single").first()), this.results_build(), 
            this.set_tab_index(), this.set_label_behavior(), this.form_field_jq.trigger("chosen:ready", {
                chosen: this
            });
        }, c.prototype.register_observers = function() {
            var a = this;
            return this.container.bind("mousedown.chosen", function(b) {
                a.container_mousedown(b);
            }), this.container.bind("mouseup.chosen", function(b) {
                a.container_mouseup(b);
            }), this.container.bind("mouseenter.chosen", function(b) {
                a.mouse_enter(b);
            }), this.container.bind("mouseleave.chosen", function(b) {
                a.mouse_leave(b);
            }), this.search_results.bind("mouseup.chosen", function(b) {
                a.search_results_mouseup(b);
            }), this.search_results.bind("mouseover.chosen", function(b) {
                a.search_results_mouseover(b);
            }), this.search_results.bind("mouseout.chosen", function(b) {
                a.search_results_mouseout(b);
            }), this.search_results.bind("mousewheel.chosen DOMMouseScroll.chosen", function(b) {
                a.search_results_mousewheel(b);
            }), this.search_results.bind("touchstart.chosen", function(b) {
                a.search_results_touchstart(b);
            }), this.search_results.bind("touchmove.chosen", function(b) {
                a.search_results_touchmove(b);
            }), this.search_results.bind("touchend.chosen", function(b) {
                a.search_results_touchend(b);
            }), this.form_field_jq.bind("chosen:updated.chosen", function(b) {
                a.results_update_field(b);
            }), this.form_field_jq.bind("chosen:activate.chosen", function(b) {
                a.activate_field(b);
            }), this.form_field_jq.bind("chosen:open.chosen", function(b) {
                a.container_mousedown(b);
            }), this.form_field_jq.bind("chosen:close.chosen", function(b) {
                a.input_blur(b);
            }), this.search_field.bind("blur.chosen", function(b) {
                a.input_blur(b);
            }), this.search_field.bind("keyup.chosen", function(b) {
                a.keyup_checker(b);
            }), this.search_field.bind("keydown.chosen", function(b) {
                a.keydown_checker(b);
            }), this.search_field.bind("focus.chosen", function(b) {
                a.input_focus(b);
            }), this.search_field.bind("cut.chosen", function(b) {
                a.clipboard_event_checker(b);
            }), this.search_field.bind("paste.chosen", function(b) {
                a.clipboard_event_checker(b);
            }), this.is_multiple ? this.search_choices.bind("click.chosen", function(b) {
                a.choices_click(b);
            }) : this.container.bind("click.chosen", function(a) {
                a.preventDefault();
            });
        }, c.prototype.destroy = function() {
            return a(this.container[0].ownerDocument).unbind("click.chosen", this.click_test_action), 
            this.search_field[0].tabIndex && (this.form_field_jq[0].tabIndex = this.search_field[0].tabIndex), 
            this.container.remove(), this.form_field_jq.removeData("chosen"), this.form_field_jq.show();
        }, c.prototype.search_field_disabled = function() {
            return this.is_disabled = this.form_field_jq[0].disabled, this.is_disabled ? (this.container.addClass("chosen-disabled"), 
            this.search_field[0].disabled = !0, this.is_multiple || this.selected_item.unbind("focus.chosen", this.activate_action), 
            this.close_field()) : (this.container.removeClass("chosen-disabled"), this.search_field[0].disabled = !1, 
            this.is_multiple ? void 0 : this.selected_item.bind("focus.chosen", this.activate_action));
        }, c.prototype.container_mousedown = function(b) {
            return this.is_disabled || (b && "mousedown" === b.type && !this.results_showing && b.preventDefault(), 
            null != b && a(b.target).hasClass("search-choice-close")) ? void 0 : (this.active_field ? this.is_multiple || !b || a(b.target)[0] !== this.selected_item[0] && !a(b.target).parents("a.chosen-single").length || (b.preventDefault(), 
            this.results_toggle()) : (this.is_multiple && this.search_field.val(""), a(this.container[0].ownerDocument).bind("click.chosen", this.click_test_action), 
            this.results_show()), this.activate_field());
        }, c.prototype.container_mouseup = function(a) {
            return "ABBR" !== a.target.nodeName || this.is_disabled ? void 0 : this.results_reset(a);
        }, c.prototype.search_results_mousewheel = function(a) {
            var b;
            return a.originalEvent && (b = -a.originalEvent.wheelDelta || a.originalEvent.detail), 
            null != b ? (a.preventDefault(), "DOMMouseScroll" === a.type && (b = 40 * b), this.search_results.scrollTop(b + this.search_results.scrollTop())) : void 0;
        }, c.prototype.blur_test = function() {
            return !this.active_field && this.container.hasClass("chosen-container-active") ? this.close_field() : void 0;
        }, c.prototype.close_field = function() {
            return a(this.container[0].ownerDocument).unbind("click.chosen", this.click_test_action), 
            this.active_field = !1, this.results_hide(), this.container.removeClass("chosen-container-active"), 
            this.clear_backstroke(), this.show_search_field_default(), this.search_field_scale();
        }, c.prototype.activate_field = function() {
            return this.container.addClass("chosen-container-active"), this.active_field = !0, 
            this.search_field.val(this.search_field.val()), this.search_field.focus();
        }, c.prototype.test_active_click = function(b) {
            var c;
            return c = a(b.target).closest(".chosen-container"), c.length && this.container[0] === c[0] ? this.active_field = !0 : this.close_field();
        }, c.prototype.results_build = function() {
            return this.parsing = !0, this.selected_option_count = null, this.results_data = d.select_to_array(this.form_field), 
            this.is_multiple ? this.search_choices.find("li.search-choice").remove() : this.is_multiple || (this.single_set_selected_text(), 
            this.disable_search || this.form_field.options.length <= this.disable_search_threshold ? (this.search_field[0].readOnly = !0, 
            this.container.addClass("chosen-container-single-nosearch")) : (this.search_field[0].readOnly = !1, 
            this.container.removeClass("chosen-container-single-nosearch"))), this.update_results_content(this.results_option_build({
                first: !0
            })), this.search_field_disabled(), this.show_search_field_default(), this.search_field_scale(), 
            this.parsing = !1;
        }, c.prototype.result_do_highlight = function(a) {
            var b, c, d, e, f;
            if (a.length) {
                if (this.result_clear_highlight(), this.result_highlight = a, this.result_highlight.addClass("highlighted"), 
                d = parseInt(this.search_results.css("maxHeight"), 10), f = this.search_results.scrollTop(), 
                e = d + f, c = this.result_highlight.position().top + this.search_results.scrollTop(), 
                b = c + this.result_highlight.outerHeight(), b >= e) return this.search_results.scrollTop(b - d > 0 ? b - d : 0);
                if (f > c) return this.search_results.scrollTop(c);
            }
        }, c.prototype.result_clear_highlight = function() {
            return this.result_highlight && this.result_highlight.removeClass("highlighted"), 
            this.result_highlight = null;
        }, c.prototype.results_show = function() {
            return this.is_multiple && this.max_selected_options <= this.choices_count() ? (this.form_field_jq.trigger("chosen:maxselected", {
                chosen: this
            }), !1) : (this.container.addClass("chosen-with-drop"), this.results_showing = !0, 
            this.search_field.focus(), this.search_field.val(this.search_field.val()), this.winnow_results(), 
            this.form_field_jq.trigger("chosen:showing_dropdown", {
                chosen: this
            }));
        }, c.prototype.update_results_content = function(a) {
            return this.search_results.html(a);
        }, c.prototype.results_hide = function() {
            return this.results_showing && (this.result_clear_highlight(), this.container.removeClass("chosen-with-drop"), 
            this.form_field_jq.trigger("chosen:hiding_dropdown", {
                chosen: this
            })), this.results_showing = !1;
        }, c.prototype.set_tab_index = function() {
            var a;
            return this.form_field.tabIndex ? (a = this.form_field.tabIndex, this.form_field.tabIndex = -1, 
            this.search_field[0].tabIndex = a) : void 0;
        }, c.prototype.set_label_behavior = function() {
            var b = this;
            return this.form_field_label = this.form_field_jq.parents("label"), !this.form_field_label.length && this.form_field.id.length && (this.form_field_label = a("label[for='" + this.form_field.id + "']")), 
            this.form_field_label.length > 0 ? this.form_field_label.bind("click.chosen", function(a) {
                return b.is_multiple ? b.container_mousedown(a) : b.activate_field();
            }) : void 0;
        }, c.prototype.show_search_field_default = function() {
            return this.is_multiple && this.choices_count() < 1 && !this.active_field ? (this.search_field.val(this.default_text), 
            this.search_field.addClass("default")) : (this.search_field.val(""), this.search_field.removeClass("default"));
        }, c.prototype.search_results_mouseup = function(b) {
            var c;
            return c = a(b.target).hasClass("active-result") ? a(b.target) : a(b.target).parents(".active-result").first(), 
            c.length ? (this.result_highlight = c, this.result_select(b), this.search_field.focus()) : void 0;
        }, c.prototype.search_results_mouseover = function(b) {
            var c;
            return c = a(b.target).hasClass("active-result") ? a(b.target) : a(b.target).parents(".active-result").first(), 
            c ? this.result_do_highlight(c) : void 0;
        }, c.prototype.search_results_mouseout = function(b) {
            return a(b.target).hasClass("active-result") ? this.result_clear_highlight() : void 0;
        }, c.prototype.choice_build = function(b) {
            var c, d, e = this;
            return c = a("<li />", {
                "class": "search-choice"
            }).html("<span>" + b.html + "</span>"), b.disabled ? c.addClass("search-choice-disabled") : (d = a("<a />", {
                "class": "search-choice-close",
                "data-option-array-index": b.array_index
            }), d.bind("click.chosen", function(a) {
                return e.choice_destroy_link_click(a);
            }), c.append(d)), this.search_container.before(c);
        }, c.prototype.choice_destroy_link_click = function(b) {
            return b.preventDefault(), b.stopPropagation(), this.is_disabled ? void 0 : this.choice_destroy(a(b.target));
        }, c.prototype.choice_destroy = function(a) {
            return this.result_deselect(a[0].getAttribute("data-option-array-index")) ? (this.show_search_field_default(), 
            this.is_multiple && this.choices_count() > 0 && this.search_field.val().length < 1 && this.results_hide(), 
            a.parents("li").first().remove(), this.search_field_scale()) : void 0;
        }, c.prototype.results_reset = function() {
            return this.reset_single_select_options(), this.form_field.options[0].selected = !0, 
            this.single_set_selected_text(), this.show_search_field_default(), this.results_reset_cleanup(), 
            this.form_field_jq.trigger("change"), this.active_field ? this.results_hide() : void 0;
        }, c.prototype.results_reset_cleanup = function() {
            return this.current_selectedIndex = this.form_field.selectedIndex, this.selected_item.find("abbr").remove();
        }, c.prototype.result_select = function(a) {
            var b, c;
            return this.result_highlight ? (b = this.result_highlight, this.result_clear_highlight(), 
            this.is_multiple && this.max_selected_options <= this.choices_count() ? (this.form_field_jq.trigger("chosen:maxselected", {
                chosen: this
            }), !1) : (this.is_multiple ? b.removeClass("active-result") : this.reset_single_select_options(), 
            c = this.results_data[b[0].getAttribute("data-option-array-index")], c.selected = !0, 
            this.form_field.options[c.options_index].selected = !0, this.selected_option_count = null, 
            this.is_multiple ? this.choice_build(c) : this.single_set_selected_text(c.text), 
            (a.metaKey || a.ctrlKey) && this.is_multiple || this.results_hide(), this.search_field.val(""), 
            (this.is_multiple || this.form_field.selectedIndex !== this.current_selectedIndex) && this.form_field_jq.trigger("change", {
                selected: this.form_field.options[c.options_index].value
            }), this.current_selectedIndex = this.form_field.selectedIndex, this.search_field_scale())) : void 0;
        }, c.prototype.single_set_selected_text = function(a) {
            return null == a && (a = this.default_text), a === this.default_text ? this.selected_item.addClass("chosen-default") : (this.single_deselect_control_build(), 
            this.selected_item.removeClass("chosen-default")), this.selected_item.find("span").text(a);
        }, c.prototype.result_deselect = function(a) {
            var b;
            return b = this.results_data[a], this.form_field.options[b.options_index].disabled ? !1 : (b.selected = !1, 
            this.form_field.options[b.options_index].selected = !1, this.selected_option_count = null, 
            this.result_clear_highlight(), this.results_showing && this.winnow_results(), this.form_field_jq.trigger("change", {
                deselected: this.form_field.options[b.options_index].value
            }), this.search_field_scale(), !0);
        }, c.prototype.single_deselect_control_build = function() {
            return this.allow_single_deselect ? (this.selected_item.find("abbr").length || this.selected_item.find("span").first().after('<abbr class="search-choice-close"></abbr>'), 
            this.selected_item.addClass("chosen-single-with-deselect")) : void 0;
        }, c.prototype.get_search_text = function() {
            return this.search_field.val() === this.default_text ? "" : a("<div/>").text(a.trim(this.search_field.val())).html();
        }, c.prototype.winnow_results_set_highlight = function() {
            var a, b;
            return b = this.is_multiple ? [] : this.search_results.find(".result-selected.active-result"), 
            a = b.length ? b.first() : this.search_results.find(".active-result").first(), null != a ? this.result_do_highlight(a) : void 0;
        }, c.prototype.no_results = function() {
            var b;
            return b = a('<li class="no-results">' + this.results_none_found + ' "<span></span>"</li>'), 
            this.search_results.append(b), this.form_field_jq.trigger("chosen:no_results", {
                chosen: this
            });
        }, c.prototype.no_results_clear = function() {
            return this.search_results.find(".no-results").remove();
        }, c.prototype.keydown_arrow = function() {
            var a;
            return this.results_showing && this.result_highlight ? (a = this.result_highlight.nextAll("li.active-result").first()) ? this.result_do_highlight(a) : void 0 : this.results_show();
        }, c.prototype.keyup_arrow = function() {
            var a;
            return this.results_showing || this.is_multiple ? this.result_highlight ? (a = this.result_highlight.prevAll("li.active-result"), 
            a.length ? this.result_do_highlight(a.first()) : (this.choices_count() > 0 && this.results_hide(), 
            this.result_clear_highlight())) : void 0 : this.results_show();
        }, c.prototype.keydown_backstroke = function() {
            var a;
            return this.pending_backstroke ? (this.choice_destroy(this.pending_backstroke.find("a").first()), 
            this.clear_backstroke()) : (a = this.search_container.siblings("li.search-choice").last(), 
            a.length && !a.hasClass("search-choice-disabled") ? (this.pending_backstroke = a, 
            this.single_backstroke_delete ? this.keydown_backstroke() : this.pending_backstroke.addClass("search-choice-focus")) : void 0);
        }, c.prototype.clear_backstroke = function() {
            return this.pending_backstroke && this.pending_backstroke.removeClass("search-choice-focus"), 
            this.pending_backstroke = null;
        }, c.prototype.keydown_checker = function(a) {
            var b, c;
            switch (b = null != (c = a.which) ? c : a.keyCode, this.search_field_scale(), 8 !== b && this.pending_backstroke && this.clear_backstroke(), 
            b) {
              case 8:
                this.backstroke_length = this.search_field.val().length;
                break;

              case 9:
                this.results_showing && !this.is_multiple && this.result_select(a), this.mouse_on_container = !1;
                break;

              case 13:
                a.preventDefault();
                break;

              case 38:
                a.preventDefault(), this.keyup_arrow();
                break;

              case 40:
                a.preventDefault(), this.keydown_arrow();
            }
        }, c.prototype.search_field_scale = function() {
            var b, c, d, e, f, g, h, i, j;
            if (this.is_multiple) {
                for (d = 0, h = 0, f = "position:absolute; left: -1000px; top: -1000px; display:none;", 
                g = [ "font-size", "font-style", "font-weight", "font-family", "line-height", "text-transform", "letter-spacing" ], 
                i = 0, j = g.length; j > i; i++) e = g[i], f += e + ":" + this.search_field.css(e) + ";";
                return b = a("<div />", {
                    style: f
                }), b.text(this.search_field.val()), a("body").append(b), h = b.width() + 25, b.remove(), 
                c = this.container.outerWidth(), h > c - 10 && (h = c - 10), this.search_field.css({
                    width: h + "px"
                });
            }
        }, c;
    }(b);
}).call(this), function() {
    var a = !1;
    window.JQClass = function() {}, JQClass.classes = {}, JQClass.extend = function b(c) {
        function d() {
            !a && this._init && this._init.apply(this, arguments);
        }
        var e = this.prototype;
        a = !0;
        var f = new this();
        a = !1;
        for (var g in c) f[g] = "function" == typeof c[g] && "function" == typeof e[g] ? function(a, b) {
            return function() {
                var c = this._super;
                this._super = function(b) {
                    return e[a].apply(this, b || []);
                };
                var d = b.apply(this, arguments);
                return this._super = c, d;
            };
        }(g, c[g]) : c[g];
        return d.prototype = f, d.prototype.constructor = d, d.extend = b, d;
    };
}(), function($) {
    function camelCase(a) {
        return a.replace(/-([a-z])/g, function(a, b) {
            return b.toUpperCase();
        });
    }
    JQClass.classes.JQPlugin = JQClass.extend({
        name: "plugin",
        defaultOptions: {},
        regionalOptions: {},
        _getters: [],
        _getMarker: function() {
            return "is-" + this.name;
        },
        _init: function() {
            $.extend(this.defaultOptions, this.regionalOptions && this.regionalOptions[""] || {});
            var a = camelCase(this.name);
            $[a] = this, $.fn[a] = function(b) {
                var c = Array.prototype.slice.call(arguments, 1);
                return $[a]._isNotChained(b, c) ? $[a][b].apply($[a], [ this[0] ].concat(c)) : this.each(function() {
                    if ("string" == typeof b) {
                        if ("_" === b[0] || !$[a][b]) throw "Unknown method: " + b;
                        $[a][b].apply($[a], [ this ].concat(c));
                    } else $[a]._attach(this, b);
                });
            };
        },
        setDefaults: function(a) {
            $.extend(this.defaultOptions, a || {});
        },
        _isNotChained: function(a, b) {
            return "option" === a && (0 === b.length || 1 === b.length && "string" == typeof b[0]) ? !0 : $.inArray(a, this._getters) > -1;
        },
        _attach: function(a, b) {
            if (a = $(a), !a.hasClass(this._getMarker())) {
                a.addClass(this._getMarker()), b = $.extend({}, this.defaultOptions, this._getMetadata(a), b || {});
                var c = $.extend({
                    name: this.name,
                    elem: a,
                    options: b
                }, this._instSettings(a, b));
                a.data(this.name, c), this._postAttach(a, c), this.option(a, b);
            }
        },
        _instSettings: function() {
            return {};
        },
        _postAttach: function() {},
        _getMetadata: function(elem) {
            try {
                var data = elem.data(this.name.toLowerCase()) || "";
                data = data.replace(/'/g, '"'), data = data.replace(/([a-zA-Z0-9]+):/g, function(a, b, c) {
                    var d = data.substring(0, c).match(/"/g);
                    return d && d.length % 2 !== 0 ? b + ":" : '"' + b + '":';
                }), data = $.parseJSON("{" + data + "}");
                for (var name in data) {
                    var value = data[name];
                    "string" == typeof value && value.match(/^new Date\((.*)\)$/) && (data[name] = eval(value));
                }
                return data;
            } catch (e) {
                return {};
            }
        },
        _getInst: function(a) {
            return $(a).data(this.name) || {};
        },
        option: function(a, b, c) {
            a = $(a);
            var d = a.data(this.name);
            if (!b || "string" == typeof b && null == c) {
                var e = (d || {}).options;
                return e && b ? e[b] : e;
            }
            if (a.hasClass(this._getMarker())) {
                var e = b || {};
                "string" == typeof b && (e = {}, e[b] = c), this._optionsChanged(a, d, e), $.extend(d.options, e);
            }
        },
        _optionsChanged: function() {},
        destroy: function(a) {
            a = $(a), a.hasClass(this._getMarker()) && (this._preDestroy(a, this._getInst(a)), 
            a.removeData(this.name).removeClass(this._getMarker()));
        },
        _preDestroy: function() {}
    }), $.JQPlugin = {
        createPlugin: function(a, b) {
            "object" == typeof a && (b = a, a = "JQPlugin"), a = camelCase(a);
            var c = camelCase(b.name);
            JQClass.classes[c] = JQClass.classes[a].extend(b), new JQClass.classes[c]();
        }
    };
}(jQuery), function(a) {
    var b = "countdown", c = 0, d = 1, e = 2, f = 3, g = 4, h = 5, i = 6;
    a.JQPlugin.createPlugin({
        name: b,
        defaultOptions: {
            until: null,
            since: null,
            timezone: null,
            serverSync: null,
            format: "dHMS",
            layout: "",
            compact: !1,
            padZeroes: !1,
            significant: 0,
            description: "",
            expiryUrl: "",
            expiryText: "",
            alwaysExpire: !1,
            onExpiry: null,
            onTick: null,
            tickInterval: 1
        },
        regionalOptions: {
            "": {
                labels: [ "Years", "Months", "Weeks", "Days", "Hours", "Minutes", "Seconds" ],
                labels1: [ "Year", "Month", "Week", "Day", "Hour", "Minute", "Second" ],
                compactLabels: [ "y", "m", "w", "d" ],
                whichLabels: null,
                digits: [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" ],
                timeSeparator: ":",
                isRTL: !1
            }
        },
        _getters: [ "getTimes" ],
        _rtlClass: b + "-rtl",
        _sectionClass: b + "-section",
        _amountClass: b + "-amount",
        _periodClass: b + "-period",
        _rowClass: b + "-row",
        _holdingClass: b + "-holding",
        _showClass: b + "-show",
        _descrClass: b + "-descr",
        _timerElems: [],
        _init: function() {
            function b(a) {
                var h = 1e12 > a ? e ? performance.now() + performance.timing.navigationStart : d() : a || d();
                h - g >= 1e3 && (c._updateElems(), g = h), f(b);
            }
            var c = this;
            this._super(), this._serverSyncs = [];
            var d = "function" == typeof Date.now ? Date.now : function() {
                return new Date().getTime();
            }, e = window.performance && "function" == typeof window.performance.now, f = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || null, g = 0;
            !f || a.noRequestAnimationFrame ? (a.noRequestAnimationFrame = null, setInterval(function() {
                c._updateElems();
            }, 980)) : (g = window.animationStartTime || window.webkitAnimationStartTime || window.mozAnimationStartTime || window.oAnimationStartTime || window.msAnimationStartTime || d(), 
            f(b));
        },
        UTCDate: function(a, b, c, d, e, f, g, h) {
            "object" == typeof b && b.constructor == Date && (h = b.getMilliseconds(), g = b.getSeconds(), 
            f = b.getMinutes(), e = b.getHours(), d = b.getDate(), c = b.getMonth(), b = b.getFullYear());
            var i = new Date();
            return i.setUTCFullYear(b), i.setUTCDate(1), i.setUTCMonth(c || 0), i.setUTCDate(d || 1), 
            i.setUTCHours(e || 0), i.setUTCMinutes((f || 0) - (Math.abs(a) < 30 ? 60 * a : a)), 
            i.setUTCSeconds(g || 0), i.setUTCMilliseconds(h || 0), i;
        },
        periodsToSeconds: function(a) {
            return 31557600 * a[0] + 2629800 * a[1] + 604800 * a[2] + 86400 * a[3] + 3600 * a[4] + 60 * a[5] + a[6];
        },
        _instSettings: function() {
            return {
                _periods: [ 0, 0, 0, 0, 0, 0, 0 ]
            };
        },
        _addElem: function(a) {
            this._hasElem(a) || this._timerElems.push(a);
        },
        _hasElem: function(b) {
            return a.inArray(b, this._timerElems) > -1;
        },
        _removeElem: function(b) {
            this._timerElems = a.map(this._timerElems, function(a) {
                return a == b ? null : a;
            });
        },
        _updateElems: function() {
            for (var a = this._timerElems.length - 1; a >= 0; a--) this._updateCountdown(this._timerElems[a]);
        },
        _optionsChanged: function(b, c, d) {
            d.layout && (d.layout = d.layout.replace(/&lt;/g, "<").replace(/&gt;/g, ">")), this._resetExtraLabels(c.options, d);
            var e = c.options.timezone != d.timezone;
            a.extend(c.options, d), this._adjustSettings(b, c, null != d.until || null != d.since || e);
            var f = new Date();
            (c._since && c._since < f || c._until && c._until > f) && this._addElem(b[0]), this._updateCountdown(b, c);
        },
        _updateCountdown: function(b, c) {
            if (b = b.jquery ? b : a(b), c = c || this._getInst(b)) {
                if (b.html(this._generateHTML(c)).toggleClass(this._rtlClass, c.options.isRTL), 
                a.isFunction(c.options.onTick)) {
                    var d = "lap" != c._hold ? c._periods : this._calculatePeriods(c, c._show, c.options.significant, new Date());
                    (1 == c.options.tickInterval || this.periodsToSeconds(d) % c.options.tickInterval == 0) && c.options.onTick.apply(b[0], [ d ]);
                }
                var e = "pause" != c._hold && (c._since ? c._now.getTime() < c._since.getTime() : c._now.getTime() >= c._until.getTime());
                if (e && !c._expiring) {
                    if (c._expiring = !0, this._hasElem(b[0]) || c.options.alwaysExpire) {
                        if (this._removeElem(b[0]), a.isFunction(c.options.onExpiry) && c.options.onExpiry.apply(b[0], []), 
                        c.options.expiryText) {
                            var f = c.options.layout;
                            c.options.layout = c.options.expiryText, this._updateCountdown(b[0], c), c.options.layout = f;
                        }
                        c.options.expiryUrl && (window.location = c.options.expiryUrl);
                    }
                    c._expiring = !1;
                } else "pause" == c._hold && this._removeElem(b[0]);
            }
        },
        _resetExtraLabels: function(a, b) {
            for (var c in b) c.match(/[Ll]abels[02-9]|compactLabels1/) && (a[c] = b[c]);
            for (var c in a) c.match(/[Ll]abels[02-9]|compactLabels1/) && "undefined" == typeof b[c] && (a[c] = null);
        },
        _adjustSettings: function(b, c, d) {
            for (var e, f = 0, g = null, h = 0; h < this._serverSyncs.length; h++) if (this._serverSyncs[h][0] == c.options.serverSync) {
                g = this._serverSyncs[h][1];
                break;
            }
            if (null != g) f = c.options.serverSync ? g : 0, e = new Date(); else {
                var i = a.isFunction(c.options.serverSync) ? c.options.serverSync.apply(b[0], []) : null;
                e = new Date(), f = i ? e.getTime() - i.getTime() : 0, this._serverSyncs.push([ c.options.serverSync, f ]);
            }
            var j = c.options.timezone;
            j = null == j ? -e.getTimezoneOffset() : j, (d || !d && null == c._until && null == c._since) && (c._since = c.options.since, 
            null != c._since && (c._since = this.UTCDate(j, this._determineTime(c._since, null)), 
            c._since && f && c._since.setMilliseconds(c._since.getMilliseconds() + f)), c._until = this.UTCDate(j, this._determineTime(c.options.until, e)), 
            f && c._until.setMilliseconds(c._until.getMilliseconds() + f)), c._show = this._determineShow(c);
        },
        _preDestroy: function(a) {
            this._removeElem(a[0]), a.empty();
        },
        pause: function(a) {
            this._hold(a, "pause");
        },
        lap: function(a) {
            this._hold(a, "lap");
        },
        resume: function(a) {
            this._hold(a, null);
        },
        toggle: function(b) {
            var c = a.data(b, this.name) || {};
            this[c._hold ? "resume" : "pause"](b);
        },
        toggleLap: function(b) {
            var c = a.data(b, this.name) || {};
            this[c._hold ? "resume" : "lap"](b);
        },
        _hold: function(b, c) {
            var d = a.data(b, this.name);
            if (d) {
                if ("pause" == d._hold && !c) {
                    d._periods = d._savePeriods;
                    var e = d._since ? "-" : "+";
                    d[d._since ? "_since" : "_until"] = this._determineTime(e + d._periods[0] + "y" + e + d._periods[1] + "o" + e + d._periods[2] + "w" + e + d._periods[3] + "d" + e + d._periods[4] + "h" + e + d._periods[5] + "m" + e + d._periods[6] + "s"), 
                    this._addElem(b);
                }
                d._hold = c, d._savePeriods = "pause" == c ? d._periods : null, a.data(b, this.name, d), 
                this._updateCountdown(b, d);
            }
        },
        getTimes: function(b) {
            var c = a.data(b, this.name);
            return c ? "pause" == c._hold ? c._savePeriods : c._hold ? this._calculatePeriods(c, c._show, c.options.significant, new Date()) : c._periods : null;
        },
        _determineTime: function(a, b) {
            var c = this, d = function(a) {
                var b = new Date();
                return b.setTime(b.getTime() + 1e3 * a), b;
            }, e = function(a) {
                a = a.toLowerCase();
                for (var b = new Date(), d = b.getFullYear(), e = b.getMonth(), f = b.getDate(), g = b.getHours(), h = b.getMinutes(), i = b.getSeconds(), j = /([+-]?[0-9]+)\s*(s|m|h|d|w|o|y)?/g, k = j.exec(a); k; ) {
                    switch (k[2] || "s") {
                      case "s":
                        i += parseInt(k[1], 10);
                        break;

                      case "m":
                        h += parseInt(k[1], 10);
                        break;

                      case "h":
                        g += parseInt(k[1], 10);
                        break;

                      case "d":
                        f += parseInt(k[1], 10);
                        break;

                      case "w":
                        f += 7 * parseInt(k[1], 10);
                        break;

                      case "o":
                        e += parseInt(k[1], 10), f = Math.min(f, c._getDaysInMonth(d, e));
                        break;

                      case "y":
                        d += parseInt(k[1], 10), f = Math.min(f, c._getDaysInMonth(d, e));
                    }
                    k = j.exec(a);
                }
                return new Date(d, e, f, g, h, i, 0);
            }, f = null == a ? b : "string" == typeof a ? e(a) : "number" == typeof a ? d(a) : a;
            return f && f.setMilliseconds(0), f;
        },
        _getDaysInMonth: function(a, b) {
            return 32 - new Date(a, b, 32).getDate();
        },
        _normalLabels: function(a) {
            return a;
        },
        _generateHTML: function(b) {
            var j = this;
            b._periods = b._hold ? b._periods : this._calculatePeriods(b, b._show, b.options.significant, new Date());
            for (var k = !1, l = 0, m = b.options.significant, n = a.extend({}, b._show), o = c; i >= o; o++) k |= "?" == b._show[o] && b._periods[o] > 0, 
            n[o] = "?" != b._show[o] || k ? b._show[o] : null, l += n[o] ? 1 : 0, m -= b._periods[o] > 0 ? 1 : 0;
            for (var p = [ !1, !1, !1, !1, !1, !1, !1 ], o = i; o >= c; o--) b._show[o] && (b._periods[o] ? p[o] = !0 : (p[o] = m > 0, 
            m--));
            var q = b.options.compact ? b.options.compactLabels : b.options.labels, r = b.options.whichLabels || this._normalLabels, s = function(a) {
                var c = b.options["compactLabels" + r(b._periods[a])];
                return n[a] ? j._translateDigits(b, b._periods[a]) + (c ? c[a] : q[a]) + " " : "";
            }, t = b.options.padZeroes ? 2 : 1, u = function(a) {
                var c = b.options["labels" + r(b._periods[a])];
                return !b.options.significant && n[a] || b.options.significant && p[a] ? '<span class="' + j._sectionClass + '"><span class="' + j._amountClass + '">' + j._minDigits(b, b._periods[a], t) + '</span><span class="' + j._periodClass + '">' + (c ? c[a] : q[a]) + "</span></span>" : "";
            };
            return b.options.layout ? this._buildLayout(b, n, b.options.layout, b.options.compact, b.options.significant, p) : (b.options.compact ? '<span class="' + this._rowClass + " " + this._amountClass + (b._hold ? " " + this._holdingClass : "") + '">' + s(c) + s(d) + s(e) + s(f) + (n[g] ? this._minDigits(b, b._periods[g], 2) : "") + (n[h] ? (n[g] ? b.options.timeSeparator : "") + this._minDigits(b, b._periods[h], 2) : "") + (n[i] ? (n[g] || n[h] ? b.options.timeSeparator : "") + this._minDigits(b, b._periods[i], 2) : "") : '<span class="' + this._rowClass + " " + this._showClass + (b.options.significant || l) + (b._hold ? " " + this._holdingClass : "") + '">' + u(c) + u(d) + u(e) + u(f) + u(g) + u(h) + u(i)) + "</span>" + (b.options.description ? '<span class="' + this._rowClass + " " + this._descrClass + '">' + b.options.description + "</span>" : "");
        },
        _buildLayout: function(b, j, k, l, m, n) {
            for (var o = b.options[l ? "compactLabels" : "labels"], p = b.options.whichLabels || this._normalLabels, q = function(a) {
                return (b.options[(l ? "compactLabels" : "labels") + p(b._periods[a])] || o)[a];
            }, r = function(a, c) {
                return b.options.digits[Math.floor(a / c) % 10];
            }, s = {
                desc: b.options.description,
                sep: b.options.timeSeparator,
                yl: q(c),
                yn: this._minDigits(b, b._periods[c], 1),
                ynn: this._minDigits(b, b._periods[c], 2),
                ynnn: this._minDigits(b, b._periods[c], 3),
                y1: r(b._periods[c], 1),
                y10: r(b._periods[c], 10),
                y100: r(b._periods[c], 100),
                y1000: r(b._periods[c], 1e3),
                ol: q(d),
                on: this._minDigits(b, b._periods[d], 1),
                onn: this._minDigits(b, b._periods[d], 2),
                onnn: this._minDigits(b, b._periods[d], 3),
                o1: r(b._periods[d], 1),
                o10: r(b._periods[d], 10),
                o100: r(b._periods[d], 100),
                o1000: r(b._periods[d], 1e3),
                wl: q(e),
                wn: this._minDigits(b, b._periods[e], 1),
                wnn: this._minDigits(b, b._periods[e], 2),
                wnnn: this._minDigits(b, b._periods[e], 3),
                w1: r(b._periods[e], 1),
                w10: r(b._periods[e], 10),
                w100: r(b._periods[e], 100),
                w1000: r(b._periods[e], 1e3),
                dl: q(f),
                dn: this._minDigits(b, b._periods[f], 1),
                dnn: this._minDigits(b, b._periods[f], 2),
                dnnn: this._minDigits(b, b._periods[f], 3),
                d1: r(b._periods[f], 1),
                d10: r(b._periods[f], 10),
                d100: r(b._periods[f], 100),
                d1000: r(b._periods[f], 1e3),
                hl: q(g),
                hn: this._minDigits(b, b._periods[g], 1),
                hnn: this._minDigits(b, b._periods[g], 2),
                hnnn: this._minDigits(b, b._periods[g], 3),
                h1: r(b._periods[g], 1),
                h10: r(b._periods[g], 10),
                h100: r(b._periods[g], 100),
                h1000: r(b._periods[g], 1e3),
                ml: q(h),
                mn: this._minDigits(b, b._periods[h], 1),
                mnn: this._minDigits(b, b._periods[h], 2),
                mnnn: this._minDigits(b, b._periods[h], 3),
                m1: r(b._periods[h], 1),
                m10: r(b._periods[h], 10),
                m100: r(b._periods[h], 100),
                m1000: r(b._periods[h], 1e3),
                sl: q(i),
                sn: this._minDigits(b, b._periods[i], 1),
                snn: this._minDigits(b, b._periods[i], 2),
                snnn: this._minDigits(b, b._periods[i], 3),
                s1: r(b._periods[i], 1),
                s10: r(b._periods[i], 10),
                s100: r(b._periods[i], 100),
                s1000: r(b._periods[i], 1e3)
            }, t = k, u = c; i >= u; u++) {
                var v = "yowdhms".charAt(u), w = new RegExp("\\{" + v + "<\\}([\\s\\S]*)\\{" + v + ">\\}", "g");
                t = t.replace(w, !m && j[u] || m && n[u] ? "$1" : "");
            }
            return a.each(s, function(a, b) {
                var c = new RegExp("\\{" + a + "\\}", "g");
                t = t.replace(c, b);
            }), t;
        },
        _minDigits: function(a, b, c) {
            return b = "" + b, b.length >= c ? this._translateDigits(a, b) : (b = "0000000000" + b, 
            this._translateDigits(a, b.substr(b.length - c)));
        },
        _translateDigits: function(a, b) {
            return ("" + b).replace(/[0-9]/g, function(b) {
                return a.options.digits[b];
            });
        },
        _determineShow: function(a) {
            var b = a.options.format, j = [];
            return j[c] = b.match("y") ? "?" : b.match("Y") ? "!" : null, j[d] = b.match("o") ? "?" : b.match("O") ? "!" : null, 
            j[e] = b.match("w") ? "?" : b.match("W") ? "!" : null, j[f] = b.match("d") ? "?" : b.match("D") ? "!" : null, 
            j[g] = b.match("h") ? "?" : b.match("H") ? "!" : null, j[h] = b.match("m") ? "?" : b.match("M") ? "!" : null, 
            j[i] = b.match("s") ? "?" : b.match("S") ? "!" : null, j;
        },
        _calculatePeriods: function(a, b, j, k) {
            a._now = k, a._now.setMilliseconds(0);
            var l = new Date(a._now.getTime());
            a._since ? k.getTime() < a._since.getTime() ? a._now = k = l : k = a._since : (l.setTime(a._until.getTime()), 
            k.getTime() > a._until.getTime() && (a._now = k = l));
            var m = [ 0, 0, 0, 0, 0, 0, 0 ];
            if (b[c] || b[d]) {
                var n = this._getDaysInMonth(k.getFullYear(), k.getMonth()), o = this._getDaysInMonth(l.getFullYear(), l.getMonth()), p = l.getDate() == k.getDate() || l.getDate() >= Math.min(n, o) && k.getDate() >= Math.min(n, o), q = function(a) {
                    return 60 * (60 * a.getHours() + a.getMinutes()) + a.getSeconds();
                }, r = Math.max(0, 12 * (l.getFullYear() - k.getFullYear()) + l.getMonth() - k.getMonth() + (l.getDate() < k.getDate() && !p || p && q(l) < q(k) ? -1 : 0));
                m[c] = b[c] ? Math.floor(r / 12) : 0, m[d] = b[d] ? r - 12 * m[c] : 0, k = new Date(k.getTime());
                var s = k.getDate() == n, t = this._getDaysInMonth(k.getFullYear() + m[c], k.getMonth() + m[d]);
                k.getDate() > t && k.setDate(t), k.setFullYear(k.getFullYear() + m[c]), k.setMonth(k.getMonth() + m[d]), 
                s && k.setDate(t);
            }
            var u = Math.floor((l.getTime() - k.getTime()) / 1e3), v = function(a, c) {
                m[a] = b[a] ? Math.floor(u / c) : 0, u -= m[a] * c;
            };
            if (v(e, 604800), v(f, 86400), v(g, 3600), v(h, 60), v(i, 1), u > 0 && !a._since) for (var w = [ 1, 12, 4.3482, 7, 24, 60, 60 ], x = i, y = 1, z = i; z >= c; z--) b[z] && (m[x] >= y && (m[x] = 0, 
            u = 1), u > 0 && (m[z]++, u = 0, x = z, y = 1)), y *= w[z];
            if (j) for (var z = c; i >= z; z++) j && m[z] ? j-- : j || (m[z] = 0);
            return m;
        }
    });
}(jQuery), !function(a, b, c, d, e) {
    "use strict";
    function f(a) {
        var b = "bez_" + d.makeArray(arguments).join("_").replace(".", "p");
        if ("function" != typeof d.easing[b]) {
            var c = function(a, b) {
                var c = [ null, null ], d = [ null, null ], e = [ null, null ], f = function(f, g) {
                    return e[g] = 3 * a[g], d[g] = 3 * (b[g] - a[g]) - e[g], c[g] = 1 - e[g] - d[g], 
                    f * (e[g] + f * (d[g] + f * c[g]));
                }, g = function(a) {
                    return e[0] + a * (2 * d[0] + 3 * c[0] * a);
                }, h = function(a) {
                    for (var b, c = a, d = 0; ++d < 14 && (b = f(c, 0) - a, !(Math.abs(b) < .001)); ) c -= b / g(c);
                    return c;
                };
                return function(a) {
                    return f(h(a), 1);
                };
            };
            d.easing[b] = function(b, d, e, f, g) {
                return f * c([ a[0], a[1] ], [ a[2], a[3] ])(d / g) + e;
            };
        }
        return b;
    }
    function g() {}
    function h(a, b, c) {
        return Math.max(isNaN(b) ? -1 / 0 : b, Math.min(isNaN(c) ? 1 / 0 : c, a));
    }
    function i(a) {
        return a.match(/ma/) && a.match(/-?\d+(?!d)/g)[a.match(/3d/) ? 12 : 4];
    }
    function j(a) {
        return Kc ? +i(a.css("transform")) : +a.css("left").replace("px", "");
    }
    function k(a) {
        var b = {};
        return Kc ? b.transform = "translate3d(" + a + "px,0,0)" : b.left = a, b;
    }
    function l(a) {
        return {
            "transition-duration": a + "ms"
        };
    }
    function m(a, b) {
        return +String(a).replace(b || "px", "") || e;
    }
    function n(a) {
        return /%$/.test(a) && m(a, "%");
    }
    function o(a, b) {
        return n(a) / 100 * b || m(a);
    }
    function p(a) {
        return (!!m(a) || !!m(a, "%")) && a;
    }
    function q(a, b, c, d) {
        return (a - (d || 0)) * (b + (c || 0));
    }
    function r(a, b, c, d) {
        return -Math.round(a / (b + (c || 0)) - (d || 0));
    }
    function s(a) {
        var b = a.data();
        if (!b.tEnd) {
            var c = a[0], d = {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "oTransitionEnd otransitionend",
                msTransition: "MSTransitionEnd",
                transition: "transitionend"
            };
            S(c, d[sc.prefixed("transition")], function(a) {
                b.tProp && a.propertyName.match(b.tProp) && b.onEndFn();
            }), b.tEnd = !0;
        }
    }
    function t(a, b, c, d) {
        var e, f = a.data();
        f && (f.onEndFn = function() {
            e || (e = !0, clearTimeout(f.tT), c());
        }, f.tProp = b, clearTimeout(f.tT), f.tT = setTimeout(function() {
            f.onEndFn();
        }, 1.5 * d), s(a));
    }
    function u(a, b) {
        if (a.length) {
            var c = a.data();
            Kc ? (a.css(l(0)), c.onEndFn = g, clearTimeout(c.tT)) : a.stop();
            var d = v(b, function() {
                return j(a);
            });
            return a.css(k(d)), d;
        }
    }
    function v() {
        for (var a, b = 0, c = arguments.length; c > b && (a = b ? arguments[b]() : arguments[b], 
        "number" != typeof a); b++) ;
        return a;
    }
    function w(a, b) {
        return Math.round(a + (b - a) / 1.5);
    }
    function x() {
        return x.p = x.p || ("https:" === c.protocol ? "https://" : "http://"), x.p;
    }
    function y(a) {
        var c = b.createElement("a");
        return c.href = a, c;
    }
    function z(a, b) {
        if ("string" != typeof a) return a;
        a = y(a);
        var c, d;
        if (a.host.match(/youtube\.com/) && a.search) {
            if (c = a.search.split("v=")[1]) {
                var e = c.indexOf("&");
                -1 !== e && (c = c.substring(0, e)), d = "youtube";
            }
        } else a.host.match(/youtube\.com|youtu\.be/) ? (c = a.pathname.replace(/^\/(embed\/|v\/)?/, "").replace(/\/.*/, ""), 
        d = "youtube") : a.host.match(/vimeo\.com/) && (d = "vimeo", c = a.pathname.replace(/^\/(video\/)?/, "").replace(/\/.*/, ""));
        return c && d || !b || (c = a.href, d = "custom"), c ? {
            id: c,
            type: d,
            s: a.search.replace(/^\?/, ""),
            p: x()
        } : !1;
    }
    function A(a, b, c) {
        var e, f, g = a.video;
        return "youtube" === g.type ? (f = x() + "img.youtube.com/vi/" + g.id + "/default.jpg", 
        e = f.replace(/\/default.jpg$/, "/hqdefault.jpg"), a.thumbsReady = !0) : "vimeo" === g.type ? d.ajax({
            url: x() + "vimeo.com/api/v2/video/" + g.id + ".json",
            dataType: "jsonp",
            success: function(d) {
                a.thumbsReady = !0, B(b, {
                    img: d[0].thumbnail_large,
                    thumb: d[0].thumbnail_small
                }, a.i, c);
            }
        }) : a.thumbsReady = !0, {
            img: e,
            thumb: f
        };
    }
    function B(a, b, c, e) {
        for (var f = 0, g = a.length; g > f; f++) {
            var h = a[f];
            if (h.i === c && h.thumbsReady) {
                var i = {
                    videoReady: !0
                };
                i[Zc] = i[_c] = i[$c] = !1, e.splice(f, 1, d.extend({}, h, i, b));
                break;
            }
        }
    }
    function C(a) {
        function b(a, b, e) {
            var f = a.children("img").eq(0), g = a.attr("href"), h = a.attr("src"), i = f.attr("src"), j = b.video, k = e ? z(g, j === !0) : !1;
            k ? g = !1 : k = j, c(a, f, d.extend(b, {
                video: k,
                img: b.img || g || h || i,
                thumb: b.thumb || i || h || g
            }));
        }
        function c(a, b, c) {
            var e = c.thumb && c.img !== c.thumb, f = m(c.width || a.attr("width")), g = m(c.height || a.attr("height"));
            d.extend(c, {
                width: f,
                height: g,
                thumbratio: R(c.thumbratio || m(c.thumbwidth || b && b.attr("width") || e || f) / m(c.thumbheight || b && b.attr("height") || e || g))
            });
        }
        var e = [];
        return a.children().each(function() {
            var a = d(this), f = Q(d.extend(a.data(), {
                id: a.attr("id")
            }));
            if (a.is("a, img")) b(a, f, !0); else {
                if (a.is(":empty")) return;
                c(a, null, d.extend(f, {
                    html: this,
                    _html: a.html()
                }));
            }
            e.push(f);
        }), e;
    }
    function D(a) {
        return 0 === a.offsetWidth && 0 === a.offsetHeight;
    }
    function E(a) {
        return !d.contains(b.documentElement, a);
    }
    function F(a, b, c) {
        a() ? b() : setTimeout(function() {
            F(a, b);
        }, c || 100);
    }
    function G(a) {
        c.replace(c.protocol + "//" + c.host + c.pathname.replace(/^\/?/, "/") + c.search + "#" + a);
    }
    function H(a, b, c) {
        var d = a.data(), e = d.measures;
        if (e && (!d.l || d.l.W !== e.width || d.l.H !== e.height || d.l.r !== e.ratio || d.l.w !== b.w || d.l.h !== b.h || d.l.m !== c)) {
            var f = e.width, g = e.height, i = b.w / b.h, j = e.ratio >= i, k = "scaledown" === c, l = "contain" === c, m = "cover" === c;
            j && (k || l) || !j && m ? (f = h(b.w, 0, k ? f : 1 / 0), g = f / e.ratio) : (j && m || !j && (k || l)) && (g = h(b.h, 0, k ? g : 1 / 0), 
            f = g * e.ratio), a.css({
                width: Math.ceil(f),
                height: Math.ceil(g),
                left: Math.floor(b.w / 2 - f / 2),
                top: Math.floor(b.h / 2 - g / 2)
            }), d.l = {
                W: e.width,
                H: e.height,
                r: e.ratio,
                w: b.w,
                h: b.h,
                m: c
            };
        }
        return !0;
    }
    function I(a, b) {
        var c = a[0];
        c.styleSheet ? c.styleSheet.cssText = b : a.html(b);
    }
    function J(a, b, c) {
        return b === c ? !1 : b >= a ? "left" : a >= c ? "right" : "left right";
    }
    function K(a, b, c, d) {
        if (!c) return !1;
        if (!isNaN(a)) return a - (d ? 0 : 1);
        for (var e, f = 0, g = b.length; g > f; f++) {
            var h = b[f];
            if (h.id === a) {
                e = f;
                break;
            }
        }
        return e;
    }
    function L(a, b, c) {
        c = c || {}, a.each(function() {
            var a, e = d(this), f = e.data();
            f.clickOn || (f.clickOn = !0, d.extend(ab(e, {
                onStart: function(b) {
                    a = b, (c.onStart || g).call(this, b);
                },
                onMove: c.onMove || g,
                onTouchEnd: c.onTouchEnd || g,
                onEnd: function(c) {
                    c.moved || b.call(this, a);
                }
            }), {
                noMove: !0
            }));
        });
    }
    function M(a, b) {
        return '<div class="' + a + '">' + (b || "") + "</div>";
    }
    function N(a) {
        for (var b = a.length; b; ) {
            var c = Math.floor(Math.random() * b--), d = a[b];
            a[b] = a[c], a[c] = d;
        }
        return a;
    }
    function O(a) {
        return "[object Array]" == Object.prototype.toString.call(a) && d.map(a, function(a) {
            return d.extend({}, a);
        });
    }
    function P(a, b, c) {
        a.scrollLeft(b || 0).scrollTop(c || 0);
    }
    function Q(a) {
        if (a) {
            var b = {};
            return d.each(a, function(a, c) {
                b[a.toLowerCase()] = c;
            }), b;
        }
    }
    function R(a) {
        if (a) {
            var b = +a;
            return isNaN(b) ? (b = a.split("/"), +b[0] / +b[1] || e) : b;
        }
    }
    function S(a, b, c, d) {
        b && (a.addEventListener ? a.addEventListener(b, c, !!d) : a.attachEvent("on" + b, c));
    }
    function T(a) {
        return !!a.getAttribute("disabled");
    }
    function U(a) {
        return {
            tabindex: -1 * a + "",
            disabled: a
        };
    }
    function V(a, b) {
        S(a, "keyup", function(c) {
            T(a) || 13 == c.keyCode && b.call(a, c);
        });
    }
    function W(a, b) {
        S(a, "focus", a.onfocusin = function(c) {
            b.call(a, c);
        }, !0);
    }
    function X(a, b) {
        a.preventDefault ? a.preventDefault() : a.returnValue = !1, b && a.stopPropagation();
    }
    function Y(a) {
        return a ? ">" : "<";
    }
    function Z(a, b) {
        var c = a.data(), e = Math.round(b.pos), f = function() {
            c.sliding = !1, (b.onEnd || g)();
        };
        "undefined" != typeof b.overPos && b.overPos !== b.pos && (e = b.overPos, f = function() {
            Z(a, d.extend({}, b, {
                overPos: b.pos,
                time: Math.max(Sc, b.time / 2)
            }));
        });
        var h = d.extend(k(e), b.width && {
            width: b.width
        });
        c.sliding = !0, Kc ? (a.css(d.extend(l(b.time), h)), b.time > 10 ? t(a, "transform", f, b.time) : f()) : a.stop().animate(h, b.time, bd, f);
    }
    function $(a, b, c, e, f, h) {
        var i = "undefined" != typeof h;
        if (i || (f.push(arguments), Array.prototype.push.call(arguments, f.length), !(f.length > 1))) {
            a = a || d(a), b = b || d(b);
            var j = a[0], k = b[0], l = "crossfade" === e.method, m = function() {
                if (!m.done) {
                    m.done = !0;
                    var a = (i || f.shift()) && f.shift();
                    a && $.apply(this, a), (e.onEnd || g)(!!a);
                }
            }, n = e.time / (h || 1);
            c.removeClass(Pb + " " + Ob), a.stop().addClass(Pb), b.stop().addClass(Ob), l && k && a.fadeTo(0, 0), 
            a.fadeTo(l ? n : 0, 1, l && m), b.fadeTo(n, 0, m), j && l || k || m();
        }
    }
    function _(a) {
        var b = (a.touches || [])[0] || a;
        a._x = b.pageX, a._y = b.clientY, a._now = d.now();
    }
    function ab(c, e) {
        function f(a) {
            return n = d(a.target), v.checked = q = r = t = !1, l || v.flow || a.touches && a.touches.length > 1 || a.which > 1 || Cc && Cc.type !== a.type && Ec || (q = e.select && n.is(e.select, u)) ? q : (p = "touchstart" === a.type, 
            r = n.is("a, a *", u), o = v.control, s = v.noMove || v.noSwipe || o ? 16 : v.snap ? 0 : 4, 
            _(a), m = Cc = a, Dc = a.type.replace(/down|start/, "move").replace(/Down/, "Move"), 
            (e.onStart || g).call(u, a, {
                control: o,
                $target: n
            }), l = v.flow = !0, void ((!p || v.go) && X(a)));
        }
        function h(a) {
            if (a.touches && a.touches.length > 1 || Pc && !a.isPrimary || Dc !== a.type || !l) return l && i(), 
            void (e.onTouchEnd || g)();
            _(a);
            var b = Math.abs(a._x - m._x), c = Math.abs(a._y - m._y), d = b - c, f = (v.go || v.x || d >= 0) && !v.noSwipe, h = 0 > d;
            p && !v.checked ? (l = f) && X(a) : (X(a), (e.onMove || g).call(u, a, {
                touch: p
            })), !t && Math.sqrt(Math.pow(b, 2) + Math.pow(c, 2)) > s && (t = !0), v.checked = v.checked || f || h;
        }
        function i(a) {
            (e.onTouchEnd || g)();
            var b = l;
            v.control = l = !1, b && (v.flow = !1), !b || r && !v.checked || (a && X(a), Ec = !0, 
            clearTimeout(Fc), Fc = setTimeout(function() {
                Ec = !1;
            }, 1e3), (e.onEnd || g).call(u, {
                moved: t,
                $target: n,
                control: o,
                touch: p,
                startEvent: m,
                aborted: !a || "MSPointerCancel" === a.type
            }));
        }
        function j() {
            v.flow || setTimeout(function() {
                v.flow = !0;
            }, 10);
        }
        function k() {
            v.flow && setTimeout(function() {
                v.flow = !1;
            }, Rc);
        }
        var l, m, n, o, p, q, r, s, t, u = c[0], v = {};
        return Pc ? (S(u, "MSPointerDown", f), S(b, "MSPointerMove", h), S(b, "MSPointerCancel", i), 
        S(b, "MSPointerUp", i)) : (S(u, "touchstart", f), S(u, "touchmove", h), S(u, "touchend", i), 
        S(b, "touchstart", j), S(b, "touchend", k), S(b, "touchcancel", k), S(a, "scroll", k), 
        S(u, "mousedown", f), S(b, "mousemove", h), S(b, "mouseup", i)), c.on("click", "a", function(a) {
            v.checked && X(a);
        }), v;
    }
    function bb(a, b) {
        function c(c, d) {
            A = !0, j = l = c._x, q = c._now, p = [ [ q, j ] ], m = n = D.noMove || d ? 0 : u(a, (b.getPos || g)()), 
            (b.onStart || g).call(B, c);
        }
        function e(a, b) {
            s = D.min, t = D.max, v = D.snap, x = a.altKey, A = z = !1, y = b.control, y || C.sliding || c(a);
        }
        function f(d, e) {
            D.noSwipe || (A || c(d), l = d._x, p.push([ d._now, l ]), n = m - (j - l), o = J(n, s, t), 
            s >= n ? n = w(n, s) : n >= t && (n = w(n, t)), D.noMove || (a.css(k(n)), z || (z = !0, 
            e.touch || Pc || a.addClass(cc)), (b.onMove || g).call(B, d, {
                pos: n,
                edge: o
            })));
        }
        function i(e) {
            if (!D.noSwipe || !e.moved) {
                A || c(e.startEvent, !0), e.touch || Pc || a.removeClass(cc), r = d.now();
                for (var f, i, j, k, o, q, u, w, y, z = r - Rc, C = null, E = Sc, F = b.friction, G = p.length - 1; G >= 0; G--) {
                    if (f = p[G][0], i = Math.abs(f - z), null === C || j > i) C = f, k = p[G][1]; else if (C === z || i > j) break;
                    j = i;
                }
                u = h(n, s, t);
                var H = k - l, I = H >= 0, J = r - C, K = J > Rc, L = !K && n !== m && u === n;
                v && (u = h(Math[L ? I ? "floor" : "ceil" : "round"](n / v) * v, s, t), s = t = u), 
                L && (v || u === n) && (y = -(H / J), E *= h(Math.abs(y), b.timeLow, b.timeHigh), 
                o = Math.round(n + y * E / F), v || (u = o), (!I && o > t || I && s > o) && (q = I ? s : t, 
                w = o - q, v || (u = q), w = h(u + .03 * w, q - 50, q + 50), E = Math.abs((n - w) / (y / F)))), 
                E *= x ? 10 : 1, (b.onEnd || g).call(B, d.extend(e, {
                    moved: e.moved || K && v,
                    pos: n,
                    newPos: u,
                    overPos: w,
                    time: E
                }));
            }
        }
        var j, l, m, n, o, p, q, r, s, t, v, x, y, z, A, B = a[0], C = a.data(), D = {};
        return D = d.extend(ab(b.$wrap, d.extend({}, b, {
            onStart: e,
            onMove: f,
            onEnd: i
        })), D);
    }
    function cb(a, b) {
        var c, e, f, h = a[0], i = {
            prevent: {}
        };
        return S(h, Qc, function(a) {
            var h = a.wheelDeltaY || -1 * a.deltaY || 0, j = a.wheelDeltaX || -1 * a.deltaX || 0, k = Math.abs(j) && !Math.abs(h), l = Y(0 > j), m = e === l, n = d.now(), o = Rc > n - f;
            e = l, f = n, k && i.ok && (!i.prevent[l] || c) && (X(a, !0), c && m && o || (b.shift && (c = !0, 
            clearTimeout(i.t), i.t = setTimeout(function() {
                c = !1;
            }, Tc)), (b.onEnd || g)(a, b.shift ? l : j)));
        }), i;
    }
    function db() {
        d.each(d.Fotorama.instances, function(a, b) {
            b.index = a;
        });
    }
    function eb(a) {
        d.Fotorama.instances.push(a), db();
    }
    function fb(a) {
        d.Fotorama.instances.splice(a.index, 1), db();
    }
    var gb = "fotorama", hb = "fullscreen", ib = gb + "__wrap", jb = ib + "--css2", kb = ib + "--css3", lb = ib + "--video", mb = ib + "--fade", nb = ib + "--slide", ob = ib + "--no-controls", pb = ib + "--no-shadows", qb = ib + "--pan-y", rb = ib + "--rtl", sb = ib + "--only-active", tb = ib + "--no-captions", ub = ib + "--toggle-arrows", vb = gb + "__stage", wb = vb + "__frame", xb = wb + "--video", yb = vb + "__shaft", zb = gb + "__grab", Ab = gb + "__pointer", Bb = gb + "__arr", Cb = Bb + "--disabled", Db = Bb + "--prev", Eb = Bb + "--next", Fb = gb + "__nav", Gb = Fb + "-wrap", Hb = Fb + "__shaft", Ib = Fb + "--dots", Jb = Fb + "--thumbs", Kb = Fb + "__frame", Lb = Kb + "--dot", Mb = Kb + "--thumb", Nb = gb + "__fade", Ob = Nb + "-front", Pb = Nb + "-rear", Qb = gb + "__shadow", Rb = Qb + "s", Sb = Rb + "--left", Tb = Rb + "--right", Ub = gb + "__active", Vb = gb + "__select", Wb = gb + "--hidden", Xb = gb + "--fullscreen", Yb = gb + "__fullscreen-icon", Zb = gb + "__error", $b = gb + "__loading", _b = gb + "__loaded", ac = _b + "--full", bc = _b + "--img", cc = gb + "__grabbing", dc = gb + "__img", ec = dc + "--full", fc = gb + "__dot", gc = gb + "__thumb", hc = gc + "-border", ic = gb + "__html", jc = gb + "__video", kc = jc + "-play", lc = jc + "-close", mc = gb + "__caption", nc = gb + "__caption__wrap", oc = gb + "__spinner", pc = '" tabindex="0" role="button', qc = d && d.fn.jquery.split(".");
    if (!qc || qc[0] < 1 || 1 == qc[0] && qc[1] < 8) throw "Fotorama requires jQuery 1.8 or later and will not run without it.";
    var rc = {}, sc = function(a, b, c) {
        function d(a) {
            r.cssText = a;
        }
        function e(a, b) {
            return typeof a === b;
        }
        function f(a, b) {
            return !!~("" + a).indexOf(b);
        }
        function g(a, b) {
            for (var d in a) {
                var e = a[d];
                if (!f(e, "-") && r[e] !== c) return "pfx" == b ? e : !0;
            }
            return !1;
        }
        function h(a, b, d) {
            for (var f in a) {
                var g = b[a[f]];
                if (g !== c) return d === !1 ? a[f] : e(g, "function") ? g.bind(d || b) : g;
            }
            return !1;
        }
        function i(a, b, c) {
            var d = a.charAt(0).toUpperCase() + a.slice(1), f = (a + " " + u.join(d + " ") + d).split(" ");
            return e(b, "string") || e(b, "undefined") ? g(f, b) : (f = (a + " " + v.join(d + " ") + d).split(" "), 
            h(f, b, c));
        }
        var j, k, l, m = "2.6.2", n = {}, o = b.documentElement, p = "modernizr", q = b.createElement(p), r = q.style, s = ({}.toString, 
        " -webkit- -moz- -o- -ms- ".split(" ")), t = "Webkit Moz O ms", u = t.split(" "), v = t.toLowerCase().split(" "), w = {}, x = [], y = x.slice, z = function(a, c, d, e) {
            var f, g, h, i, j = b.createElement("div"), k = b.body, l = k || b.createElement("body");
            if (parseInt(d, 10)) for (;d--; ) h = b.createElement("div"), h.id = e ? e[d] : p + (d + 1), 
            j.appendChild(h);
            return f = [ "&#173;", '<style id="s', p, '">', a, "</style>" ].join(""), j.id = p, 
            (k ? j : l).innerHTML += f, l.appendChild(j), k || (l.style.background = "", l.style.overflow = "hidden", 
            i = o.style.overflow, o.style.overflow = "hidden", o.appendChild(l)), g = c(j, a), 
            k ? j.parentNode.removeChild(j) : (l.parentNode.removeChild(l), o.style.overflow = i), 
            !!g;
        }, A = {}.hasOwnProperty;
        l = e(A, "undefined") || e(A.call, "undefined") ? function(a, b) {
            return b in a && e(a.constructor.prototype[b], "undefined");
        } : function(a, b) {
            return A.call(a, b);
        }, Function.prototype.bind || (Function.prototype.bind = function(a) {
            var b = this;
            if ("function" != typeof b) throw new TypeError();
            var c = y.call(arguments, 1), d = function() {
                if (this instanceof d) {
                    var e = function() {};
                    e.prototype = b.prototype;
                    var f = new e(), g = b.apply(f, c.concat(y.call(arguments)));
                    return Object(g) === g ? g : f;
                }
                return b.apply(a, c.concat(y.call(arguments)));
            };
            return d;
        }), w.csstransforms3d = function() {
            var a = !!i("perspective");
            return a;
        };
        for (var B in w) l(w, B) && (k = B.toLowerCase(), n[k] = w[B](), x.push((n[k] ? "" : "no-") + k));
        return n.addTest = function(a, b) {
            if ("object" == typeof a) for (var d in a) l(a, d) && n.addTest(d, a[d]); else {
                if (a = a.toLowerCase(), n[a] !== c) return n;
                b = "function" == typeof b ? b() : b, "undefined" != typeof enableClasses && enableClasses && (o.className += " " + (b ? "" : "no-") + a), 
                n[a] = b;
            }
            return n;
        }, d(""), q = j = null, n._version = m, n._prefixes = s, n._domPrefixes = v, n._cssomPrefixes = u, 
        n.testProp = function(a) {
            return g([ a ]);
        }, n.testAllProps = i, n.testStyles = z, n.prefixed = function(a, b, c) {
            return b ? i(a, b, c) : i(a, "pfx");
        }, n;
    }(a, b), tc = {
        ok: !1,
        is: function() {
            return !1;
        },
        request: function() {},
        cancel: function() {},
        event: "",
        prefix: ""
    }, uc = "webkit moz o ms khtml".split(" ");
    if ("undefined" != typeof b.cancelFullScreen) tc.ok = !0; else for (var vc = 0, wc = uc.length; wc > vc; vc++) if (tc.prefix = uc[vc], 
    "undefined" != typeof b[tc.prefix + "CancelFullScreen"]) {
        tc.ok = !0;
        break;
    }
    tc.ok && (tc.event = tc.prefix + "fullscreenchange", tc.is = function() {
        switch (this.prefix) {
          case "":
            return b.fullScreen;

          case "webkit":
            return b.webkitIsFullScreen;

          default:
            return b[this.prefix + "FullScreen"];
        }
    }, tc.request = function(a) {
        return "" === this.prefix ? a.requestFullScreen() : a[this.prefix + "RequestFullScreen"]();
    }, tc.cancel = function() {
        return "" === this.prefix ? b.cancelFullScreen() : b[this.prefix + "CancelFullScreen"]();
    });
    var xc, yc = {
        lines: 12,
        length: 5,
        width: 2,
        radius: 7,
        corners: 1,
        rotate: 15,
        color: "rgba(128, 128, 128, .75)",
        hwaccel: !0
    }, zc = {
        top: "auto",
        left: "auto",
        className: ""
    };
    !function(a, b) {
        xc = b();
    }(this, function() {
        function a(a, c) {
            var d, e = b.createElement(a || "div");
            for (d in c) e[d] = c[d];
            return e;
        }
        function c(a) {
            for (var b = 1, c = arguments.length; c > b; b++) a.appendChild(arguments[b]);
            return a;
        }
        function d(a, b, c, d) {
            var e = [ "opacity", b, ~~(100 * a), c, d ].join("-"), f = .01 + c / d * 100, g = Math.max(1 - (1 - a) / b * (100 - f), a), h = m.substring(0, m.indexOf("Animation")).toLowerCase(), i = h && "-" + h + "-" || "";
            return o[e] || (p.insertRule("@" + i + "keyframes " + e + "{0%{opacity:" + g + "}" + f + "%{opacity:" + a + "}" + (f + .01) + "%{opacity:1}" + (f + b) % 100 + "%{opacity:" + a + "}100%{opacity:" + g + "}}", p.cssRules.length), 
            o[e] = 1), e;
        }
        function f(a, b) {
            var c, d, f = a.style;
            for (b = b.charAt(0).toUpperCase() + b.slice(1), d = 0; d < n.length; d++) if (c = n[d] + b, 
            f[c] !== e) return c;
            return f[b] !== e ? b : void 0;
        }
        function g(a, b) {
            for (var c in b) a.style[f(a, c) || c] = b[c];
            return a;
        }
        function h(a) {
            for (var b = 1; b < arguments.length; b++) {
                var c = arguments[b];
                for (var d in c) a[d] === e && (a[d] = c[d]);
            }
            return a;
        }
        function i(a) {
            for (var b = {
                x: a.offsetLeft,
                y: a.offsetTop
            }; a = a.offsetParent; ) b.x += a.offsetLeft, b.y += a.offsetTop;
            return b;
        }
        function j(a, b) {
            return "string" == typeof a ? a : a[b % a.length];
        }
        function k(a) {
            return "undefined" == typeof this ? new k(a) : void (this.opts = h(a || {}, k.defaults, q));
        }
        function l() {
            function b(b, c) {
                return a("<" + b + ' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">', c);
            }
            p.addRule(".spin-vml", "behavior:url(#default#VML)"), k.prototype.lines = function(a, d) {
                function e() {
                    return g(b("group", {
                        coordsize: k + " " + k,
                        coordorigin: -i + " " + -i
                    }), {
                        width: k,
                        height: k
                    });
                }
                function f(a, f, h) {
                    c(m, c(g(e(), {
                        rotation: 360 / d.lines * a + "deg",
                        left: ~~f
                    }), c(g(b("roundrect", {
                        arcsize: d.corners
                    }), {
                        width: i,
                        height: d.width,
                        left: d.radius,
                        top: -d.width >> 1,
                        filter: h
                    }), b("fill", {
                        color: j(d.color, a),
                        opacity: d.opacity
                    }), b("stroke", {
                        opacity: 0
                    }))));
                }
                var h, i = d.length + d.width, k = 2 * i, l = 2 * -(d.width + d.length) + "px", m = g(e(), {
                    position: "absolute",
                    top: l,
                    left: l
                });
                if (d.shadow) for (h = 1; h <= d.lines; h++) f(h, -2, "progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)");
                for (h = 1; h <= d.lines; h++) f(h);
                return c(a, m);
            }, k.prototype.opacity = function(a, b, c, d) {
                var e = a.firstChild;
                d = d.shadow && d.lines || 0, e && b + d < e.childNodes.length && (e = e.childNodes[b + d], 
                e = e && e.firstChild, e = e && e.firstChild, e && (e.opacity = c));
            };
        }
        var m, n = [ "webkit", "Moz", "ms", "O" ], o = {}, p = function() {
            var d = a("style", {
                type: "text/css"
            });
            return c(b.getElementsByTagName("head")[0], d), d.sheet || d.styleSheet;
        }(), q = {
            lines: 12,
            length: 7,
            width: 5,
            radius: 10,
            rotate: 0,
            corners: 1,
            color: "#000",
            direction: 1,
            speed: 1,
            trail: 100,
            opacity: .25,
            fps: 20,
            zIndex: 2e9,
            className: "spinner",
            top: "auto",
            left: "auto",
            position: "relative"
        };
        k.defaults = {}, h(k.prototype, {
            spin: function(b) {
                this.stop();
                var c, d, e = this, f = e.opts, h = e.el = g(a(0, {
                    className: f.className
                }), {
                    position: f.position,
                    width: 0,
                    zIndex: f.zIndex
                }), j = f.radius + f.length + f.width;
                if (b && (b.insertBefore(h, b.firstChild || null), d = i(b), c = i(h), g(h, {
                    left: ("auto" == f.left ? d.x - c.x + (b.offsetWidth >> 1) : parseInt(f.left, 10) + j) + "px",
                    top: ("auto" == f.top ? d.y - c.y + (b.offsetHeight >> 1) : parseInt(f.top, 10) + j) + "px"
                })), h.setAttribute("role", "progressbar"), e.lines(h, e.opts), !m) {
                    var k, l = 0, n = (f.lines - 1) * (1 - f.direction) / 2, o = f.fps, p = o / f.speed, q = (1 - f.opacity) / (p * f.trail / 100), r = p / f.lines;
                    !function s() {
                        l++;
                        for (var a = 0; a < f.lines; a++) k = Math.max(1 - (l + (f.lines - a) * r) % p * q, f.opacity), 
                        e.opacity(h, a * f.direction + n, k, f);
                        e.timeout = e.el && setTimeout(s, ~~(1e3 / o));
                    }();
                }
                return e;
            },
            stop: function() {
                var a = this.el;
                return a && (clearTimeout(this.timeout), a.parentNode && a.parentNode.removeChild(a), 
                this.el = e), this;
            },
            lines: function(b, e) {
                function f(b, c) {
                    return g(a(), {
                        position: "absolute",
                        width: e.length + e.width + "px",
                        height: e.width + "px",
                        background: b,
                        boxShadow: c,
                        transformOrigin: "left",
                        transform: "rotate(" + ~~(360 / e.lines * i + e.rotate) + "deg) translate(" + e.radius + "px,0)",
                        borderRadius: (e.corners * e.width >> 1) + "px"
                    });
                }
                for (var h, i = 0, k = (e.lines - 1) * (1 - e.direction) / 2; i < e.lines; i++) h = g(a(), {
                    position: "absolute",
                    top: 1 + ~(e.width / 2) + "px",
                    transform: e.hwaccel ? "translate3d(0,0,0)" : "",
                    opacity: e.opacity,
                    animation: m && d(e.opacity, e.trail, k + i * e.direction, e.lines) + " " + 1 / e.speed + "s linear infinite"
                }), e.shadow && c(h, g(f("#000", "0 0 4px #000"), {
                    top: "2px"
                })), c(b, c(h, f(j(e.color, i), "0 0 1px rgba(0,0,0,.1)")));
                return b;
            },
            opacity: function(a, b, c) {
                b < a.childNodes.length && (a.childNodes[b].style.opacity = c);
            }
        });
        var r = g(a("group"), {
            behavior: "url(#default#VML)"
        });
        return !f(r, "transform") && r.adj ? l() : m = f(r, "animation"), k;
    });
    var Ac, Bc, Cc, Dc, Ec, Fc, Gc = d(a), Hc = d(b), Ic = "quirks" === c.hash.replace("#", ""), Jc = sc.csstransforms3d, Kc = Jc && !Ic, Lc = Jc || "CSS1Compat" === b.compatMode, Mc = tc.ok, Nc = navigator.userAgent.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i), Oc = !Kc || Nc, Pc = navigator.msPointerEnabled, Qc = "onwheel" in b.createElement("div") ? "wheel" : b.onmousewheel !== e ? "mousewheel" : "DOMMouseScroll", Rc = 250, Sc = 300, Tc = 1400, Uc = 5e3, Vc = 2, Wc = 64, Xc = 500, Yc = 333, Zc = "$stageFrame", $c = "$navDotFrame", _c = "$navThumbFrame", ad = "auto", bd = f([ .1, 0, .25, 1 ]), cd = 99999, dd = {
        width: null,
        minwidth: null,
        maxwidth: "100%",
        height: null,
        minheight: null,
        maxheight: null,
        ratio: null,
        margin: Vc,
        glimpse: 0,
        fit: "contain",
        nav: "dots",
        navposition: "bottom",
        navwidth: null,
        thumbwidth: Wc,
        thumbheight: Wc,
        thumbmargin: Vc,
        thumbborderwidth: Vc,
        thumbfit: "cover",
        allowfullscreen: !1,
        transition: "slide",
        clicktransition: null,
        transitionduration: Sc,
        captions: !0,
        hash: !1,
        startindex: 0,
        loop: !1,
        autoplay: !1,
        stopautoplayontouch: !0,
        keyboard: !1,
        arrows: !0,
        click: !0,
        swipe: !0,
        trackpad: !1,
        controlsonstart: !0,
        shuffle: !1,
        direction: "ltr",
        shadows: !0,
        spinner: null
    }, ed = {
        left: !0,
        right: !0,
        down: !1,
        up: !1,
        space: !1,
        home: !1,
        end: !1
    };
    jQuery.Fotorama = function(a, e) {
        function f() {
            d.each(yd, function(a, b) {
                if (!b.i) {
                    b.i = le++;
                    var c = z(b.video, !0);
                    if (c) {
                        var d = {};
                        b.video = c, b.img || b.thumb ? b.thumbsReady = !0 : d = A(b, yd, he), B(yd, {
                            img: d.img,
                            thumb: d.thumb
                        }, b.i, he);
                    }
                }
            });
        }
        function g(a) {
            return Yd[a] || he.fullScreen;
        }
        function i(a) {
            var b = "keydown." + gb, c = gb + ie, d = "keydown." + c, f = "resize." + c + " orientationchange." + c;
            a ? (Hc.on(d, function(a) {
                var b, c;
                Cd && 27 === a.keyCode ? (b = !0, md(Cd, !0, !0)) : (he.fullScreen || e.keyboard && !he.index) && (27 === a.keyCode ? (b = !0, 
                he.cancelFullScreen()) : a.shiftKey && 32 === a.keyCode && g("space") || 37 === a.keyCode && g("left") || 38 === a.keyCode && g("up") ? c = "<" : 32 === a.keyCode && g("space") || 39 === a.keyCode && g("right") || 40 === a.keyCode && g("down") ? c = ">" : 36 === a.keyCode && g("home") ? c = "<<" : 35 === a.keyCode && g("end") && (c = ">>")), 
                (b || c) && X(a), c && he.show({
                    index: c,
                    slow: a.altKey,
                    user: !0
                });
            }), he.index || Hc.off(b).on(b, "textarea, input, select", function(a) {
                !Bc.hasClass(hb) && a.stopPropagation();
            }), Gc.on(f, he.resize)) : (Hc.off(d), Gc.off(f));
        }
        function j(b) {
            b !== j.f && (b ? (a.html("").addClass(gb + " " + je).append(pe).before(ne).before(oe), 
            eb(he)) : (pe.detach(), ne.detach(), oe.detach(), a.html(me.urtext).removeClass(je), 
            fb(he)), i(b), j.f = b);
        }
        function n() {
            yd = he.data = yd || O(e.data) || C(a), zd = he.size = yd.length, !xd.ok && e.shuffle && N(yd), 
            f(), Ie = y(Ie), zd && j(!0);
        }
        function s() {
            var a = 2 > zd || Cd;
            Le.noMove = a || Rd, Le.noSwipe = a || !e.swipe, !Vd && re.toggleClass(zb, !e.click && !Le.noMove && !Le.noSwipe), 
            Pc && pe.toggleClass(qb, !Le.noSwipe);
        }
        function t(a) {
            a === !0 && (a = ""), e.autoplay = Math.max(+a || Uc, 1.5 * Ud);
        }
        function w() {
            function a(a, c) {
                b[a ? "add" : "remove"].push(c);
            }
            he.options = e = Q(e), Rd = "crossfade" === e.transition || "dissolve" === e.transition, 
            Ld = e.loop && (zd > 2 || Rd && (!Vd || "slide" !== Vd)), Ud = +e.transitionduration || Sc, 
            Xd = "rtl" === e.direction, Yd = d.extend({}, e.keyboard && ed, e.keyboard);
            var b = {
                add: [],
                remove: []
            };
            zd > 1 ? (Md = e.nav, Od = "top" === e.navposition, b.remove.push(Vb), ve.toggle(!!e.arrows)) : (Md = !1, 
            ve.hide()), cc(), Bd = new xc(d.extend(yc, e.spinner, zc, {
                direction: Xd ? -1 : 1
            })), Ec(), Fc(), e.autoplay && t(e.autoplay), Sd = m(e.thumbwidth) || Wc, Td = m(e.thumbheight) || Wc, 
            Me.ok = Oe.ok = e.trackpad && !Oc, s(), dd(e, [ Ke ]), Nd = "thumbs" === Md, Nd ? (rc(zd, "navThumb"), 
            Ad = Ae, ge = _c, I(ne, d.Fotorama.jst.style({
                w: Sd,
                h: Td,
                b: e.thumbborderwidth,
                m: e.thumbmargin,
                s: ie,
                q: !Lc
            })), xe.addClass(Jb).removeClass(Ib)) : "dots" === Md ? (rc(zd, "navDot"), Ad = ze, 
            ge = $c, xe.addClass(Ib).removeClass(Jb)) : (Md = !1, xe.removeClass(Jb + " " + Ib)), 
            Md && (Od ? we.insertBefore(qe) : we.insertAfter(qe), wc.nav = !1, wc(Ad, ye, "nav")), 
            Pd = e.allowfullscreen, Pd ? (Ce.prependTo(qe), Qd = Mc && "native" === Pd) : (Ce.detach(), 
            Qd = !1), a(Rd, mb), a(!Rd, nb), a(!e.captions, tb), a(Xd, rb), a("always" !== e.arrows, ub), 
            Wd = e.shadows && !Oc, a(!Wd, pb), pe.addClass(b.add.join(" ")).removeClass(b.remove.join(" ")), 
            Je = d.extend({}, e);
        }
        function x(a) {
            return 0 > a ? (zd + a % zd) % zd : a >= zd ? a % zd : a;
        }
        function y(a) {
            return h(a, 0, zd - 1);
        }
        function D(a) {
            return Ld ? x(a) : y(a);
        }
        function T(a) {
            return a > 0 || Ld ? a - 1 : !1;
        }
        function _(a) {
            return zd - 1 > a || Ld ? a + 1 : !1;
        }
        function ab() {
            Le.min = Ld ? -1 / 0 : -q(zd - 1, Ke.w, e.margin, Fd), Le.max = Ld ? 1 / 0 : -q(0, Ke.w, e.margin, Fd), 
            Le.snap = Ke.w + e.margin;
        }
        function db() {
            Ne.min = Math.min(0, Ke.nw - ye.width()), Ne.max = 0, ye.toggleClass(zb, !(Ne.noMove = Ne.min === Ne.max));
        }
        function Nb(a, b, c) {
            if ("number" == typeof a) {
                a = new Array(a);
                var e = !0;
            }
            return d.each(a, function(a, d) {
                if (e && (d = a), "number" == typeof d) {
                    var f = yd[x(d)];
                    if (f) {
                        var g = "$" + b + "Frame", h = f[g];
                        c.call(this, a, d, f, h, g, h && h.data());
                    }
                }
            });
        }
        function Ob(a, b, c, d) {
            (!Zd || "*" === Zd && d === Kd) && (a = p(e.width) || p(a) || Xc, b = p(e.height) || p(b) || Yc, 
            he.resize({
                width: a,
                ratio: e.ratio || c || a / b
            }, 0, d !== Kd && "*"));
        }
        function Pb(a, b, c, f, g) {
            Nb(a, b, function(a, h, i, j, k, l) {
                function m(a) {
                    var b = x(h);
                    fd(a, {
                        index: b,
                        src: v,
                        frame: yd[b]
                    });
                }
                function n() {
                    s.remove(), d.Fotorama.cache[v] = "error", i.html && "stage" === b || !w || w === v ? (!v || i.html || q ? "stage" === b && (j.trigger("f:load").removeClass($b + " " + Zb).addClass(_b), 
                    m("load"), Ob()) : (j.trigger("f:error").removeClass($b).addClass(Zb), m("error")), 
                    l.state = "error", !(zd > 1 && yd[h] === i) || i.html || i.deleted || i.video || q || (i.deleted = !0, 
                    he.splice(h, 1))) : (i[u] = v = w, Pb([ h ], b, c, f, !0));
                }
                function o() {
                    d.Fotorama.measures[v] = t.measures = d.Fotorama.measures[v] || {
                        width: r.width,
                        height: r.height,
                        ratio: r.width / r.height
                    }, Ob(t.measures.width, t.measures.height, t.measures.ratio, h), s.off("load error").addClass(dc + (q ? " " + ec : "")).prependTo(j), 
                    H(s, c || Ke, f || i.fit || e.fit), d.Fotorama.cache[v] = l.state = "loaded", setTimeout(function() {
                        j.trigger("f:load").removeClass($b + " " + Zb).addClass(_b + " " + (q ? ac : bc)), 
                        "stage" === b ? m("load") : (i.thumbratio === ad || !i.thumbratio && e.thumbratio === ad) && (i.thumbratio = t.measures.ratio, 
                        vd());
                    }, 0);
                }
                function p() {
                    var a = 10;
                    F(function() {
                        return !ee || !a-- && !Oc;
                    }, function() {
                        o();
                    });
                }
                if (j) {
                    var q = he.fullScreen && i.full && i.full !== i.img && !l.$full && "stage" === b;
                    if (!l.$img || g || q) {
                        var r = new Image(), s = d(r), t = s.data();
                        l[q ? "$full" : "$img"] = s;
                        var u = "stage" === b ? q ? "full" : "img" : "thumb", v = i[u], w = q ? null : i["stage" === b ? "thumb" : "img"];
                        if ("navThumb" === b && (j = l.$wrap), !v) return void n();
                        d.Fotorama.cache[v] ? !function y() {
                            "error" === d.Fotorama.cache[v] ? n() : "loaded" === d.Fotorama.cache[v] ? setTimeout(p, 0) : setTimeout(y, 100);
                        }() : (d.Fotorama.cache[v] = "*", s.on("load", p).on("error", n)), l.state = "", 
                        r.src = v;
                    }
                }
            });
        }
        function Qb(a) {
            He.append(Bd.spin().el).appendTo(a);
        }
        function cc() {
            He.detach(), Bd && Bd.stop();
        }
        function jc() {
            var a = Dd[Zc];
            a && !a.data().state && (Qb(a), a.on("f:load f:error", function() {
                a.off("f:load f:error"), cc();
            }));
        }
        function qc(a) {
            V(a, sd), W(a, function() {
                setTimeout(function() {
                    P(xe);
                }, 0), Nc({
                    time: Ud,
                    guessIndex: d(this).data().eq,
                    minMax: Ne
                });
            });
        }
        function rc(a, b) {
            Nb(a, b, function(a, c, e, f, g, h) {
                if (!f) {
                    f = e[g] = pe[g].clone(), h = f.data(), h.data = e;
                    var i = f[0];
                    "stage" === b ? (e.html && d('<div class="' + ic + '"></div>').append(e._html ? d(e.html).removeAttr("id").html(e._html) : e.html).appendTo(f), 
                    e.caption && d(M(mc, M(nc, e.caption))).appendTo(f), e.video && f.addClass(xb).append(Ee.clone()), 
                    W(i, function() {
                        setTimeout(function() {
                            P(qe);
                        }, 0), pd({
                            index: h.eq,
                            user: !0
                        });
                    }), se = se.add(f)) : "navDot" === b ? (qc(i), ze = ze.add(f)) : "navThumb" === b && (qc(i), 
                    h.$wrap = f.children(":first"), Ae = Ae.add(f), e.video && f.append(Ee.clone()));
                }
            });
        }
        function sc(a, b, c) {
            return a && a.length && H(a, b, c);
        }
        function uc(a) {
            Nb(a, "stage", function(a, b, c, f, g, h) {
                if (f) {
                    var i = x(b);
                    h.eq = i, Qe[Zc][i] = f.css(d.extend({
                        left: Rd ? 0 : q(b, Ke.w, e.margin, Fd)
                    }, Rd && l(0))), E(f[0]) && (f.appendTo(re), md(c.$video));
                    var j = c.fit || e.fit;
                    sc(h.$img, Ke, j), sc(h.$full, Ke, j);
                }
            });
        }
        function vc(a, b) {
            if ("thumbs" === Md && !isNaN(a)) {
                var c = -a, f = -a + Ke.nw;
                Ae.each(function() {
                    var a = d(this), g = a.data(), h = g.eq, i = {
                        h: Td
                    }, j = (yd[h] || {}).thumbfit || e.thumbfit;
                    i.w = g.w, g.l + g.w < c || g.l > f || sc(g.$img, i, j) || b && Pb([ h ], "navThumb", i, j);
                });
            }
        }
        function wc(a, b, c) {
            if (!wc[c]) {
                var f = "nav" === c && Nd, g = 0;
                b.append(a.filter(function() {
                    for (var a, b = d(this), c = b.data(), e = 0, f = yd.length; f > e; e++) if (c.data === yd[e]) {
                        a = !0, c.eq = e;
                        break;
                    }
                    return a || b.remove() && !1;
                }).sort(function(a, b) {
                    return d(a).data().eq - d(b).data().eq;
                }).each(function() {
                    if (f) {
                        var a = d(this), b = a.data(), c = Math.round(Td * b.data.thumbratio) || Sd;
                        b.l = g, b.w = c, a.css({
                            width: c
                        }), g += c + e.thumbmargin;
                    }
                })), wc[c] = !0;
            }
        }
        function Cc(a) {
            return a - Re > Ke.w / 3;
        }
        function Dc(a) {
            return !(Ld || Ie + a && Ie - zd + a || Cd);
        }
        function Ec() {
            var a = Dc(0), b = Dc(1);
            te.toggleClass(Cb, a).attr(U(a)), ue.toggleClass(Cb, b).attr(U(b));
        }
        function Fc() {
            Me.ok && (Me.prevent = {
                "<": Dc(0),
                ">": Dc(1)
            });
        }
        function Ic(a) {
            var b, c, d = a.data();
            return Nd ? (b = d.l, c = d.w) : (b = a.position().left, c = a.width()), {
                c: b + c / 2,
                min: -b + 10 * e.thumbmargin,
                max: -b + Ke.w - c - 10 * e.thumbmargin
            };
        }
        function Jc(a) {
            var b = Dd[ge].data();
            Z(Be, {
                time: 1.2 * a,
                pos: b.l,
                width: b.w - 2 * e.thumbborderwidth
            });
        }
        function Nc(a) {
            var b = yd[a.guessIndex][ge];
            if (b) {
                var c = Ne.min !== Ne.max, d = a.minMax || c && Ic(Dd[ge]), e = c && (a.keep && Nc.l ? Nc.l : h((a.coo || Ke.nw / 2) - Ic(b).c, d.min, d.max)), f = c && h(e, Ne.min, Ne.max), g = 1.1 * a.time;
                Z(ye, {
                    time: g,
                    pos: f || 0,
                    onEnd: function() {
                        vc(f, !0);
                    }
                }), ld(xe, J(f, Ne.min, Ne.max)), Nc.l = e;
            }
        }
        function Qc() {
            Tc(ge), Pe[ge].push(Dd[ge].addClass(Ub));
        }
        function Tc(a) {
            for (var b = Pe[a]; b.length; ) b.shift().removeClass(Ub);
        }
        function Vc(a) {
            var b = Qe[a];
            d.each(Ed, function(a, c) {
                delete b[x(c)];
            }), d.each(b, function(a, c) {
                delete b[a], c.detach();
            });
        }
        function bd(a) {
            Fd = Gd = Ie;
            var b = Dd[Zc];
            b && (Tc(Zc), Pe[Zc].push(b.addClass(Ub)), a || he.show.onEnd(!0), u(re, 0, !0), 
            Vc(Zc), uc(Ed), ab(), db());
        }
        function dd(a, b) {
            a && d.each(b, function(b, c) {
                c && d.extend(c, {
                    width: a.width || c.width,
                    height: a.height,
                    minwidth: a.minwidth,
                    maxwidth: a.maxwidth,
                    minheight: a.minheight,
                    maxheight: a.maxheight,
                    ratio: R(a.ratio)
                });
            });
        }
        function fd(b, c) {
            a.trigger(gb + ":" + b, [ he, c ]);
        }
        function gd() {
            clearTimeout(hd.t), ee = 1, e.stopautoplayontouch ? he.stopAutoplay() : be = !0;
        }
        function hd() {
            e.stopautoplayontouch || (id(), jd()), hd.t = setTimeout(function() {
                ee = 0;
            }, Sc + Rc);
        }
        function id() {
            be = !(!Cd && !ce);
        }
        function jd() {
            if (clearTimeout(jd.t), !e.autoplay || be) return void (he.autoplay && (he.autoplay = !1, 
            fd("stopautoplay")));
            he.autoplay || (he.autoplay = !0, fd("startautoplay"));
            var a = Ie, b = Dd[Zc].data();
            F(function() {
                return b.state || a !== Ie;
            }, function() {
                jd.t = setTimeout(function() {
                    be || a !== Ie || he.show(Ld ? Y(!Xd) : x(Ie + (Xd ? -1 : 1)));
                }, e.autoplay);
            });
        }
        function kd() {
            he.fullScreen && (he.fullScreen = !1, Mc && tc.cancel(ke), Bc.removeClass(hb), Ac.removeClass(hb), 
            a.removeClass(Xb).insertAfter(oe), Ke = d.extend({}, de), md(Cd, !0, !0), rd("x", !1), 
            he.resize(), Pb(Ed, "stage"), P(Gc, _d, $d), fd("fullscreenexit"));
        }
        function ld(a, b) {
            Wd && (a.removeClass(Sb + " " + Tb), b && !Cd && a.addClass(b.replace(/^|\s/g, " " + Rb + "--")));
        }
        function md(a, b, c) {
            b && (pe.removeClass(lb), Cd = !1, s()), a && a !== Cd && (a.remove(), fd("unloadvideo")), 
            c && (id(), jd());
        }
        function nd(a) {
            pe.toggleClass(ob, a);
        }
        function od(a) {
            if (!Le.flow) {
                var b = a ? a.pageX : od.x, c = b && !Dc(Cc(b)) && e.click;
                od.p !== c && qe.toggleClass(Ab, c) && (od.p = c, od.x = b);
            }
        }
        function pd(a) {
            clearTimeout(pd.t), e.clicktransition && e.clicktransition !== e.transition ? (Vd = e.transition, 
            he.setOptions({
                transition: e.clicktransition
            }), pd.t = setTimeout(function() {
                he.show(a);
            }, 10)) : he.show(a);
        }
        function qd(a, b) {
            var c = a.target, f = d(c);
            f.hasClass(kc) ? he.playVideo() : c === De ? he.toggleFullScreen() : Cd ? c === Ge && md(Cd, !0, !0) : b ? nd() : e.click && pd({
                index: a.shiftKey || Y(Cc(a._x)),
                slow: a.altKey,
                user: !0
            });
        }
        function rd(a, b) {
            Le[a] = Ne[a] = b;
        }
        function sd(a) {
            var b = d(this).data().eq;
            pd({
                index: b,
                slow: a.altKey,
                user: !0,
                coo: a._x - xe.offset().left
            });
        }
        function td(a) {
            pd({
                index: ve.index(this) ? ">" : "<",
                slow: a.altKey,
                user: !0
            });
        }
        function ud(a) {
            W(a, function() {
                P(qe), setTimeout(function() {
                    P(qe);
                }, 0), nd(!1);
            });
        }
        function vd() {
            if (vd.ok, n(), w(), !vd.i) {
                vd.i = !0;
                var a = e.startindex;
                (a || e.hash && c.hash) && (Kd = K(a || c.hash.replace(/^#/, ""), yd, 0 === he.index || a, a)), 
                Ie = Fd = Gd = Hd = Kd = D(Kd) || 0;
            }
            if (zd) {
                if (wd()) return;
                Cd && md(Cd, !0), Ed = [], Vc(Zc), vd.ok = !0, he.show({
                    index: Ie,
                    time: 0
                }), he.resize();
            } else he.destroy();
        }
        function wd() {
            return !wd.f === Xd ? (wd.f = Xd, Ie = zd - 1 - Ie, he.reverse(), !0) : void 0;
        }
        function xd() {
            xd.ok || (xd.ok = !0, fd("ready"));
        }
        Ac = d("html"), Bc = d("body");
        var yd, zd, Ad, Bd, Cd, Dd, Ed, Fd, Gd, Hd, Id, Jd, Kd, Ld, Md, Nd, Od, Pd, Qd, Rd, Sd, Td, Ud, Vd, Wd, Xd, Yd, Zd, $d, _d, ae, be, ce, de, ee, fe, ge, he = this, ie = d.now(), je = gb + ie, ke = a[0], le = 1, me = a.data(), ne = d("<style></style>"), oe = d(M(Wb)), pe = d(M(ib)), qe = d(M(vb)).appendTo(pe), re = (qe[0], 
        d(M(yb)).appendTo(qe)), se = d(), te = d(M(Bb + " " + Db + pc)), ue = d(M(Bb + " " + Eb + pc)), ve = te.add(ue).appendTo(qe), we = d(M(Gb)), xe = d(M(Fb)).appendTo(we), ye = d(M(Hb)).appendTo(xe), ze = d(), Ae = d(), Be = (re.data(), 
        ye.data(), d(M(hc)).appendTo(ye)), Ce = d(M(Yb + pc)), De = Ce[0], Ee = d(M(kc)), Fe = d(M(lc)).appendTo(qe), Ge = Fe[0], He = d(M(oc)), Ie = !1, Je = {}, Ke = {}, Le = {}, Me = {}, Ne = {}, Oe = {}, Pe = {}, Qe = {}, Re = 0, Se = [];
        pe[Zc] = d(M(wb)), pe[_c] = d(M(Kb + " " + Mb + pc, M(gc))), pe[$c] = d(M(Kb + " " + Lb + pc, M(fc))), 
        Pe[Zc] = [], Pe[_c] = [], Pe[$c] = [], Qe[Zc] = {}, pe.addClass(Kc ? kb : jb).toggleClass(ob, !e.controlsonstart), 
        me.fotorama = this, he.startAutoplay = function(a) {
            return he.autoplay ? this : (be = ce = !1, t(a || e.autoplay), jd(), this);
        }, he.stopAutoplay = function() {
            return he.autoplay && (be = ce = !0, jd()), this;
        }, he.show = function(a) {
            var b;
            "object" != typeof a ? (b = a, a = {}) : b = a.index, b = ">" === b ? Gd + 1 : "<" === b ? Gd - 1 : "<<" === b ? 0 : ">>" === b ? zd - 1 : b, 
            b = isNaN(b) ? K(b, yd, !0) : b, b = "undefined" == typeof b ? Ie || 0 : b, he.activeIndex = Ie = D(b), 
            Id = T(Ie), Jd = _(Ie), Ed = [ Ie, Id, Jd ], Gd = Ld ? b : Ie;
            var c = Math.abs(Hd - Gd), d = v(a.time, function() {
                return Math.min(Ud * (1 + (c - 1) / 12), 2 * Ud);
            }), f = a.overPos;
            a.slow && (d *= 10);
            var g = Dd;
            he.activeFrame = Dd = yd[Ie];
            var i = g === Dd;
            md(Cd, Dd.i !== yd[x(Fd)].i), rc(Ed, "stage"), uc(Oc ? [ Gd ] : [ Gd, T(Gd), _(Gd) ]), 
            rd("go", !0), i || fd("show", {
                user: a.user,
                time: d
            }), be = !0;
            var j = he.show.onEnd = function(b) {
                if (!j.ok) {
                    if (j.ok = !0, b || bd(!0), !i && (fd("showend", {
                        user: a.user
                    }), !b && Vd && Vd !== e.transition)) return he.setOptions({
                        transition: Vd
                    }), void (Vd = !1);
                    jc(), Pb(Ed, "stage"), rd("go", !1), Fc(), od(), id(), jd();
                }
            };
            if (Rd) {
                var k = Dd[Zc], l = Ie !== Hd ? yd[Hd][Zc] : null;
                $(k, l, se, {
                    time: d,
                    method: e.transition,
                    onEnd: j
                }, Se);
            } else Z(re, {
                pos: -q(Gd, Ke.w, e.margin, Fd),
                overPos: f,
                time: d,
                onEnd: j
            });
            if (Ec(), Md) {
                Qc();
                var m = y(Ie + h(Gd - Hd, -1, 1));
                Nc({
                    time: d,
                    coo: m !== Ie && a.coo,
                    guessIndex: "undefined" != typeof a.coo ? m : Ie,
                    keep: i
                }), Nd && Jc(d);
            }
            return ae = "undefined" != typeof Hd && Hd !== Ie, Hd = Ie, e.hash && ae && !he.eq && G(Dd.id || Ie + 1), 
            this;
        }, he.requestFullScreen = function() {
            return Pd && !he.fullScreen && ($d = Gc.scrollTop(), _d = Gc.scrollLeft(), P(Gc), 
            rd("x", !0), de = d.extend({}, Ke), a.addClass(Xb).appendTo(Bc.addClass(hb)), Ac.addClass(hb), 
            md(Cd, !0, !0), he.fullScreen = !0, Qd && tc.request(ke), he.resize(), Pb(Ed, "stage"), 
            jc(), fd("fullscreenenter")), this;
        }, he.cancelFullScreen = function() {
            return Qd && tc.is() ? tc.cancel(b) : kd(), this;
        }, he.toggleFullScreen = function() {
            return he[(he.fullScreen ? "cancel" : "request") + "FullScreen"]();
        }, S(b, tc.event, function() {
            !yd || tc.is() || Cd || kd();
        }), he.resize = function(a) {
            if (!yd) return this;
            var b = arguments[1] || 0, c = arguments[2];
            dd(he.fullScreen ? {
                width: "100%",
                maxwidth: null,
                minwidth: null,
                height: "100%",
                maxheight: null,
                minheight: null
            } : Q(a), [ Ke, c || he.fullScreen || e ]);
            var d = Ke.width, f = Ke.height, g = Ke.ratio, i = Gc.height() - (Md ? xe.height() : 0);
            return p(d) && (pe.addClass(sb).css({
                width: d,
                minWidth: Ke.minwidth || 0,
                maxWidth: Ke.maxwidth || cd
            }), d = Ke.W = Ke.w = pe.width(), Ke.nw = Md && o(e.navwidth, d) || d, e.glimpse && (Ke.w -= Math.round(2 * (o(e.glimpse, d) || 0))), 
            re.css({
                width: Ke.w,
                marginLeft: (Ke.W - Ke.w) / 2
            }), f = o(f, i), f = f || g && d / g, f && (d = Math.round(d), f = Ke.h = Math.round(h(f, o(Ke.minheight, i), o(Ke.maxheight, i))), 
            qe.stop().animate({
                width: d,
                height: f
            }, b, function() {
                pe.removeClass(sb);
            }), bd(), Md && (xe.stop().animate({
                width: Ke.nw
            }, b), Nc({
                guessIndex: Ie,
                time: b,
                keep: !0
            }), Nd && wc.nav && Jc(b)), Zd = c || !0, xd())), Re = qe.offset().left, this;
        }, he.setOptions = function(a) {
            return d.extend(e, a), vd(), this;
        }, he.shuffle = function() {
            return yd && N(yd) && vd(), this;
        }, he.destroy = function() {
            return he.cancelFullScreen(), he.stopAutoplay(), yd = he.data = null, j(), Ed = [], 
            Vc(Zc), vd.ok = !1, this;
        }, he.playVideo = function() {
            var a = Dd, b = a.video, c = Ie;
            return "object" == typeof b && a.videoReady && (Qd && he.fullScreen && he.cancelFullScreen(), 
            F(function() {
                return !tc.is() || c !== Ie;
            }, function() {
                c === Ie && (a.$video = a.$video || d(d.Fotorama.jst.video(b)), a.$video.appendTo(a[Zc]), 
                pe.addClass(lb), Cd = a.$video, s(), ve.blur(), Ce.blur(), fd("loadvideo"));
            })), this;
        }, he.stopVideo = function() {
            return md(Cd, !0, !0), this;
        }, qe.on("mousemove", od), Le = bb(re, {
            onStart: gd,
            onMove: function(a, b) {
                ld(qe, b.edge);
            },
            onTouchEnd: hd,
            onEnd: function(a) {
                ld(qe);
                var b = (Pc && !fe || a.touch) && e.arrows && "always" !== e.arrows;
                if (a.moved || b && a.pos !== a.newPos && !a.control) {
                    var c = r(a.newPos, Ke.w, e.margin, Fd);
                    he.show({
                        index: c,
                        time: Rd ? Ud : a.time,
                        overPos: a.overPos,
                        user: !0
                    });
                } else a.aborted || a.control || qd(a.startEvent, b);
            },
            timeLow: 1,
            timeHigh: 1,
            friction: 2,
            select: "." + Vb + ", ." + Vb + " *",
            $wrap: qe
        }), Ne = bb(ye, {
            onStart: gd,
            onMove: function(a, b) {
                ld(xe, b.edge);
            },
            onTouchEnd: hd,
            onEnd: function(a) {
                function b() {
                    Nc.l = a.newPos, id(), jd(), vc(a.newPos, !0);
                }
                if (a.moved) a.pos !== a.newPos ? (be = !0, Z(ye, {
                    time: a.time,
                    pos: a.newPos,
                    overPos: a.overPos,
                    onEnd: b
                }), vc(a.newPos), Wd && ld(xe, J(a.newPos, Ne.min, Ne.max))) : b(); else {
                    var c = a.$target.closest("." + Kb, ye)[0];
                    c && sd.call(c, a.startEvent);
                }
            },
            timeLow: .5,
            timeHigh: 2,
            friction: 5,
            $wrap: xe
        }), Me = cb(qe, {
            shift: !0,
            onEnd: function(a, b) {
                gd(), hd(), he.show({
                    index: b,
                    slow: a.altKey
                });
            }
        }), Oe = cb(xe, {
            onEnd: function(a, b) {
                gd(), hd();
                var c = u(ye) + .25 * b;
                ye.css(k(h(c, Ne.min, Ne.max))), Wd && ld(xe, J(c, Ne.min, Ne.max)), Oe.prevent = {
                    "<": c >= Ne.max,
                    ">": c <= Ne.min
                }, clearTimeout(Oe.t), Oe.t = setTimeout(function() {
                    Nc.l = c, vc(c, !0);
                }, Rc), vc(c);
            }
        }), pe.hover(function() {
            setTimeout(function() {
                ee || nd(!(fe = !0));
            }, 0);
        }, function() {
            fe && nd(!(fe = !1));
        }), L(ve, function(a) {
            X(a), td.call(this, a);
        }, {
            onStart: function() {
                gd(), Le.control = !0;
            },
            onTouchEnd: hd
        }), ve.each(function() {
            V(this, function(a) {
                td.call(this, a);
            }), ud(this);
        }), V(De, he.toggleFullScreen), ud(De), d.each("load push pop shift unshift reverse sort splice".split(" "), function(a, b) {
            he[b] = function() {
                return yd = yd || [], "load" !== b ? Array.prototype[b].apply(yd, arguments) : arguments[0] && "object" == typeof arguments[0] && arguments[0].length && (yd = O(arguments[0])), 
                vd(), he;
            };
        }), vd();
    }, d.fn.fotorama = function(b) {
        return this.each(function() {
            var c = this, e = d(this), f = e.data(), g = f.fotorama;
            g ? g.setOptions(b, !0) : F(function() {
                return !D(c);
            }, function() {
                f.urtext = e.html(), new d.Fotorama(e, d.extend({}, dd, a.fotoramaDefaults, b, f));
            });
        });
    }, d.Fotorama.instances = [], d.Fotorama.cache = {}, d.Fotorama.measures = {}, d = d || {}, 
    d.Fotorama = d.Fotorama || {}, d.Fotorama.jst = d.Fotorama.jst || {}, d.Fotorama.jst.style = function(a) {
        var b, c = "";
        return rc.escape, c += ".fotorama" + (null == (b = a.s) ? "" : b) + " .fotorama__nav--thumbs .fotorama__nav__frame{\npadding:" + (null == (b = a.m) ? "" : b) + "px;\nheight:" + (null == (b = a.h) ? "" : b) + "px}\n.fotorama" + (null == (b = a.s) ? "" : b) + " .fotorama__thumb-border{\nheight:" + (null == (b = a.h - a.b * (a.q ? 0 : 2)) ? "" : b) + "px;\nborder-width:" + (null == (b = a.b) ? "" : b) + "px;\nmargin-top:" + (null == (b = a.m) ? "" : b) + "px}";
    }, d.Fotorama.jst.video = function(a) {
        function b() {
            c += d.call(arguments, "");
        }
        var c = "", d = (rc.escape, Array.prototype.join);
        return c += '<div class="fotorama__video"><iframe src="', b(("youtube" == a.type ? a.p + "youtube.com/embed/" + a.id + "?autoplay=1" : "vimeo" == a.type ? a.p + "player.vimeo.com/video/" + a.id + "?autoplay=1&badge=0" : a.id) + (a.s && "custom" != a.type ? "&" + a.s : "")), 
        c += '" frameborder="0" allowfullscreen></iframe></div>\n';
    }, d(function() {
        d("." + gb + ':not([data-auto="false"])').fotorama();
    });
}(window, document, location, "undefined" != typeof jQuery && jQuery), InfoBox.prototype = new google.maps.OverlayView(), 
InfoBox.prototype.createInfoBoxDiv_ = function() {
    var a, b = this, c = function(a) {
        a.cancelBubble = !0, a.stopPropagation && a.stopPropagation();
    }, d = function(a) {
        a.returnValue = !1, a.preventDefault && a.preventDefault(), b.enableEventPropagation_ || c(a);
    };
    this.div_ || (this.div_ = document.createElement("div"), this.setBoxStyle_(), "undefined" == typeof this.content_.nodeType ? this.div_.innerHTML = this.getCloseBoxImg_() + this.content_ : (this.div_.innerHTML = this.getCloseBoxImg_(), 
    this.div_.appendChild(this.content_)), this.getPanes()[this.pane_].appendChild(this.div_), 
    this.addClickHandler_(), this.div_.style.width ? this.fixedWidthSet_ = !0 : 0 !== this.maxWidth_ && this.div_.offsetWidth > this.maxWidth_ ? (this.div_.style.width = this.maxWidth_, 
    this.div_.style.overflow = "auto", this.fixedWidthSet_ = !0) : (a = this.getBoxWidths_(), 
    this.div_.style.width = this.div_.offsetWidth - a.left - a.right + "px", this.fixedWidthSet_ = !1), 
    this.panBox_(this.disableAutoPan_), this.enableEventPropagation_ || (this.eventListener1_ = google.maps.event.addDomListener(this.div_, "mousedown", c), 
    this.eventListener2_ = google.maps.event.addDomListener(this.div_, "click", c), 
    this.eventListener3_ = google.maps.event.addDomListener(this.div_, "dblclick", c), 
    this.eventListener4_ = google.maps.event.addDomListener(this.div_, "mouseover", function() {
        this.style.cursor = "default";
    })), this.contextListener_ = google.maps.event.addDomListener(this.div_, "contextmenu", d), 
    google.maps.event.trigger(this, "domready"));
}, InfoBox.prototype.getCloseBoxImg_ = function() {
    var a = "";
    return "" !== this.closeBoxURL_ && (a = "<img", a += " src='" + this.closeBoxURL_ + "'", 
    a += " align=right", a += " style='", a += " position: relative;", a += " cursor: pointer;", 
    a += " margin: " + this.closeBoxMargin_ + ";", a += "'>"), a;
}, InfoBox.prototype.addClickHandler_ = function() {
    var a;
    "" !== this.closeBoxURL_ ? (a = this.div_.firstChild, this.closeListener_ = google.maps.event.addDomListener(a, "click", this.getCloseClickHandler_())) : this.closeListener_ = null;
}, InfoBox.prototype.getCloseClickHandler_ = function() {
    var a = this;
    return function(b) {
        b.cancelBubble = !0, b.stopPropagation && b.stopPropagation(), a.close(), google.maps.event.trigger(a, "closeclick");
    };
}, InfoBox.prototype.panBox_ = function(a) {
    var b, c, d = 0, e = 0;
    if (!a && (b = this.getMap(), b instanceof google.maps.Map)) {
        b.getBounds().contains(this.position_) || b.setCenter(this.position_), c = b.getBounds();
        var f = b.getDiv(), g = f.offsetWidth, h = f.offsetHeight, i = this.pixelOffset_.width, j = this.pixelOffset_.height, k = this.div_.offsetWidth, l = this.div_.offsetHeight, m = this.infoBoxClearance_.width, n = this.infoBoxClearance_.height, o = this.getProjection().fromLatLngToContainerPixel(this.position_);
        if (o.x < -i + m ? d = o.x + i - m : o.x + k + i + m > g && (d = o.x + k + i + m - g), 
        this.alignBottom_ ? o.y < -j + n + l ? e = o.y + j - n - l : o.y + j + n > h && (e = o.y + j + n - h) : o.y < -j + n ? e = o.y + j - n : o.y + l + j + n > h && (e = o.y + l + j + n - h), 
        0 !== d || 0 !== e) {
            {
                b.getCenter();
            }
            b.panBy(d, e);
        }
    }
}, InfoBox.prototype.setBoxStyle_ = function() {
    var a, b;
    if (this.div_) {
        this.div_.className = this.boxClass_, this.div_.style.cssText = "", b = this.boxStyle_;
        for (a in b) b.hasOwnProperty(a) && (this.div_.style[a] = b[a]);
        "undefined" != typeof this.div_.style.opacity && "" !== this.div_.style.opacity && (this.div_.style.filter = "alpha(opacity=" + 100 * this.div_.style.opacity + ")"), 
        this.div_.style.position = "absolute", this.div_.style.visibility = "hidden", null !== this.zIndex_ && (this.div_.style.zIndex = this.zIndex_);
    }
}, InfoBox.prototype.getBoxWidths_ = function() {
    var a, b = {
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    }, c = this.div_;
    return document.defaultView && document.defaultView.getComputedStyle ? (a = c.ownerDocument.defaultView.getComputedStyle(c, ""), 
    a && (b.top = parseInt(a.borderTopWidth, 10) || 0, b.bottom = parseInt(a.borderBottomWidth, 10) || 0, 
    b.left = parseInt(a.borderLeftWidth, 10) || 0, b.right = parseInt(a.borderRightWidth, 10) || 0)) : document.documentElement.currentStyle && c.currentStyle && (b.top = parseInt(c.currentStyle.borderTopWidth, 10) || 0, 
    b.bottom = parseInt(c.currentStyle.borderBottomWidth, 10) || 0, b.left = parseInt(c.currentStyle.borderLeftWidth, 10) || 0, 
    b.right = parseInt(c.currentStyle.borderRightWidth, 10) || 0), b;
}, InfoBox.prototype.onRemove = function() {
    this.div_ && (this.div_.parentNode.removeChild(this.div_), this.div_ = null);
}, InfoBox.prototype.draw = function() {
    this.createInfoBoxDiv_();
    var a = this.getProjection().fromLatLngToDivPixel(this.position_);
    this.div_.style.left = a.x + this.pixelOffset_.width + "px", this.alignBottom_ ? this.div_.style.bottom = -(a.y + this.pixelOffset_.height) + "px" : this.div_.style.top = a.y + this.pixelOffset_.height + "px", 
    this.div_.style.visibility = this.isHidden_ ? "hidden" : "visible";
}, InfoBox.prototype.setOptions = function(a) {
    "undefined" != typeof a.boxClass && (this.boxClass_ = a.boxClass, this.setBoxStyle_()), 
    "undefined" != typeof a.boxStyle && (this.boxStyle_ = a.boxStyle, this.setBoxStyle_()), 
    "undefined" != typeof a.content && this.setContent(a.content), "undefined" != typeof a.disableAutoPan && (this.disableAutoPan_ = a.disableAutoPan), 
    "undefined" != typeof a.maxWidth && (this.maxWidth_ = a.maxWidth), "undefined" != typeof a.pixelOffset && (this.pixelOffset_ = a.pixelOffset), 
    "undefined" != typeof a.alignBottom && (this.alignBottom_ = a.alignBottom), "undefined" != typeof a.position && this.setPosition(a.position), 
    "undefined" != typeof a.zIndex && this.setZIndex(a.zIndex), "undefined" != typeof a.closeBoxMargin && (this.closeBoxMargin_ = a.closeBoxMargin), 
    "undefined" != typeof a.closeBoxURL && (this.closeBoxURL_ = a.closeBoxURL), "undefined" != typeof a.infoBoxClearance && (this.infoBoxClearance_ = a.infoBoxClearance), 
    "undefined" != typeof a.isHidden && (this.isHidden_ = a.isHidden), "undefined" != typeof a.enableEventPropagation && (this.enableEventPropagation_ = a.enableEventPropagation), 
    this.div_ && this.draw();
}, InfoBox.prototype.setContent = function(a) {
    this.content_ = a, this.div_ && (this.closeListener_ && (google.maps.event.removeListener(this.closeListener_), 
    this.closeListener_ = null), this.fixedWidthSet_ || (this.div_.style.width = ""), 
    "undefined" == typeof a.nodeType ? this.div_.innerHTML = this.getCloseBoxImg_() + a : (this.div_.innerHTML = this.getCloseBoxImg_(), 
    this.div_.appendChild(a)), this.fixedWidthSet_ || (this.div_.style.width = this.div_.offsetWidth + "px", 
    "undefined" == typeof a.nodeType ? this.div_.innerHTML = this.getCloseBoxImg_() + a : (this.div_.innerHTML = this.getCloseBoxImg_(), 
    this.div_.appendChild(a))), this.addClickHandler_()), google.maps.event.trigger(this, "content_changed");
}, InfoBox.prototype.setPosition = function(a) {
    this.position_ = a, this.div_ && this.draw(), google.maps.event.trigger(this, "position_changed");
}, InfoBox.prototype.setZIndex = function(a) {
    this.zIndex_ = a, this.div_ && (this.div_.style.zIndex = a), google.maps.event.trigger(this, "zindex_changed");
}, InfoBox.prototype.getContent = function() {
    return this.content_;
}, InfoBox.prototype.getPosition = function() {
    return this.position_;
}, InfoBox.prototype.getZIndex = function() {
    return this.zIndex_;
}, InfoBox.prototype.show = function() {
    this.isHidden_ = !1, this.div_ && (this.div_.style.visibility = "visible");
}, InfoBox.prototype.hide = function() {
    this.isHidden_ = !0, this.div_ && (this.div_.style.visibility = "hidden");
}, InfoBox.prototype.open = function(a, b) {
    var c = this;
    b && (this.position_ = b.getPosition(), this.moveListener_ = google.maps.event.addListener(b, "position_changed", function() {
        c.setPosition(this.getPosition());
    })), this.setMap(a), this.div_ && this.panBox_();
}, InfoBox.prototype.close = function() {
    this.closeListener_ && (google.maps.event.removeListener(this.closeListener_), this.closeListener_ = null), 
    this.eventListener1_ && (google.maps.event.removeListener(this.eventListener1_), 
    google.maps.event.removeListener(this.eventListener2_), google.maps.event.removeListener(this.eventListener3_), 
    google.maps.event.removeListener(this.eventListener4_), this.eventListener1_ = null, 
    this.eventListener2_ = null, this.eventListener3_ = null, this.eventListener4_ = null), 
    this.moveListener_ && (google.maps.event.removeListener(this.moveListener_), this.moveListener_ = null), 
    this.contextListener_ && (google.maps.event.removeListener(this.contextListener_), 
    this.contextListener_ = null), this.setMap(null);
}, function(a) {
    function b(a, b, c) {
        if ((a[b] || a[c]) && a[b] === a[c]) throw Error("(Link) '" + b + "' can't match '" + c + "'.'");
    }
    function c(c) {
        if (void 0 === c && (c = {}), "object" != typeof c) throw Error("(Format) 'format' option must be an object.");
        var d = {};
        a(e).each(function(a, b) {
            if (void 0 === c[b]) d[b] = f[a]; else {
                if (typeof c[b] != typeof f[a]) throw Error("(Format) 'format." + b + "' must be a " + typeof f[a] + ".");
                if ("decimals" === b && (0 > c[b] || 7 < c[b])) throw Error("(Format) 'format.decimals' option must be between 0 and 7.");
                d[b] = c[b];
            }
        }), b(d, "mark", "thousand"), b(d, "prefix", "negative"), b(d, "prefix", "negativeBefore"), 
        this.r = d;
    }
    function d(b, c) {
        return "object" != typeof b && a.error("(Link) Initialize with an object."), new d.prototype.p(b.target || function() {}, b.method, b.format || {}, c);
    }
    var e = "decimals mark thousand prefix postfix encoder decoder negative negativeBefore to from".split(" "), f = [ 2, ".", "", "", "", function(a) {
        return a;
    }, function(a) {
        return a;
    }, "-", "", function(a) {
        return a;
    }, function(a) {
        return a;
    } ];
    c.prototype.a = function(a) {
        return this.r[a];
    }, c.prototype.L = function(a) {
        function b(a) {
            return a.split("").reverse().join("");
        }
        a = this.a("encoder")(a);
        var c = this.a("decimals"), d = "", e = "", f = "", g = "";
        return 0 === parseFloat(a.toFixed(c)) && (a = "0"), 0 > a && (d = this.a("negative"), 
        e = this.a("negativeBefore")), a = Math.abs(a).toFixed(c).toString(), a = a.split("."), 
        this.a("thousand") ? (f = b(a[0]).match(/.{1,3}/g), f = b(f.join(b(this.a("thousand"))))) : f = a[0], 
        this.a("mark") && 1 < a.length && (g = this.a("mark") + a[1]), this.a("to")(e + this.a("prefix") + d + f + g + this.a("postfix"));
    }, c.prototype.w = function(a) {
        function b(a) {
            return a.replace(/[\-\/\\\^$*+?.()|\[\]{}]/g, "\\$&");
        }
        var c;
        return null === a || void 0 === a ? !1 : (a = this.a("from")(a), a = a.toString(), 
        c = a.replace(RegExp("^" + b(this.a("negativeBefore"))), ""), a !== c ? (a = c, 
        c = "-") : c = "", a = a.replace(RegExp("^" + b(this.a("prefix"))), ""), this.a("negative") && (c = "", 
        a = a.replace(RegExp("^" + b(this.a("negative"))), "-")), a = a.replace(RegExp(b(this.a("postfix")) + "$"), "").replace(RegExp(b(this.a("thousand")), "g"), "").replace(this.a("mark"), "."), 
        a = this.a("decoder")(parseFloat(c + a)), isNaN(a) ? !1 : a);
    }, d.prototype.K = function(b, c) {
        this.method = c || "html", this.j = a(b.replace("-tooltip-", "") || "<div/>")[0];
    }, d.prototype.H = function(a) {
        this.method = "val", this.j = document.createElement("input"), this.j.name = a, 
        this.j.type = "hidden";
    }, d.prototype.G = function(b) {
        function c(a, b) {
            return [ b ? null : a, b ? a : null ];
        }
        var d = this;
        this.method = "val", this.target = b.on("change", function(b) {
            d.B.val(c(a(b.target).val(), d.t), {
                link: d,
                set: !0
            });
        });
    }, d.prototype.p = function(b, c, d, e) {
        if (this.g = d, this.update = !e, "string" == typeof b && 0 === b.indexOf("-tooltip-")) this.K(b, c); else if ("string" == typeof b && 0 !== b.indexOf("-")) this.H(b); else {
            if ("function" != typeof b) {
                if (b instanceof a || a.zepto && a.zepto.isZ(b)) {
                    if (!c) {
                        if (b.is("input, select, textarea")) return void this.G(b);
                        c = "html";
                    }
                    if ("function" == typeof c || "string" == typeof c && b[c]) return this.method = c, 
                    void (this.target = b);
                }
                throw new RangeError("(Link) Invalid Link.");
            }
            this.target = !1, this.method = b;
        }
    }, d.prototype.write = function(a, b, c, d) {
        this.update && !1 === d || (this.u = a, this.F = a = this.format(a), "function" == typeof this.method ? this.method.call(this.target[0] || c[0], a, b, c) : this.target[this.method](a, b, c));
    }, d.prototype.q = function(b) {
        this.g = new c(a.extend({}, b, this.g instanceof c ? this.g.r : this.g));
    }, d.prototype.J = function(a) {
        this.B = a;
    }, d.prototype.I = function(a) {
        this.t = a;
    }, d.prototype.format = function(a) {
        return this.g.L(a);
    }, d.prototype.A = function(a) {
        return this.g.w(a);
    }, d.prototype.p.prototype = d.prototype, a.Link = d;
}(window.jQuery || window.Zepto), function(a) {
    function b(a) {
        return "number" == typeof a && !isNaN(a) && isFinite(a);
    }
    function c(b) {
        return a.isArray(b) ? b : [ b ];
    }
    function d(a, b) {
        a.addClass(b), setTimeout(function() {
            a.removeClass(b);
        }, 300);
    }
    function e(a, b) {
        return 100 * b / (a[1] - a[0]);
    }
    function f(a, b) {
        if (b >= a.d.slice(-1)[0]) return 100;
        for (var c, d, f, g = 1; b >= a.d[g]; ) g++;
        return c = a.d[g - 1], d = a.d[g], f = a.c[g - 1], c = [ c, d ], f + e(c, 0 > c[0] ? b + Math.abs(c[0]) : b - c[0]) / (100 / (a.c[g] - f));
    }
    function g(a, b) {
        if (b >= 100) return a.d.slice(-1)[0];
        for (var c, d, e, f = 1; b >= a.c[f]; ) f++;
        return c = a.d[f - 1], d = a.d[f], e = a.c[f - 1], c = [ c, d ], 100 / (a.c[f] - e) * (b - e) * (c[1] - c[0]) / 100 + c[0];
    }
    function h(a, b) {
        for (var c, d = 1; (a.dir ? 100 - b : b) >= a.c[d]; ) d++;
        return a.m ? (c = a.c[d - 1], d = a.c[d], b - c > (d - c) / 2 ? d : c) : (a.h[d - 1] ? (c = a.h[d - 1], 
        d = a.c[d - 1] + Math.round((b - a.c[d - 1]) / c) * c) : d = b, d);
    }
    function i(a, c) {
        if (!b(c)) throw Error("noUiSlider: 'step' is not numeric.");
        a.h[0] = c;
    }
    function j(c, d) {
        if ("object" != typeof d || a.isArray(d)) throw Error("noUiSlider: 'range' is not an object.");
        if (void 0 === d.min || void 0 === d.max) throw Error("noUiSlider: Missing 'min' or 'max' in 'range'.");
        a.each(d, function(d, e) {
            var f;
            if ("number" == typeof e && (e = [ e ]), !a.isArray(e)) throw Error("noUiSlider: 'range' contains invalid value.");
            if (f = "min" === d ? 0 : "max" === d ? 100 : parseFloat(d), !b(f) || !b(e[0])) throw Error("noUiSlider: 'range' value isn't numeric.");
            c.c.push(f), c.d.push(e[0]), f ? c.h.push(isNaN(e[1]) ? !1 : e[1]) : isNaN(e[1]) || (c.h[0] = e[1]);
        }), a.each(c.h, function(a, b) {
            return b ? void (c.h[a] = e([ c.d[a], c.d[a + 1] ], b) / (100 / (c.c[a + 1] - c.c[a]))) : !0;
        });
    }
    function k(b, c) {
        if ("number" == typeof c && (c = [ c ]), !a.isArray(c) || !c.length || 2 < c.length) throw Error("noUiSlider: 'start' option is incorrect.");
        b.b = c.length, b.start = c;
    }
    function l(a, b) {
        if (a.m = b, "boolean" != typeof b) throw Error("noUiSlider: 'snap' option must be a boolean.");
    }
    function m(a, b) {
        if ("lower" === b && 1 === a.b) a.i = 1; else if ("upper" === b && 1 === a.b) a.i = 2; else if (!0 === b && 2 === a.b) a.i = 3; else {
            if (!1 !== b) throw Error("noUiSlider: 'connect' option doesn't match handle count.");
            a.i = 0;
        }
    }
    function n(a, b) {
        switch (b) {
          case "horizontal":
            a.k = 0;
            break;

          case "vertical":
            a.k = 1;
            break;

          default:
            throw Error("noUiSlider: 'orientation' option is invalid.");
        }
    }
    function o(a, c) {
        if (2 < a.c.length) throw Error("noUiSlider: 'margin' option is only supported on linear sliders.");
        if (a.margin = e(a.d, c), !b(c)) throw Error("noUiSlider: 'margin' option must be numeric.");
    }
    function p(a, b) {
        switch (b) {
          case "ltr":
            a.dir = 0;
            break;

          case "rtl":
            a.dir = 1, a.i = [ 0, 2, 1, 3 ][a.i];
            break;

          default:
            throw Error("noUiSlider: 'direction' option was not recognized.");
        }
    }
    function q(a, b) {
        if ("string" != typeof b) throw Error("noUiSlider: 'behaviour' must be a string containing options.");
        var c = 0 <= b.indexOf("snap");
        a.n = {
            s: 0 <= b.indexOf("tap") || c,
            extend: 0 <= b.indexOf("extend"),
            v: 0 <= b.indexOf("drag"),
            fixed: 0 <= b.indexOf("fixed"),
            m: c
        };
    }
    function r(b, c, d) {
        b.o = [ c.lower, c.upper ], b.g = c.format, a.each(b.o, function(b, e) {
            if (!a.isArray(e)) throw Error("noUiSlider: 'serialization." + (b ? "upper" : "lower") + "' must be an array.");
            a.each(e, function() {
                if (!(this instanceof a.Link)) throw Error("noUiSlider: 'serialization." + (b ? "upper" : "lower") + "' can only contain Link instances.");
                this.I(b), this.J(d), this.q(c.format);
            });
        }), b.dir && 1 < b.b && b.o.reverse();
    }
    function s(b, c) {
        var d, e = {
            c: [],
            d: [],
            h: [ !1 ],
            margin: 0
        };
        return d = {
            step: {
                e: !1,
                f: i
            },
            start: {
                e: !0,
                f: k
            },
            connect: {
                e: !0,
                f: m
            },
            direction: {
                e: !0,
                f: p
            },
            range: {
                e: !0,
                f: j
            },
            snap: {
                e: !1,
                f: l
            },
            orientation: {
                e: !1,
                f: n
            },
            margin: {
                e: !1,
                f: o
            },
            behaviour: {
                e: !0,
                f: q
            },
            serialization: {
                e: !0,
                f: r
            }
        }, b = a.extend({
            connect: !1,
            direction: "ltr",
            behaviour: "tap",
            orientation: "horizontal"
        }, b), b.serialization = a.extend({
            lower: [],
            upper: [],
            format: {}
        }, b.serialization), a.each(d, function(a, d) {
            if (void 0 === b[a]) {
                if (d.e) throw Error("noUiSlider: '" + a + "' is required.");
                return !0;
            }
            d.f(e, b[a], c);
        }), e.style = e.k ? "top" : "left", e;
    }
    function t(b, c) {
        var d = a("<div><div/></div>").addClass(G[2]), e = [ "-lower", "-upper" ];
        return b.dir && e.reverse(), d.children().addClass(G[3] + " " + G[3] + e[c]), d;
    }
    function u(b, c) {
        return c.j && (c = new a.Link({
            target: a(c.j).clone().appendTo(b),
            method: c.method,
            format: c.g
        }, !0)), c;
    }
    function v(b, c) {
        var d, e = [];
        for (d = 0; d < b.b; d++) {
            var f = e, g = d, h = b.o[d], i = c[d].children(), j = b.g, k = void 0, l = [], k = new a.Link({}, !0);
            for (k.q(j), l.push(k), k = 0; k < h.length; k++) l.push(u(i, h[k]));
            f[g] = l;
        }
        return e;
    }
    function w(a, b, c) {
        switch (a) {
          case 1:
            b.addClass(G[7]), c[0].addClass(G[6]);
            break;

          case 3:
            c[1].addClass(G[6]);

          case 2:
            c[0].addClass(G[7]);

          case 0:
            b.addClass(G[6]);
        }
    }
    function x(a, b) {
        var c, d = [];
        for (c = 0; c < a.b; c++) d.push(t(a, c).appendTo(b));
        return d;
    }
    function y(b, c) {
        return c.addClass([ G[0], G[8 + b.dir], G[4 + b.k] ].join(" ")), a("<div/>").appendTo(c).addClass(G[1]);
    }
    function z(b, e, i) {
        function j() {
            return t[[ "width", "height" ][e.k]]();
        }
        function k(a) {
            var b, c = [ A.val() ];
            for (b = 0; b < a.length; b++) A.trigger(a[b], c);
        }
        function l(b, c, d) {
            var f = b[0] !== z[0][0] ? 1 : 0, i = B[0] + e.margin, j = B[1] - e.margin;
            return d && 1 < z.length && (c = f ? Math.max(c, i) : Math.min(c, j)), 100 > c && (c = h(e, c)), 
            c = Math.max(Math.min(parseFloat(c.toFixed(7)), 100), 0), c === B[f] ? 1 === z.length ? !1 : c === i || c === j ? 0 : !1 : (b.css(e.style, c + "%"), 
            b.is(":first-child") && b.toggleClass(G[17], c > 50), B[f] = c, e.dir && (c = 100 - c), 
            a(u[f]).each(function() {
                this.write(g(e, c), b.children(), A);
            }), !0);
        }
        function m(a, b, c) {
            c || d(A, G[14]), l(a, b, !1), k([ "slide", "set", "change" ]);
        }
        function n(a, b, c, d) {
            a = a.replace(/\s/g, ".nui ") + ".nui", b.on(a, function(a) {
                var b = A.attr("disabled");
                if (A.hasClass(G[14]) || void 0 !== b && null !== b) return !1;
                a.preventDefault();
                var f, g, b = 0 === a.type.indexOf("touch"), h = 0 === a.type.indexOf("mouse"), i = 0 === a.type.indexOf("pointer"), j = a;
                0 === a.type.indexOf("MSPointer") && (i = !0), a.originalEvent && (a = a.originalEvent), 
                b && (f = a.changedTouches[0].pageX, g = a.changedTouches[0].pageY), (h || i) && (i || void 0 !== window.pageXOffset || (window.pageXOffset = document.documentElement.scrollLeft, 
                window.pageYOffset = document.documentElement.scrollTop), f = a.clientX + window.pageXOffset, 
                g = a.clientY + window.pageYOffset), j.C = [ f, g ], j.cursor = h, a = j, a.l = a.C[e.k], 
                c(a, d);
            });
        }
        function o(a, b) {
            var c, d = b.b || z, e = !1, e = 100 * (a.l - b.start) / j(), f = d[0][0] !== z[0][0] ? 1 : 0, g = b.D;
            c = e + g[0], e += g[1], 1 < d.length ? (0 > c && (e += Math.abs(c)), e > 100 && (c -= e - 100), 
            c = [ Math.max(Math.min(c, 100), 0), Math.max(Math.min(e, 100), 0) ]) : c = [ c, e ], 
            e = l(d[0], c[f], 1 === d.length), 1 < d.length && (e = l(d[1], c[f ? 0 : 1], !1) || e), 
            e && k([ "slide" ]);
        }
        function p(b) {
            a("." + G[15]).removeClass(G[15]), b.cursor && a("body").css("cursor", "").off(".nui"), 
            D.off(".nui"), A.removeClass(G[12]), k([ "set", "change" ]);
        }
        function q(b, c) {
            1 === c.b.length && c.b[0].children().addClass(G[15]), b.stopPropagation(), n(F.move, D, o, {
                start: b.l,
                b: c.b,
                D: [ B[0], B[z.length - 1] ]
            }), n(F.end, D, p, null), b.cursor && (a("body").css("cursor", a(b.target).css("cursor")), 
            1 < z.length && A.addClass(G[12]), a("body").on("selectstart.nui", !1));
        }
        function r(b) {
            var c = b.l, d = 0;
            b.stopPropagation(), a.each(z, function() {
                d += this.offset()[e.style];
            }), d = d / 2 > c || 1 === z.length ? 0 : 1, c -= t.offset()[e.style], c = 100 * c / j(), 
            m(z[d], c, e.n.m), e.n.m && q(b, {
                b: [ z[d] ]
            });
        }
        function s(a) {
            var b = (a = a.l < t.offset()[e.style]) ? 0 : 100;
            a = a ? 0 : z.length - 1, m(z[a], b, !1);
        }
        var t, u, z, A = a(b), B = [ -1, -1 ];
        if (A.hasClass(G[0])) throw Error("Slider was already initialized.");
        t = y(e, A), z = x(e, t), u = v(e, z), w(e.i, A, z), function(a) {
            var b;
            if (!a.fixed) for (b = 0; b < z.length; b++) n(F.start, z[b].children(), q, {
                b: [ z[b] ]
            });
            a.s && n(F.start, t, r, {
                b: z
            }), a.extend && (A.addClass(G[16]), a.s && n(F.start, A, s, {
                b: z
            })), a.v && (b = t.find("." + G[7]).addClass(G[10]), a.fixed && (b = b.add(t.children().not(b).children())), 
            n(F.start, b, q, {
                b: z
            }));
        }(e.n), b.vSet = function() {
            var b, g, h, i, j, m, n = Array.prototype.slice.call(arguments, 0), o = c(n[0]);
            for ("object" == typeof n[1] ? (b = n[1].set, g = n[1].link, h = n[1].update, i = n[1].animate) : !0 === n[1] && (b = !0), 
            e.dir && 1 < e.b && o.reverse(), i && d(A, G[14]), n = 1 < z.length ? 3 : 1, 1 === o.length && (n = 1), 
            j = 0; n > j; j++) i = g || u[j % 2][0], i = i.A(o[j % 2]), !1 !== i && (i = f(e, i), 
            e.dir && (i = 100 - i), !0 !== l(z[j % 2], i, !0) && a(u[j % 2]).each(function(a) {
                return a ? void this.write(m, z[j % 2].children(), A, h) : (m = this.u, !0);
            }));
            return !0 === b && k([ "set" ]), this;
        }, b.vGet = function() {
            var a, b = [];
            for (a = 0; a < e.b; a++) b[a] = u[a][0].F;
            return 1 === b.length ? b[0] : e.dir ? b.reverse() : b;
        }, b.destroy = function() {
            return a.each(u, function() {
                a.each(this, function() {
                    this.target && this.target.off(".nui");
                });
            }), a(this).off(".nui").removeClass(G.join(" ")).empty(), i;
        }, A.val(e.start);
    }
    function A(a) {
        if (!this.length) throw Error("noUiSlider: Can't initialize slider on empty selection.");
        var b = s(a, this);
        return this.each(function() {
            z(this, b, a);
        });
    }
    function B(b) {
        return this.each(function() {
            var c = a(this).val(), d = this.destroy(), e = a.extend({}, d, b);
            a(this).noUiSlider(e), d.start === e.start && a(this).val(c);
        });
    }
    function C() {
        return this[0][arguments.length ? "vSet" : "vGet"].apply(this[0], arguments);
    }
    var D = a(document), E = a.fn.val, F = window.navigator.pointerEnabled ? {
        start: "pointerdown",
        move: "pointermove",
        end: "pointerup"
    } : window.navigator.msPointerEnabled ? {
        start: "MSPointerDown",
        move: "MSPointerMove",
        end: "MSPointerUp"
    } : {
        start: "mousedown touchstart",
        move: "mousemove touchmove",
        end: "mouseup touchend"
    }, G = "noUi-target noUi-base noUi-origin noUi-handle noUi-horizontal noUi-vertical noUi-background noUi-connect noUi-ltr noUi-rtl noUi-dragable  noUi-state-drag  noUi-state-tap noUi-active noUi-extended noUi-stacking".split(" ");
    a.fn.val = function() {
        var b = arguments, c = a(this[0]);
        return arguments.length ? this.each(function() {
            (a(this).hasClass(G[0]) ? C : E).apply(a(this), b);
        }) : (c.hasClass(G[0]) ? C : E).call(c);
    }, a.noUiSlider = {
        Link: a.Link
    }, a.fn.noUiSlider = function(a, b) {
        return (b ? B : A).call(this, a);
    };
}(window.jQuery || window.Zepto), function(a) {
    "use strict";
    function b(a, b, c) {
        return a.addEventListener ? a.addEventListener(b, c, !1) : a.attachEvent ? a.attachEvent("on" + b, c) : void 0;
    }
    function c(a, b) {
        var c, d;
        for (c = 0, d = a.length; d > c; c++) if (a[c] === b) return !0;
        return !1;
    }
    function d(a, b) {
        var c;
        a.createTextRange ? (c = a.createTextRange(), c.move("character", b), c.select()) : a.selectionStart && (a.focus(), 
        a.setSelectionRange(b, b));
    }
    function e(a, b) {
        try {
            return a.type = b, !0;
        } catch (c) {
            return !1;
        }
    }
    a.Placeholders = {
        Utils: {
            addEventListener: b,
            inArray: c,
            moveCaret: d,
            changeType: e
        }
    };
}(this), function(a) {
    "use strict";
    function b() {}
    function c() {
        try {
            return document.activeElement;
        } catch (a) {}
    }
    function d(a, b) {
        var c, d, e = !!b && a.value !== b, f = a.value === a.getAttribute(H);
        return (e || f) && "true" === a.getAttribute(I) ? (a.removeAttribute(I), a.value = a.value.replace(a.getAttribute(H), ""), 
        a.className = a.className.replace(G, ""), d = a.getAttribute(O), parseInt(d, 10) >= 0 && (a.setAttribute("maxLength", d), 
        a.removeAttribute(O)), c = a.getAttribute(J), c && (a.type = c), !0) : !1;
    }
    function e(a) {
        var b, c, d = a.getAttribute(H);
        return "" === a.value && d ? (a.setAttribute(I, "true"), a.value = d, a.className += " " + F, 
        c = a.getAttribute(O), c || (a.setAttribute(O, a.maxLength), a.removeAttribute("maxLength")), 
        b = a.getAttribute(J), b ? a.type = "text" : "password" === a.type && T.changeType(a, "text") && a.setAttribute(J, "password"), 
        !0) : !1;
    }
    function f(a, b) {
        var c, d, e, f, g, h, i;
        if (a && a.getAttribute(H)) b(a); else for (e = a ? a.getElementsByTagName("input") : p, 
        f = a ? a.getElementsByTagName("textarea") : q, c = e ? e.length : 0, d = f ? f.length : 0, 
        i = 0, h = c + d; h > i; i++) g = c > i ? e[i] : f[i - c], b(g);
    }
    function g(a) {
        f(a, d);
    }
    function h(a) {
        f(a, e);
    }
    function i(a) {
        return function() {
            r && a.value === a.getAttribute(H) && "true" === a.getAttribute(I) ? T.moveCaret(a, 0) : d(a);
        };
    }
    function j(a) {
        return function() {
            e(a);
        };
    }
    function k(a) {
        return function(b) {
            return t = a.value, "true" === a.getAttribute(I) && t === a.getAttribute(H) && T.inArray(D, b.keyCode) ? (b.preventDefault && b.preventDefault(), 
            !1) : void 0;
        };
    }
    function l(a) {
        return function() {
            d(a, t), "" === a.value && (a.blur(), T.moveCaret(a, 0));
        };
    }
    function m(a) {
        return function() {
            a === c() && a.value === a.getAttribute(H) && "true" === a.getAttribute(I) && T.moveCaret(a, 0);
        };
    }
    function n(a) {
        return function() {
            g(a);
        };
    }
    function o(a) {
        a.form && (y = a.form, "string" == typeof y && (y = document.getElementById(y)), 
        y.getAttribute(K) || (T.addEventListener(y, "submit", n(y)), y.setAttribute(K, "true"))), 
        T.addEventListener(a, "focus", i(a)), T.addEventListener(a, "blur", j(a)), r && (T.addEventListener(a, "keydown", k(a)), 
        T.addEventListener(a, "keyup", l(a)), T.addEventListener(a, "click", m(a))), a.setAttribute(L, "true"), 
        a.setAttribute(H, w), (r || a !== c()) && e(a);
    }
    var p, q, r, s, t, u, v, w, x, y, z, A, B, C = [ "text", "search", "url", "tel", "email", "password", "number", "textarea" ], D = [ 27, 33, 34, 35, 36, 37, 38, 39, 40, 8, 46 ], E = "#ccc", F = "placeholdersjs", G = RegExp("(?:^|\\s)" + F + "(?!\\S)"), H = "data-placeholder-value", I = "data-placeholder-active", J = "data-placeholder-type", K = "data-placeholder-submit", L = "data-placeholder-bound", M = "data-placeholder-focus", N = "data-placeholder-live", O = "data-placeholder-maxlength", P = document.createElement("input"), Q = document.getElementsByTagName("head")[0], R = document.documentElement, S = a.Placeholders, T = S.Utils;
    if (S.nativeSupport = void 0 !== P.placeholder, !S.nativeSupport) {
        for (p = document.getElementsByTagName("input"), q = document.getElementsByTagName("textarea"), 
        r = "false" === R.getAttribute(M), s = "false" !== R.getAttribute(N), u = document.createElement("style"), 
        u.type = "text/css", v = document.createTextNode("." + F + " { color:" + E + "; }"), 
        u.styleSheet ? u.styleSheet.cssText = v.nodeValue : u.appendChild(v), Q.insertBefore(u, Q.firstChild), 
        B = 0, A = p.length + q.length; A > B; B++) z = p.length > B ? p[B] : q[B - p.length], 
        w = z.attributes.placeholder, w && (w = w.nodeValue, w && T.inArray(C, z.type) && o(z));
        x = setInterval(function() {
            for (B = 0, A = p.length + q.length; A > B; B++) z = p.length > B ? p[B] : q[B - p.length], 
            w = z.attributes.placeholder, w ? (w = w.nodeValue, w && T.inArray(C, z.type) && (z.getAttribute(L) || o(z), 
            (w !== z.getAttribute(H) || "password" === z.type && !z.getAttribute(J)) && ("password" === z.type && !z.getAttribute(J) && T.changeType(z, "text") && z.setAttribute(J, "password"), 
            z.value === z.getAttribute(H) && (z.value = w), z.setAttribute(H, w)))) : z.getAttribute(I) && (d(z), 
            z.removeAttribute(H));
            s || clearInterval(x);
        }, 100);
    }
    T.addEventListener(a, "beforeunload", function() {
        S.disable();
    }), S.disable = S.nativeSupport ? b : g, S.enable = S.nativeSupport ? b : h;
}(this), function() {
    for (var a, b = function() {}, c = "assert clear count debug dir dirxml error exception group groupCollapsed groupEnd info log markTimeline profile profileEnd table time timeEnd timeStamp trace warn".split(" "), d = c.length, e = window.console = window.console || {}; d--; ) a = c[d], 
    e[a] || (e[a] = b);
}(), function(a) {
    a.fn.loremImages = function(b, c, d) {
        var e = a.extend({}, a.fn.loremImages.defaults, d);
        return this.each(function(d, f) {
            var g = a(f), h = "";
            for (d = 0; d < e.count; d++) {
                var i = b + Math.round(Math.random() * e.randomWidth), j = c + Math.round(Math.random() * e.randomHeight);
                h += e.itemBuilder.call(g, d, "//lorempixel.com/" + (e.grey ? "g/" : "") + i + "/" + j + "/" + (e.category ? e.category + "/" : "") + "?" + Math.round(1e3 * Math.random()), i, j);
            }
            g.append(h);
        });
    }, a.fn.loremImages.defaults = {
        count: 10,
        grey: 0,
        randomWidth: 0,
        randomHeight: 0,
        category: 0,
        itemBuilder: function(a, b) {
            return '<img src="' + b + '" alt="Lorempixel">';
        }
    };
}(jQuery), jQuery.easing.jswing = jQuery.easing.swing, jQuery.extend(jQuery.easing, {
    def: "easeOutQuad",
    swing: function(a, b, c, d, e) {
        return jQuery.easing[jQuery.easing.def](a, b, c, d, e);
    },
    easeInQuad: function(a, b, c, d, e) {
        return d * (b /= e) * b + c;
    },
    easeOutQuad: function(a, b, c, d, e) {
        return -d * (b /= e) * (b - 2) + c;
    },
    easeInOutQuad: function(a, b, c, d, e) {
        return 1 > (b /= e / 2) ? d / 2 * b * b + c : -d / 2 * (--b * (b - 2) - 1) + c;
    },
    easeInCubic: function(a, b, c, d, e) {
        return d * (b /= e) * b * b + c;
    },
    easeOutCubic: function(a, b, c, d, e) {
        return d * ((b = b / e - 1) * b * b + 1) + c;
    },
    easeInOutCubic: function(a, b, c, d, e) {
        return 1 > (b /= e / 2) ? d / 2 * b * b * b + c : d / 2 * ((b -= 2) * b * b + 2) + c;
    },
    easeInQuart: function(a, b, c, d, e) {
        return d * (b /= e) * b * b * b + c;
    },
    easeOutQuart: function(a, b, c, d, e) {
        return -d * ((b = b / e - 1) * b * b * b - 1) + c;
    },
    easeInOutQuart: function(a, b, c, d, e) {
        return 1 > (b /= e / 2) ? d / 2 * b * b * b * b + c : -d / 2 * ((b -= 2) * b * b * b - 2) + c;
    },
    easeInQuint: function(a, b, c, d, e) {
        return d * (b /= e) * b * b * b * b + c;
    },
    easeOutQuint: function(a, b, c, d, e) {
        return d * ((b = b / e - 1) * b * b * b * b + 1) + c;
    },
    easeInOutQuint: function(a, b, c, d, e) {
        return 1 > (b /= e / 2) ? d / 2 * b * b * b * b * b + c : d / 2 * ((b -= 2) * b * b * b * b + 2) + c;
    },
    easeInSine: function(a, b, c, d, e) {
        return -d * Math.cos(b / e * (Math.PI / 2)) + d + c;
    },
    easeOutSine: function(a, b, c, d, e) {
        return d * Math.sin(b / e * (Math.PI / 2)) + c;
    },
    easeInOutSine: function(a, b, c, d, e) {
        return -d / 2 * (Math.cos(Math.PI * b / e) - 1) + c;
    },
    easeInExpo: function(a, b, c, d, e) {
        return 0 == b ? c : d * Math.pow(2, 10 * (b / e - 1)) + c;
    },
    easeOutExpo: function(a, b, c, d, e) {
        return b == e ? c + d : d * (-Math.pow(2, -10 * b / e) + 1) + c;
    },
    easeInOutExpo: function(a, b, c, d, e) {
        return 0 == b ? c : b == e ? c + d : 1 > (b /= e / 2) ? d / 2 * Math.pow(2, 10 * (b - 1)) + c : d / 2 * (-Math.pow(2, -10 * --b) + 2) + c;
    },
    easeInCirc: function(a, b, c, d, e) {
        return -d * (Math.sqrt(1 - (b /= e) * b) - 1) + c;
    },
    easeOutCirc: function(a, b, c, d, e) {
        return d * Math.sqrt(1 - (b = b / e - 1) * b) + c;
    },
    easeInOutCirc: function(a, b, c, d, e) {
        return 1 > (b /= e / 2) ? -d / 2 * (Math.sqrt(1 - b * b) - 1) + c : d / 2 * (Math.sqrt(1 - (b -= 2) * b) + 1) + c;
    },
    easeInElastic: function(a, b, c, d, e) {
        var a = 1.70158, f = 0, g = d;
        return 0 == b ? c : 1 == (b /= e) ? c + d : (f || (f = .3 * e), g < Math.abs(d) ? (g = d, 
        a = f / 4) : a = f / (2 * Math.PI) * Math.asin(d / g), -(g * Math.pow(2, 10 * (b -= 1)) * Math.sin(2 * (b * e - a) * Math.PI / f)) + c);
    },
    easeOutElastic: function(a, b, c, d, e) {
        var a = 1.70158, f = 0, g = d;
        return 0 == b ? c : 1 == (b /= e) ? c + d : (f || (f = .3 * e), g < Math.abs(d) ? (g = d, 
        a = f / 4) : a = f / (2 * Math.PI) * Math.asin(d / g), g * Math.pow(2, -10 * b) * Math.sin(2 * (b * e - a) * Math.PI / f) + d + c);
    },
    easeInOutElastic: function(a, b, c, d, e) {
        var a = 1.70158, f = 0, g = d;
        return 0 == b ? c : 2 == (b /= e / 2) ? c + d : (f || (f = .3 * e * 1.5), g < Math.abs(d) ? (g = d, 
        a = f / 4) : a = f / (2 * Math.PI) * Math.asin(d / g), 1 > b ? -.5 * g * Math.pow(2, 10 * (b -= 1)) * Math.sin(2 * (b * e - a) * Math.PI / f) + c : .5 * g * Math.pow(2, -10 * (b -= 1)) * Math.sin(2 * (b * e - a) * Math.PI / f) + d + c);
    },
    easeInBack: function(a, b, c, d, e, f) {
        return void 0 == f && (f = 1.70158), d * (b /= e) * b * ((f + 1) * b - f) + c;
    },
    easeOutBack: function(a, b, c, d, e, f) {
        return void 0 == f && (f = 1.70158), d * ((b = b / e - 1) * b * ((f + 1) * b + f) + 1) + c;
    },
    easeInOutBack: function(a, b, c, d, e, f) {
        return void 0 == f && (f = 1.70158), 1 > (b /= e / 2) ? d / 2 * b * b * (((f *= 1.525) + 1) * b - f) + c : d / 2 * ((b -= 2) * b * (((f *= 1.525) + 1) * b + f) + 2) + c;
    },
    easeInBounce: function(a, b, c, d, e) {
        return d - jQuery.easing.easeOutBounce(a, e - b, 0, d, e) + c;
    },
    easeOutBounce: function(a, b, c, d, e) {
        return (b /= e) < 1 / 2.75 ? 7.5625 * d * b * b + c : 2 / 2.75 > b ? d * (7.5625 * (b -= 1.5 / 2.75) * b + .75) + c : 2.5 / 2.75 > b ? d * (7.5625 * (b -= 2.25 / 2.75) * b + .9375) + c : d * (7.5625 * (b -= 2.625 / 2.75) * b + .984375) + c;
    },
    easeInOutBounce: function(a, b, c, d, e) {
        return e / 2 > b ? .5 * jQuery.easing.easeInBounce(a, 2 * b, 0, d, e) + c : .5 * jQuery.easing.easeOutBounce(a, 2 * b - e, 0, d, e) + .5 * d + c;
    }
}), function(a, b) {
    var c, d = a.jQuery || a.Cowboy || (a.Cowboy = {});
    d.throttle = c = function(a, c, e, f) {
        function g() {
            function d() {
                i = +new Date(), e.apply(j, l);
            }
            function g() {
                h = b;
            }
            var j = this, k = +new Date() - i, l = arguments;
            f && !h && d(), h && clearTimeout(h), f === b && k > a ? d() : c !== !0 && (h = setTimeout(f ? g : d, f === b ? a - k : a));
        }
        var h, i = 0;
        return "boolean" != typeof c && (f = e, e = c, c = b), d.guid && (g.guid = e.guid = e.guid || d.guid++), 
        g;
    }, d.debounce = function(a, d, e) {
        return e === b ? c(a, d, !1) : c(a, e, d !== !1);
    };
}(this), function(a) {
    a.fn.rating = function(b) {
        b = b || function() {}, this.each(function(c, d) {
            a(d).data("rating", {
                callback: b
            }).bind("init.rating", a.fn.rating.init).bind("set.rating", a.fn.rating.set).bind("hover.rating", a.fn.rating.hover).trigger("init.rating");
        });
    }, a.extend(a.fn.rating, {
        init: function() {
            for (var b = a(this), c = "", d = null, e = b.children(), f = 0, g = e.length; g > f; f++) c = c + '<a class="star" title="' + a(e[f]).val() + '" />', 
            a(e[f]).is(":checked") ? (d = a(e[f]).val(), console.log(1)) : console.log(0);
            e.hide(), b.append('<div class="stars">' + c + "</div>").trigger("set.rating", d), 
            a("a", b).bind("click", a.fn.rating.click), b.trigger("hover.rating");
        },
        set: function(b, c) {
            var d = a(this), e = a("a", d), f = void 0;
            c && (e.removeClass("fullStar"), f = e.filter(function() {
                return a(this).attr("title") == c ? a(this) : !1;
            }), f.addClass("fullStar").prevAll().addClass("fullStar"));
        },
        hover: function() {
            var b = a(this), c = a("a", b);
            a(this).hasClass("disabled") || (c.bind("mouseenter", function() {
                a(this).addClass("tmp_fs").prevAll().addClass("tmp_fs"), a(this).nextAll().addClass("tmp_es");
            }), c.bind("mouseleave", function() {
                a(this).removeClass("tmp_fs").prevAll().removeClass("tmp_fs"), a(this).nextAll().removeClass("tmp_es");
            }));
        },
        click: function(b) {
            b.preventDefault();
            var c = a(b.target), d = c.parent().parent(), e = d.children("input"), f = c.attr("title");
            d.hasClass("disabled") || (matchInput = e.filter(function() {
                return a(this).val() == f ? !0 : !1;
            }), matchInput.attr("checked", !0).siblings("input").attr("checked", !1), d.trigger("set.rating", matchInput.val()).data("rating").callback(f, b));
        }
    });
}(jQuery), function(a, b, c) {
    function d(b, z, A) {
        function B() {
            var c = 0, d = zb.length;
            if (rb.old = a.extend({}, rb), pb = mb ? 0 : nb[kb.horizontal ? "width" : "height"](), 
            ub = sb[kb.horizontal ? "width" : "height"](), qb = mb ? b : ob[kb.horizontal ? "outerWidth" : "outerHeight"](), 
            zb.length = 0, rb.start = 0, rb.end = Math.max(qb - pb, 0), Fb) {
                c = Bb.length, Ab = ob.children(kb.itemSelector), Bb.length = 0;
                var e, f = j(ob, kb.horizontal ? "paddingLeft" : "paddingTop"), g = j(ob, kb.horizontal ? "paddingRight" : "paddingBottom"), h = "border-box" === a(Ab).css("boxSizing"), i = "none" !== Ab.css("float"), l = 0, m = Ab.length - 1;
                qb = 0, Ab.each(function(b, c) {
                    var d = a(c), h = d[kb.horizontal ? "outerWidth" : "outerHeight"](), k = j(d, kb.horizontal ? "marginLeft" : "marginTop"), d = j(d, kb.horizontal ? "marginRight" : "marginBottom"), n = h + k + d, o = !k || !d, p = {};
                    p.el = c, p.size = o ? h : n, p.half = p.size / 2, p.start = qb + (o ? k : 0), p.center = p.start - Math.round(pb / 2 - p.size / 2), 
                    p.end = p.start - pb + p.size, b || (qb += f), qb += n, kb.horizontal || i || d && k && b > 0 && (qb -= Math.min(k, d)), 
                    b === m && (p.end += g, qb += g, l = o ? d : 0), Bb.push(p), e = p;
                }), ob[0].style[kb.horizontal ? "width" : "height"] = (h ? qb : qb - f - g) + "px", 
                qb -= l, Bb.length ? (rb.start = Bb[0][Db ? "center" : "start"], rb.end = Db ? e.center : qb > pb ? e.end : rb.start) : rb.start = rb.end = 0;
            }
            if (rb.center = Math.round(rb.end / 2 + rb.start / 2), a.extend(Cb, K(void 0)), 
            tb.length && ub > 0 && (kb.dynamicHandle ? (vb = rb.start === rb.end ? ub : Math.round(ub * pb / qb), 
            vb = k(vb, kb.minHandleSize, ub), tb[0].style[kb.horizontal ? "width" : "height"] = vb + "px") : vb = tb[kb.horizontal ? "outerWidth" : "outerHeight"](), 
            wb.end = ub - vb, Vb || E()), !mb && pb > 0) {
                var n = rb.start, h = "";
                if (Fb) a.each(Bb, function(a, b) {
                    Db ? zb.push(b.center) : b.start + b.size > n && n <= rb.end && (n = b.start, zb.push(n), 
                    n += pb, n > rb.end && n < rb.end + pb && zb.push(rb.end));
                }); else for (;n - pb < rb.end; ) zb.push(n), n += pb;
                if (xb[0] && d !== zb.length) {
                    for (d = 0; d < zb.length; d++) h += kb.pageBuilder.call(lb, d);
                    yb = xb.html(h).children(), yb.eq(Cb.activePage).addClass(kb.activeClass);
                }
            }
            Cb.slideeSize = qb, Cb.frameSize = pb, Cb.sbSize = ub, Cb.handleSize = vb, Fb ? (lb.initialized ? (Cb.activeItem >= Bb.length || 0 === c && 0 < Bb.length) && J(Cb.activeItem >= Bb.length ? Bb.length - 1 : 0, !c) : (J(kb.startAt), 
            lb[Eb ? "toCenter" : "toStart"](kb.startAt)), C(Eb && Bb.length ? Bb[Cb.activeItem].center : k(rb.dest, rb.start, rb.end))) : lb.initialized ? C(k(rb.dest, rb.start, rb.end)) : C(kb.startAt, 1), 
            bb("load");
        }
        function C(b, c, d) {
            if (Fb && Tb.released && !d) {
                d = K(b);
                var e = b > rb.start && b < rb.end;
                Eb ? (e && (b = Bb[d.centerItem].center), Db && kb.activateMiddle && J(d.centerItem)) : e && (b = Bb[d.firstItem].start);
            }
            Tb.init && Tb.slidee && kb.elasticBounds ? b > rb.end ? b = rb.end + (b - rb.end) / 6 : b < rb.start && (b = rb.start + (b - rb.start) / 6) : b = k(b, rb.start, rb.end), 
            cb = +new Date(), db = 0, eb = rb.cur, fb = b, gb = b - rb.cur, hb = Tb.tweese || Tb.init && !Tb.slidee, 
            ib = !hb && (c || Tb.init && Tb.slidee || !kb.speed), Tb.tweese = 0, b !== rb.dest && (rb.dest = b, 
            bb("change"), Vb || D()), Tb.released && !lb.isPaused && lb.resume(), a.extend(Cb, K(void 0)), 
            L(), yb[0] && Pb.page !== Cb.activePage && (Pb.page = Cb.activePage, yb.removeClass(kb.activeClass).eq(Cb.activePage).addClass(kb.activeClass), 
            bb("activePage", Pb.page));
        }
        function D() {
            Vb ? (ib ? rb.cur = fb : hb ? (jb = fb - rb.cur, .1 > Math.abs(jb) ? rb.cur = fb : rb.cur += jb * (Tb.released ? kb.swingSpeed : kb.syncSpeed)) : (db = Math.min(+new Date() - cb, kb.speed), 
            rb.cur = eb + gb * jQuery.easing[kb.easing](db / kb.speed, db, 0, 1, kb.speed)), 
            fb === rb.cur ? (rb.cur = fb, Tb.tweese = Vb = 0) : Vb = q(D), bb("move"), mb || (l ? ob[0].style[l] = m + (kb.horizontal ? "translateX" : "translateY") + "(" + -rb.cur + "px)" : ob[0].style[kb.horizontal ? "left" : "top"] = -Math.round(rb.cur) + "px"), 
            !Vb && Tb.released && bb("moveEnd"), E()) : (Vb = q(D), Tb.released && bb("moveStart"));
        }
        function E() {
            tb.length && (wb.cur = rb.start === rb.end ? 0 : ((Tb.init && !Tb.slidee ? rb.dest : rb.cur) - rb.start) / (rb.end - rb.start) * wb.end, 
            wb.cur = k(Math.round(wb.cur), wb.start, wb.end), Pb.hPos !== wb.cur && (Pb.hPos = wb.cur, 
            l ? tb[0].style[l] = m + (kb.horizontal ? "translateX" : "translateY") + "(" + wb.cur + "px)" : tb[0].style[kb.horizontal ? "left" : "top"] = wb.cur + "px"));
        }
        function F() {
            Sb.speed && rb.cur !== (0 < Sb.speed ? rb.end : rb.start) || lb.stop(), Yb = Tb.init ? q(F) : 0, 
            Sb.now = +new Date(), Sb.pos = rb.cur + (Sb.now - Sb.lastTime) / 1e3 * Sb.speed, 
            C(Tb.init ? Sb.pos : Math.round(Sb.pos)), Tb.init || rb.cur !== rb.dest || bb("moveEnd"), 
            Sb.lastTime = Sb.now;
        }
        function G(a, b, d) {
            "boolean" === e(b) && (d = b, b = c), b === c ? C(rb[a], d) : Eb && "center" !== a || (b = lb.getPos(b)) && C(b[a], d, !Eb);
        }
        function H(a) {
            return null != a ? i(a) ? a >= 0 && a < Bb.length ? a : -1 : Ab.index(a) : -1;
        }
        function I(a) {
            return H(i(a) && 0 > a ? a + Bb.length : a);
        }
        function J(a, b) {
            var c = H(a);
            return !Fb || 0 > c ? !1 : ((Pb.active !== c || b) && (Ab.eq(Cb.activeItem).removeClass(kb.activeClass), 
            Ab.eq(c).addClass(kb.activeClass), Pb.active = Cb.activeItem = c, L(), bb("active", c)), 
            c);
        }
        function K(a) {
            a = k(i(a) ? a : rb.dest, rb.start, rb.end);
            var b = {}, c = Db ? 0 : pb / 2;
            if (!mb) for (var d = 0, e = zb.length; e > d; d++) {
                if (a >= rb.end || d === zb.length - 1) {
                    b.activePage = zb.length - 1;
                    break;
                }
                if (a <= zb[d] + c) {
                    b.activePage = d;
                    break;
                }
            }
            if (Fb) {
                for (var e = d = c = !1, f = 0, g = Bb.length; g > f; f++) if (!1 === c && a <= Bb[f].start + Bb[f].half && (c = f), 
                !1 === e && a <= Bb[f].center + Bb[f].half && (e = f), f === g - 1 || a <= Bb[f].end + Bb[f].half) {
                    d = f;
                    break;
                }
                b.firstItem = i(c) ? c : 0, b.centerItem = i(e) ? e : b.firstItem, b.lastItem = i(d) ? d : b.centerItem;
            }
            return b;
        }
        function L() {
            var a = rb.dest <= rb.start, b = rb.dest >= rb.end, c = a ? 1 : b ? 2 : 3;
            Pb.slideePosState !== c && (Pb.slideePosState = c, Mb.is("button,input") && Mb.prop("disabled", a), 
            Nb.is("button,input") && Nb.prop("disabled", b), Mb.add(Jb)[a ? "addClass" : "removeClass"](kb.disabledClass), 
            Nb.add(Ib)[b ? "addClass" : "removeClass"](kb.disabledClass)), Pb.fwdbwdState !== c && Tb.released && (Pb.fwdbwdState = c, 
            Jb.is("button,input") && Jb.prop("disabled", a), Ib.is("button,input") && Ib.prop("disabled", b)), 
            Fb && (a = 0 === Cb.activeItem, b = Cb.activeItem >= Bb.length - 1, c = a ? 1 : b ? 2 : 3, 
            Pb.itemsButtonState !== c && (Pb.itemsButtonState = c, Kb.is("button,input") && Kb.prop("disabled", a), 
            Lb.is("button,input") && Lb.prop("disabled", b), Kb[a ? "addClass" : "removeClass"](kb.disabledClass), 
            Lb[b ? "addClass" : "removeClass"](kb.disabledClass)));
        }
        function M(a, b, c) {
            if (a = I(a), b = I(b), a > -1 && b > -1 && a !== b && !(c && b === a - 1 || !c && b === a + 1)) {
                Ab.eq(a)[c ? "insertAfter" : "insertBefore"](Bb[b].el);
                var d = b > a ? a : c ? b : b - 1, e = a > b ? a : c ? b + 1 : b, f = a > b;
                a === Cb.activeItem ? Pb.active = Cb.activeItem = c ? f ? b + 1 : b : f ? b : b - 1 : Cb.activeItem > d && Cb.activeItem < e && (Pb.active = Cb.activeItem += f ? 1 : -1), 
                B();
            }
        }
        function N(a, b) {
            for (var c = 0, d = Ob[a].length; d > c; c++) if (Ob[a][c] === b) return c;
            return -1;
        }
        function O(a) {
            return Math.round(k(a, wb.start, wb.end) / wb.end * (rb.end - rb.start)) + rb.start;
        }
        function P() {
            Tb.history[0] = Tb.history[1], Tb.history[1] = Tb.history[2], Tb.history[2] = Tb.history[3], 
            Tb.history[3] = Tb.delta;
        }
        function Q(a) {
            Tb.released = 0, Tb.source = a, Tb.slidee = "slidee" === a;
        }
        function R(b) {
            if (!(Tb.init || ~a.inArray(b.target.nodeName, x) || a(b.target).is(kb.interactive))) {
                var c = "touchstart" === b.type, d = b.data.source, e = "slidee" === d;
                ("handle" !== d || kb.dragHandle && wb.start !== wb.end) && (!e || (c ? kb.touchDragging : kb.mouseDragging && 2 > b.which)) && (c || f(b, 1), 
                Q(d), Tb.init = 1, Tb.$source = a(b.target), Tb.touch = c, Tb.pointer = c ? b.originalEvent.touches[0] : b, 
                Tb.initX = Tb.pointer.pageX, Tb.initY = Tb.pointer.pageY, Tb.initPos = e ? rb.cur : wb.cur, 
                Tb.start = +new Date(), Tb.time = 0, Tb.path = 0, Tb.delta = 0, Tb.locked = 0, Tb.history = [ 0, 0, 0, 0 ], 
                Tb.pathToLock = e ? c ? 30 : 10 : 0, Tb.initLoc = Tb[kb.horizontal ? "initX" : "initY"], 
                Tb.deltaMin = e ? -Tb.initLoc : -wb.cur, Tb.deltaMax = e ? document[kb.horizontal ? "width" : "height"] - Tb.initLoc : wb.end - wb.cur, 
                r.on(c ? u : t, S), lb.pause(1), (e ? ob : tb).addClass(kb.draggedClass), bb("moveStart"), 
                e && (Wb = setInterval(P, 10)));
            }
        }
        function S(a) {
            Tb.released = "mouseup" === a.type || "touchend" === a.type, Tb.pointer = Tb.touch ? a.originalEvent[Tb.released ? "changedTouches" : "touches"][0] : a, 
            Tb.pathX = Tb.pointer.pageX - Tb.initX, Tb.pathY = Tb.pointer.pageY - Tb.initY, 
            Tb.path = Math.sqrt(Math.pow(Tb.pathX, 2) + Math.pow(Tb.pathY, 2)), Tb.delta = k(kb.horizontal ? Tb.pathX : Tb.pathY, Tb.deltaMin, Tb.deltaMax), 
            !Tb.locked && Tb.path > Tb.pathToLock && (Tb.locked = 1, (kb.horizontal ? Math.abs(Tb.pathX) < Math.abs(Tb.pathY) : Math.abs(Tb.pathX) > Math.abs(Tb.pathY)) ? Tb.released = 1 : Tb.slidee && Tb.$source.on(v, g)), 
            Tb.released ? (Tb.touch || f(a), T(), kb.releaseSwing && Tb.slidee && (Tb.swing = 300 * ((Tb.delta - Tb.history[0]) / 40), 
            Tb.delta += Tb.swing, Tb.tweese = 10 < Math.abs(Tb.swing))) : !Tb.locked && Tb.touch || f(a), 
            C(Tb.slidee ? Math.round(Tb.initPos - Tb.delta) : O(Tb.initPos + Tb.delta));
        }
        function T() {
            clearInterval(Wb), r.off(Tb.touch ? u : t, S), (Tb.slidee ? ob : tb).removeClass(kb.draggedClass), 
            setTimeout(function() {
                Tb.$source.off(v, g);
            }), lb.resume(1), rb.cur === rb.dest && Tb.init && bb("moveEnd"), Tb.init = 0;
        }
        function U() {
            lb.stop(), r.off("mouseup", U);
        }
        function V(a) {
            switch (f(a), this) {
              case Ib[0]:
              case Jb[0]:
                lb.moveBy(Ib.is(this) ? kb.moveBy : -kb.moveBy), r.on("mouseup", U);
                break;

              case Kb[0]:
                lb.prev();
                break;

              case Lb[0]:
                lb.next();
                break;

              case Mb[0]:
                lb.prevPage();
                break;

              case Nb[0]:
                lb.nextPage();
            }
        }
        function W(a) {
            return Ub.curDelta = a.wheelDelta ? -a.wheelDelta / 120 : (a.detail || a.deltaY) / 3, 
            Fb ? (n = +new Date(), Ub.last < n - Ub.resetTime && (Ub.delta = 0), Ub.last = n, 
            Ub.delta += Ub.curDelta, 1 > Math.abs(Ub.delta) ? Ub.finalDelta = 0 : (Ub.finalDelta = Math.round(Ub.delta / 1), 
            Ub.delta %= 1), Ub.finalDelta) : Ub.curDelta;
        }
        function X(a) {
            kb.scrollBy && rb.start !== rb.end && (f(a, 1), lb.slideBy(kb.scrollBy * W(a.originalEvent)));
        }
        function Y(a) {
            kb.clickBar && a.target === sb[0] && (f(a), C(O((kb.horizontal ? a.pageX - sb.offset().left : a.pageY - sb.offset().top) - vb / 2)));
        }
        function Z(a) {
            if (kb.keyboardNavBy) switch (a.which) {
              case kb.horizontal ? 37 : 38:
                f(a), lb["pages" === kb.keyboardNavBy ? "prevPage" : "prev"]();
                break;

              case kb.horizontal ? 39 : 40:
                f(a), lb["pages" === kb.keyboardNavBy ? "nextPage" : "next"]();
            }
        }
        function $(b) {
            ~a.inArray(this.nodeName, x) || a(this).is(kb.interactive) ? b.stopPropagation() : this.parentNode === ob[0] && lb.activate(this);
        }
        function _() {
            this.parentNode === xb[0] && lb.activatePage(yb.index(this));
        }
        function ab(a) {
            kb.pauseOnHover && lb["mouseenter" === a.type ? "pause" : "resume"](2);
        }
        function bb(a, b) {
            if (Ob[a]) {
                for (Rb = Ob[a].length, Qb = y.length = 0; Rb > Qb; Qb++) y.push(Ob[a][Qb]);
                for (Qb = 0; Rb > Qb; Qb++) y[Qb].call(lb, a, b);
            }
        }
        var cb, db, eb, fb, gb, hb, ib, jb, kb = a.extend({}, d.defaults, z), lb = this, mb = i(b), nb = a(b), ob = nb.children().eq(0), pb = 0, qb = 0, rb = {
            start: 0,
            center: 0,
            end: 0,
            cur: 0,
            dest: 0
        }, sb = a(kb.scrollBar).eq(0), tb = sb.children().eq(0), ub = 0, vb = 0, wb = {
            start: 0,
            end: 0,
            cur: 0
        }, xb = a(kb.pagesBar), yb = 0, zb = [], Ab = 0, Bb = [], Cb = {
            firstItem: 0,
            lastItem: 0,
            centerItem: 0,
            activeItem: -1,
            activePage: 0
        };
        z = "basic" === kb.itemNav;
        var Db = "forceCentered" === kb.itemNav, Eb = "centered" === kb.itemNav || Db, Fb = !mb && (z || Eb || Db), Gb = kb.scrollSource ? a(kb.scrollSource) : nb, Hb = kb.dragSource ? a(kb.dragSource) : nb, Ib = a(kb.forward), Jb = a(kb.backward), Kb = a(kb.prev), Lb = a(kb.next), Mb = a(kb.prevPage), Nb = a(kb.nextPage), Ob = {}, Pb = {};
        jb = ib = hb = gb = fb = eb = db = cb = void 0;
        var Qb, Rb, Sb = {}, Tb = {
            released: 1
        }, Ub = {
            last: 0,
            delta: 0,
            resetTime: 200
        }, Vb = 0, Wb = 0, Xb = 0, Yb = 0;
        mb || (b = nb[0]), lb.initialized = 0, lb.frame = b, lb.slidee = ob[0], lb.pos = rb, 
        lb.rel = Cb, lb.items = Bb, lb.pages = zb, lb.isPaused = 0, lb.options = kb, lb.dragging = Tb, 
        lb.reload = B, lb.getPos = function(a) {
            if (Fb) return a = H(a), -1 !== a ? Bb[a] : !1;
            var b = ob.find(a).eq(0);
            return b[0] ? (a = kb.horizontal ? b.offset().left - ob.offset().left : b.offset().top - ob.offset().top, 
            b = b[kb.horizontal ? "outerWidth" : "outerHeight"](), {
                start: a,
                center: a - pb / 2 + b / 2,
                end: a - pb + b,
                size: b
            }) : !1;
        }, lb.moveBy = function(a) {
            Sb.speed = a, !Tb.init && Sb.speed && rb.cur !== (0 < Sb.speed ? rb.end : rb.start) && (Sb.lastTime = +new Date(), 
            Sb.startPos = rb.cur, Q("button"), Tb.init = 1, bb("moveStart"), p(Yb), F());
        }, lb.stop = function() {
            "button" === Tb.source && (Tb.init = 0, Tb.released = 1);
        }, lb.prev = function() {
            lb.activate(Cb.activeItem - 1);
        }, lb.next = function() {
            lb.activate(Cb.activeItem + 1);
        }, lb.prevPage = function() {
            lb.activatePage(Cb.activePage - 1);
        }, lb.nextPage = function() {
            lb.activatePage(Cb.activePage + 1);
        }, lb.slideBy = function(a, b) {
            a && (Fb ? lb[Eb ? "toCenter" : "toStart"](k((Eb ? Cb.centerItem : Cb.firstItem) + kb.scrollBy * a, 0, Bb.length)) : C(rb.dest + a, b));
        }, lb.slideTo = function(a, b) {
            C(a, b);
        }, lb.toStart = function(a, b) {
            G("start", a, b);
        }, lb.toEnd = function(a, b) {
            G("end", a, b);
        }, lb.toCenter = function(a, b) {
            G("center", a, b);
        }, lb.getIndex = H, lb.activate = function(a, b) {
            var c = J(a);
            kb.smart && !1 !== c && (Eb ? lb.toCenter(c, b) : c >= Cb.lastItem ? lb.toStart(c, b) : c <= Cb.firstItem ? lb.toEnd(c, b) : Tb.released && !lb.isPaused && lb.resume());
        }, lb.activatePage = function(a, b) {
            i(a) && C(zb[k(a, 0, zb.length - 1)], b);
        }, lb.resume = function(a) {
            !kb.cycleBy || !kb.cycleInterval || "items" === kb.cycleBy && !Bb[0] || a < lb.isPaused || (lb.isPaused = 0, 
            Xb ? Xb = clearTimeout(Xb) : bb("resume"), Xb = setTimeout(function() {
                switch (bb("cycle"), kb.cycleBy) {
                  case "items":
                    lb.activate(Cb.activeItem >= Bb.length - 1 ? 0 : Cb.activeItem + 1);
                    break;

                  case "pages":
                    lb.activatePage(Cb.activePage >= zb.length - 1 ? 0 : Cb.activePage + 1);
                }
            }, kb.cycleInterval));
        }, lb.pause = function(a) {
            a < lb.isPaused || (lb.isPaused = a || 100, Xb && (Xb = clearTimeout(Xb), bb("pause")));
        }, lb.toggle = function() {
            lb[Xb ? "pause" : "resume"]();
        }, lb.set = function(b, c) {
            a.isPlainObject(b) ? a.extend(kb, b) : kb.hasOwnProperty(b) && (kb[b] = c);
        }, lb.add = function(b, c) {
            var d = a(b);
            Fb ? (null != c && Bb[0] ? Bb.length && d.insertBefore(Bb[c].el) : d.appendTo(ob), 
            c <= Cb.activeItem && (Pb.active = Cb.activeItem += d.length)) : ob.append(d), B();
        }, lb.remove = function(b) {
            if (Fb) {
                if (b = I(b), b > -1) {
                    Ab.eq(b).remove();
                    var c = b === Cb.activeItem;
                    b < Cb.activeItem && (Pb.active = --Cb.activeItem), B(), c && (Pb.active = null, 
                    lb.activate(Cb.activeItem));
                }
            } else a(b).remove(), B();
        }, lb.moveAfter = function(a, b) {
            M(a, b, 1);
        }, lb.moveBefore = function(a, b) {
            M(a, b);
        }, lb.on = function(a, b) {
            if ("object" === e(a)) for (var c in a) a.hasOwnProperty(c) && lb.on(c, a[c]); else if ("function" === e(b)) {
                c = a.split(" ");
                for (var d = 0, f = c.length; f > d; d++) Ob[c[d]] = Ob[c[d]] || [], -1 === N(c[d], b) && Ob[c[d]].push(b);
            } else if ("array" === e(b)) for (c = 0, d = b.length; d > c; c++) lb.on(a, b[c]);
        }, lb.one = function(a, b) {
            function c() {
                b.apply(lb, arguments), lb.off(a, c);
            }
            lb.on(a, c);
        }, lb.off = function(a, b) {
            if (b instanceof Array) for (var c = 0, d = b.length; d > c; c++) lb.off(a, b[c]); else for (var c = a.split(" "), d = 0, e = c.length; e > d; d++) if (Ob[c[d]] = Ob[c[d]] || [], 
            null == b) Ob[c[d]].length = 0; else {
                var f = N(c[d], b);
                -1 !== f && Ob[c[d]].splice(f, 1);
            }
        }, lb.destroy = function() {
            return r.add(Gb).add(tb).add(sb).add(xb).add(Ib).add(Jb).add(Kb).add(Lb).add(Mb).add(Nb).unbind("." + o), 
            Kb.add(Lb).add(Mb).add(Nb).removeClass(kb.disabledClass), Ab && Ab.eq(Cb.activeItem).removeClass(kb.activeClass), 
            xb.empty(), mb || (nb.unbind("." + o), ob.add(tb).css(l || (kb.horizontal ? "left" : "top"), l ? "none" : 0), 
            a.removeData(b, o)), Bb.length = zb.length = 0, Pb = {}, lb.initialized = 0, lb;
        }, lb.init = function() {
            if (!lb.initialized) {
                lb.on(A);
                var a = tb;
                return mb || (a = a.add(ob), nb.css("overflow", "hidden"), l || "static" !== nb.css("position") || nb.css("position", "relative")), 
                l ? m && a.css(l, m) : ("static" === sb.css("position") && sb.css("position", "relative"), 
                a.css({
                    position: "absolute"
                })), kb.forward && Ib.on(w, V), kb.backward && Jb.on(w, V), kb.prev && Kb.on(v, V), 
                kb.next && Lb.on(v, V), kb.prevPage && Mb.on(v, V), kb.nextPage && Nb.on(v, V), 
                Gb.on("DOMMouseScroll." + o + " mousewheel." + o, X), sb[0] && sb.on(v, Y), Fb && kb.activateOn && nb.on(kb.activateOn + "." + o, "*", $), 
                xb[0] && kb.activatePageOn && xb.on(kb.activatePageOn + "." + o, "*", _), Hb.on(s, {
                    source: "slidee"
                }, R), tb && tb.on(s, {
                    source: "handle"
                }, R), r.bind("keydown." + o, Z), mb || (nb.on("mouseenter." + o + " mouseleave." + o, ab), 
                nb.on("scroll." + o, h)), B(), kb.cycleBy && !mb && lb[kb.startPaused ? "pause" : "resume"](), 
                lb.initialized = 1, lb;
            }
        };
    }
    function e(a) {
        return null == a ? String(a) : "object" == typeof a || "function" == typeof a ? Object.prototype.toString.call(a).match(/\s([a-z]+)/i)[1].toLowerCase() || "object" : typeof a;
    }
    function f(a, b) {
        a.preventDefault(), b && a.stopPropagation();
    }
    function g(b) {
        f(b, 1), a(this).off(b.type, g);
    }
    function h() {
        this.scrollTop = this.scrollLeft = 0;
    }
    function i(a) {
        return !isNaN(parseFloat(a)) && isFinite(a);
    }
    function j(a, b) {
        return 0 | Math.round(String(a.css(b)).replace(/[^\-0-9.]/g, ""));
    }
    function k(a, b, c) {
        return b > a ? b : a > c ? c : a;
    }
    var l, m, n, o = "sly", p = b.cancelAnimationFrame || b.cancelRequestAnimationFrame, q = b.requestAnimationFrame, r = a(document), s = "touchstart." + o + " mousedown." + o, t = "mousemove." + o + " mouseup." + o, u = "touchmove." + o + " touchend." + o, v = "click." + o, w = "mousedown." + o, x = [ "INPUT", "SELECT", "BUTTON", "TEXTAREA" ], y = [];
    !function(a) {
        for (var b = [ "moz", "webkit", "o" ], c = 0, d = 0, e = b.length; e > d && !p; ++d) q = (p = a[b[d] + "CancelAnimationFrame"] || a[b[d] + "CancelRequestAnimationFrame"]) && a[b[d] + "RequestAnimationFrame"];
        p || (q = function(b) {
            var d = +new Date(), e = Math.max(0, 16 - (d - c));
            return c = d + e, a.setTimeout(function() {
                b(d + e);
            }, e);
        }, p = function(a) {
            clearTimeout(a);
        });
    }(window), function() {
        function a(a) {
            for (var d = 0, e = b.length; e > d; d++) {
                var f = b[d] ? b[d] + a.charAt(0).toUpperCase() + a.slice(1) : a;
                if (null != c.style[f]) return f;
            }
        }
        var b = [ "", "webkit", "moz", "ms", "o" ], c = document.createElement("div");
        l = a("transform"), m = a("perspective") ? "translateZ(0) " : "";
    }(), b.Sly = d, a.fn.sly = function(b, c) {
        var f, g;
        return a.isPlainObject(b) || (("string" === e(b) || !1 === b) && (f = !1 === b ? "destroy" : b, 
        g = Array.prototype.slice.call(arguments, 1)), b = {}), this.each(function(e, h) {
            var i = a.data(h, o);
            i || f ? i && f && i[f] && i[f].apply(i, g) : a.data(h, o, new d(h, b, c).init());
        });
    }, d.defaults = {
        horizontal: 0,
        itemNav: null,
        itemSelector: null,
        smart: 0,
        activateOn: null,
        activateMiddle: 0,
        scrollSource: null,
        scrollBy: 0,
        dragSource: null,
        mouseDragging: 0,
        touchDragging: 0,
        releaseSwing: 0,
        swingSpeed: .2,
        elasticBounds: 0,
        interactive: null,
        scrollBar: null,
        dragHandle: 0,
        dynamicHandle: 0,
        minHandleSize: 50,
        clickBar: 0,
        syncSpeed: .5,
        pagesBar: null,
        activatePageOn: null,
        pageBuilder: function(a) {
            return "<li>" + (a + 1) + "</li>";
        },
        forward: null,
        backward: null,
        prev: null,
        next: null,
        prevPage: null,
        nextPage: null,
        cycleBy: null,
        cycleInterval: 5e3,
        pauseOnHover: 0,
        startPaused: 0,
        moveBy: 300,
        speed: 0,
        easing: "swing",
        startAt: 0,
        keyboardNavBy: null,
        draggedClass: "dragged",
        activeClass: "active",
        disabledClass: "disabled"
    };
}(jQuery, window), function(a) {
    "use strict";
    "function" == typeof define && define.amd ? define(a) : "undefined" != typeof module && "undefined" != typeof module.exports ? module.exports = a() : window.Sortable = a();
}(function() {
    "use strict";
    function a(a, c) {
        this.el = a, this.options = c = c || {};
        var d = {
            group: Math.random(),
            store: null,
            handle: null,
            draggable: a.children[0] && a.children[0].nodeName || (/[uo]l/i.test(a.nodeName) ? "li" : "*"),
            ghostClass: "sortable-ghost",
            ignore: "a, img",
            filter: null
        };
        for (var f in d) c[f] = c[f] || d[f];
        F.forEach(function(d) {
            c[d] = b(this, c[d] || G), e(a, d.substr(2).toLowerCase(), c[d]);
        }), a[x] = c.group;
        for (var g in this) "_" === g.charAt(0) && (this[g] = b(this, this[g]));
        e(a, "mousedown", this._onTapStart), e(a, "touchstart", this._onTapStart), B && e(a, "selectstart", this._onTapStart), 
        e(a, "dragover", this._onDragOver), e(a, "dragenter", this._onDragOver), I.push(this._onDragOver), 
        c.store && this.sort(c.store.get(this));
    }
    function b(a, b) {
        var c = H.call(arguments, 2);
        return b.bind ? b.bind.apply(b, [ a ].concat(c)) : function() {
            return b.apply(a, c.concat(H.call(arguments)));
        };
    }
    function c(a, b, c) {
        if ("*" === b) return a;
        if (a) {
            c = c || z, b = b.split(".");
            var d = b.shift().toUpperCase(), e = new RegExp("\\s(" + b.join("|") + ")\\s", "g");
            do if (!("" !== d && a.nodeName != d || b.length && ((" " + a.className + " ").match(e) || []).length != b.length)) return a; while (a !== c && (a = a.parentNode));
        }
        return null;
    }
    function d(a) {
        a.dataTransfer.dropEffect = "move", a.preventDefault();
    }
    function e(a, b, c) {
        a.addEventListener(b, c, !1);
    }
    function f(a, b, c) {
        a.removeEventListener(b, c, !1);
    }
    function g(a, b, c) {
        if (a) if (a.classList) a.classList[c ? "add" : "remove"](b); else {
            var d = (" " + a.className + " ").replace(/\s+/g, " ").replace(" " + b + " ", "");
            a.className = d + (c ? " " + b : "");
        }
    }
    function h(a, b, c) {
        if (a && a.style) {
            if (void 0 === c) return z.defaultView && z.defaultView.getComputedStyle ? c = z.defaultView.getComputedStyle(a, "") : a.currentStyle && (c = a.currentStyle), 
            void 0 === b ? c : c[b];
            a.style[b] = c + ("string" == typeof c ? "" : "px");
        }
    }
    function i(a, b, c) {
        if (a) {
            var d = a.getElementsByTagName(b), e = 0, f = d.length;
            if (c) for (;f > e; e++) c(d[e], e);
            return d;
        }
        return [];
    }
    function j(a) {
        return a.draggable = !1;
    }
    function k() {
        C = !1;
    }
    function l(a, b) {
        var c = a.lastElementChild.getBoundingClientRect();
        return b.clientY - (c.top + c.height) > 5;
    }
    function m(a) {
        for (var b = a.innerHTML + a.className + a.src, c = b.length, d = 0; c--; ) d += b.charCodeAt(c);
        return d.toString(36);
    }
    var n, o, p, q, r, s, t, u, v, w, x = "Sortable" + new Date().getTime(), y = window, z = y.document, A = y.parseInt, B = !!z.createElement("div").dragDrop, C = !1, D = function(a, b) {
        var c = z.createEvent("Event");
        return c.initEvent(a, !0, !0), c.item = b, c;
    }, E = function(a, b, c) {
        a.dispatchEvent(D(b, c || a));
    }, F = "onAdd onUpdate onRemove onStart onEnd onFilter".split(" "), G = function() {}, H = [].slice, I = [];
    return a.prototype = {
        constructor: a,
        _applyEffects: function() {
            g(n, this.options.ghostClass, !0);
        },
        _onTapStart: function(a) {
            var b = a.touches && a.touches[0], f = (b || a).target, g = this.options, h = this.el, k = g.filter;
            if ("function" == typeof k && k.call(this, f, this)) return void E(h, "filter", f);
            if (k && (k = k.split(",").filter(function(a) {
                return c(f, a.trim(), h);
            }), k.length)) return void E(h, "filter", f);
            if (g.handle && (f = c(f, g.handle, h)), f = c(f, g.draggable, h), f && "selectstart" == a.type && "A" != f.tagName && "IMG" != f.tagName && f.dragDrop(), 
            f && !n && f.parentNode === h) {
                v = a, p = this.el, n = f, q = n.nextSibling, u = this.options.group, n.draggable = !0, 
                g.ignore.split(",").forEach(function(a) {
                    i(f, a.trim(), j);
                }), b && (v = {
                    target: f,
                    clientX: b.clientX,
                    clientY: b.clientY
                }, this._onDragStart(v, !0), a.preventDefault()), e(z, "mouseup", this._onDrop), 
                e(z, "touchend", this._onDrop), e(z, "touchcancel", this._onDrop), e(this.el, "dragstart", this._onDragStart), 
                e(this.el, "dragend", this._onDrop), e(z, "dragover", d);
                try {
                    z.selection ? z.selection.empty() : window.getSelection().removeAllRanges();
                } catch (l) {}
                E(n, "start");
            }
        },
        _emulateDragOver: function() {
            if (w) {
                h(o, "display", "none");
                var a = z.elementFromPoint(w.clientX, w.clientY), b = a, c = this.options.group, d = I.length;
                if (b) do {
                    if (b[x] === c) {
                        for (;d--; ) I[d]({
                            clientX: w.clientX,
                            clientY: w.clientY,
                            target: a,
                            rootEl: b
                        });
                        break;
                    }
                    a = b;
                } while (b = b.parentNode);
                h(o, "display", "");
            }
        },
        _onTouchMove: function(a) {
            if (v) {
                var b = a.touches[0], c = b.clientX - v.clientX, d = b.clientY - v.clientY, e = "translate3d(" + c + "px," + d + "px,0)";
                w = b, h(o, "webkitTransform", e), h(o, "mozTransform", e), h(o, "msTransform", e), 
                h(o, "transform", e), a.preventDefault();
            }
        },
        _onDragStart: function(a, b) {
            var c = a.dataTransfer;
            if (this._offUpEvents(), b) {
                var d, f = n.getBoundingClientRect(), g = h(n);
                o = n.cloneNode(!0), h(o, "top", f.top - A(g.marginTop, 10)), h(o, "left", f.left - A(g.marginLeft, 10)), 
                h(o, "width", f.width), h(o, "height", f.height), h(o, "opacity", "0.8"), h(o, "position", "fixed"), 
                h(o, "zIndex", "100000"), p.appendChild(o), d = o.getBoundingClientRect(), h(o, "width", 2 * f.width - d.width), 
                h(o, "height", 2 * f.height - d.height), e(z, "touchmove", this._onTouchMove), e(z, "touchend", this._onDrop), 
                e(z, "touchcancel", this._onDrop), this._loopId = setInterval(this._emulateDragOver, 150);
            } else c.effectAllowed = "move", c.setData("Text", n.textContent), e(z, "drop", this._onDrop);
            setTimeout(this._applyEffects);
        },
        _onDragOver: function(a) {
            if (!C && u === this.options.group && (void 0 === a.rootEl || a.rootEl === this.el)) {
                var b = this.el, d = c(a.target, this.options.draggable, b);
                if (0 === b.children.length || b.children[0] === o || b === a.target && l(b, a)) b.appendChild(n); else if (d && d !== n && void 0 !== d.parentNode[x]) {
                    r !== d && (r = d, s = h(d), t = d.getBoundingClientRect());
                    var e, f = t, g = f.right - f.left, i = f.bottom - f.top, j = /left|right|inline/.test(s.cssFloat + s.display), m = d.offsetWidth > n.offsetWidth, p = d.offsetHeight > n.offsetHeight, q = (j ? (a.clientX - f.left) / g : (a.clientY - f.top) / i) > .5, v = d.nextElementSibling;
                    C = !0, setTimeout(k, 30), e = j ? d.previousElementSibling === n && !m || q && m : v !== n && !p || q && p, 
                    e && !v ? b.appendChild(n) : d.parentNode.insertBefore(n, e ? v : d);
                }
            }
        },
        _offUpEvents: function() {
            f(z, "mouseup", this._onDrop), f(z, "touchmove", this._onTouchMove), f(z, "touchend", this._onDrop), 
            f(z, "touchcancel", this._onDrop);
        },
        _onDrop: function(a) {
            clearInterval(this._loopId), f(z, "drop", this._onDrop), f(z, "dragover", d), f(this.el, "dragend", this._onDrop), 
            f(this.el, "dragstart", this._onDragStart), f(this.el, "selectstart", this._onTapStart), 
            this._offUpEvents(), a && (a.preventDefault(), a.stopPropagation(), o && o.parentNode.removeChild(o), 
            n && (j(n), g(n, this.options.ghostClass, !1), p.contains(n) ? n.nextSibling !== q && E(n, "update") : (E(p, "remove", n), 
            E(n, "add")), E(n, "end")), p = n = o = q = v = w = r = s = u = null, this.options.store && this.options.store.set(this));
        },
        toArray: function() {
            for (var a, b = [], c = this.el.children, d = 0, e = c.length; e > d; d++) a = c[d], 
            b.push(a.getAttribute("data-id") || m(a));
            return b;
        },
        sort: function(a) {
            var b = {}, c = this.el;
            this.toArray().forEach(function(a, d) {
                b[a] = c.children[d];
            }), a.forEach(function(a) {
                b[a] && (c.removeChild(b[a]), c.appendChild(b[a]));
            });
        },
        closest: function(a, b) {
            return c(a, b || this.options.draggable, this.el);
        },
        destroy: function() {
            var a = this.el, b = this.options;
            F.forEach(function(c) {
                f(a, c.substr(2).toLowerCase(), b[c]);
            }), f(a, "mousedown", this._onTapStart), f(a, "touchstart", this._onTapStart), f(a, "selectstart", this._onTapStart), 
            f(a, "dragover", this._onDragOver), f(a, "dragenter", this._onDragOver), Array.prototype.forEach.call(a.querySelectorAll("[draggable]"), function(a) {
                a.removeAttribute("draggable");
            }), I.splice(I.indexOf(this._onDragOver), 1), this._onDrop(), this.el = null;
        }
    }, a.utils = {
        on: e,
        off: f,
        css: h,
        find: i,
        bind: b,
        closest: c,
        toggleClass: g,
        createEvent: D,
        dispatchEvent: E
    }, a.version = "0.5.0", a;
}), jQuery.fn.allowDigitsOnly = function() {
    return this.each(function() {
        $(this).keydown(function(a) {
            var b = a.charCode || a.keyCode || 0;
            return 8 == b || 9 == b || 46 == b || 110 == b || 190 == b || b >= 35 && 40 >= b || b >= 48 && 57 >= b || b >= 96 && 105 >= b;
        });
    });
}, $(document).ready(function() {
    function a() {
        var a = new google.maps.LatLng(55.011644, 82.944377), b = {
            zoom: 15,
            center: a,
            panControl: !1,
            zoomControl: !1,
            mapTypeControl: !1,
            scaleControl: !1,
            streetViewControl: !1,
            overviewMapControl: !1,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }, c = new google.maps.Map(document.getElementById("map_canvas"), b), d = new google.maps.MarkerImage("/assets/images/pin.png", new google.maps.Size(44, 50), new google.maps.Point(0, 0), new google.maps.Point(14, 47)), e = new google.maps.Marker({
            map: c,
            position: a,
            visible: !0,
            icon: d
        }), f = document.createElement("div");
        f.className = f.className + " info-bubble", f.innerHTML = "<div class='info-bubble__title'>\n                            ул. Кочубея, 7/1, ТЦ КУМ\n                          </div>\n                          <div class='info-bubble__time'>\n                            Пн-пт: 9:00 – 21:00\n                            <br>\n                            Сб, вс: 10:00 – 18:00\n                          </div>\n                          <div class='info-bubble__phone'>\n                            8 (383) 207-80-80\n                          </div>\n                          <div class='info-bubble__img'>\n                            <img src='//strana.ru/media/images/uploaded/gallery_promo21099001.jpg'>\n                          </div>\n                          ";
        var g = {
            content: f,
            disableAutoPan: !1,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-125, 0),
            boxStyle: {
                width: "250px"
            },
            infoBoxClearance: new google.maps.Size(1, 1),
            pane: "floatPane",
            enableEventPropagation: !1
        }, h = new InfoBox(g);
        google.maps.event.addDomListener(h.content_, "click", function() {
            this.className.match(/(?:^|\s)active(?!\S)/) ? this.className = this.className.replace(/(?:^|\s)active(?!\S)/g, "") : this.className += " active";
        }), h.open(c, e);
    }
    function b(a) {
        var b = a.parent();
        $cnt = $(b.data("tabs")), tab, $cnt.children(".tabs__itm").eq(a.index()).addClass("active").siblings().removeClass("active"), 
        a.addClass("active").siblings().removeClass("active"), a.parent().data("inited", !0);
    }
    var c = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.offsetWidth;
    if ($(document).on("touchstart click", ".top-bar__user-panel.dropdown", function() {}), 
    $("#map_canvas").length && a(), $("#js-sortable-container1").length) {
        var d = $("#js-sortable-container1"), e = $("#js-sortable-container2"), f = d.find(".js-sortable-list"), g = e.find(".js-sortable-list"), h = $("#account-order-modal"), i = function(a, b) {
            b.children().length ? (b.removeClass("empty"), a.find(".js-sortable-helper").hide(), 
            a.find(".js-sortable-header").fadeIn(0)) : (b.addClass("empty"), a.find(".js-sortable-helper").show(), 
            a.find(".js-sortable-header").fadeOut(0)), h.show();
        };
        new Sortable(f[0], {
            draggable: ".js-sortable-item",
            group: "order-divide",
            onAdd: function() {
                i(d, f);
            },
            onRemove: function() {
                i(d, f);
            }
        }), new Sortable(g[0], {
            draggable: ".js-sortable-item",
            group: "order-divide",
            onAdd: function() {
                i(e, g);
            },
            onRemove: function() {
                i(e, g);
            }
        }), $(document).on("click", "#account-order-modal-close", function(a) {
            a.preventDefault(), h.hide();
        });
    }
    if ($(document).on("click", ".js-payment-tabs a", function(a) {
        a.preventDefault();
        var b = $($(this).attr("href")), c = $(".js-payment-tabs");
        $(this).hasClass("active") || (c.find(".active").removeClass("active"), $(this).addClass("active"), 
        b.show().siblings().hide());
    }), $("#js-delivery-selector").length) {
        var j = ($("#js-delivery-selector"), $("#js-delivery-region-wrapper")), k = $("#js-delivery-selector").val();
        j.children().hide(), j.children().eq(k).show(), $(document).on("change", "#js-delivery-selector", function(a) {
            a.preventDefault(), j.children().hide(), j.children().eq($(this).val()).show();
        });
    }
    $(".compare").sly({
        horizontal: 1,
        itemNav: "centered",
        smart: 1,
        activateOn: "click",
        mouseDragging: 1,
        touchDragging: 1,
        startAt: 1,
        scrollBy: 0,
        activatePageOn: "click",
        speed: 300,
        elasticBounds: 1,
        easing: "easeOutExpo",
        dragHandle: 1,
        dynamicHandle: 1,
        prev: $("#compare-prev"),
        next: $("#compare-next")
    }), $(".discounts-timer-holder").length && $(".discounts-timer-holder").each(function() {
        var a = $(this).data("date").split(",");
        a = new Date(a[0], a[1], a[2]), $(this).countdown({
            until: a,
            format: "HMS",
            compact: !0
        });
    }), $(".chosen").length && $(".chosen").chosen({
        width: "100%",
        disable_search_threshold: 10
    }), $(".star-rating").length && $(".star-rating").rating(), $(".digit-input").length && ($(".digit-input input").allowDigitsOnly(), 
    $(".digit-input a").click(function(a) {
        a.preventDefault();
        var b = $(this).parent().data("min-value") || 1, c = $(this).parent().data("max-value") || 5, d = parseInt($(this).data("op")), e = $(this).siblings("input").eq(0), f = parseInt(e.val()) + 1 * d;
        currentInputValue = isNaN(f) ? 1 : f, e.val(currentInputValue), currentInputValue < parseInt(b) + 1 && e.val(b), 
        currentInputValue > parseInt(c) - 1 && e.val(c);
    })), $(".js-expand__a").click(function(a) {
        a.preventDefault();
        var b = $(this).parent();
        b.hasClass("active") ? b.find(".js-expand__cnt").slideUp(250, function() {
            $(this).hide(), b.removeClass("active");
        }) : (b.find(".js-expand__cnt").show().slideUp(0).slideDown(250), b.addClass("active"));
    }), $(".js-toggle-item").click(function(a) {
        a.preventDefault();
        var b = $(this), c = b.data("item") || this, d = $(c);
        d.length && (d.hasClass("active") ? d.removeClass("active") : d.addClass("active"));
    }), $(".js-account-orders-old-dates .open").click(function(a) {
        a.preventDefault();
        var b = $(this).parent();
        b.addClass("active");
    }), $(".js-account-orders-old-dates .close").click(function(a) {
        a.preventDefault();
        var b = $(this).parent();
        b.removeClass("active");
    }), $(".js-showhide-item").click(function(a) {
        a.preventDefault();
        var b = $(this), c = $(b.data("item"));
        c.length && (c.is(":visible") ? c.hide() : c.show());
    }), $("#catalog-switcher a").click(function(a) {
        a.preventDefault(), $(this).hasClass("active") || ($(this).addClass("active").siblings().removeClass("active"), 
        $("#catalog-main").toggleClass("simple").toggleClass("detailed"));
    });
    var l = function(a) {
        (880 > a || Modernizr.touch) && ($("#catalog-main").removeClass("detailed").addClass("simple"), 
        $("#catalog-switcher-detailed").removeClass("active"), $("#catalog-switcher-simple").addClass("active"));
    };
    $("#search-mobile-close").click(function(a) {
        a.preventDefault(), $("#search-mobile").hide(), $("#search-mobile").data("active", !1);
    });
    var m = function(a) {
        1025 > a && $(document).on("click", ".js-dropdown-search", function(a) {
            a.preventDefault();
            var b = $("#search-mobile").data("active");
            b ? $("#search-mobile").submit() : $("#search-mobile").show().data("active", !0);
        });
    };
    m(c), $("#catalog-main").length && (l(), $(window).resize(function() {
        setTimeout(function() {
            var a = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.offsetWidth;
            l(a), m(a);
        }, 100);
    })), $("#catalog-filter-price-slider").length && $("#catalog-filter-price-slider").noUiSlider({
        start: [ 9e3, 1e5 ],
        step: 500,
        margin: 1e4,
        connect: !0,
        behaviour: "tap-drag",
        range: {
            min: 12e3,
            max: 13e4
        },
        serialization: {
            lower: [ $.Link({
                target: $("#catalog-filter-price-min")
            }) ],
            upper: [ $.Link({
                target: $("#catalog-filter-price-max")
            }) ],
            format: {
                decimals: 0
            }
        }
    }), $(".js-order-edit").on("click", function(a) {
        a.preventDefault(), $(this).parents(".order-panel").removeClass("complite").addClass("active");
    }), $(".js-order-next-step").on("click", function(a) {
        a.preventDefault(), $(this).parents(".order-panel").removeClass("active").addClass("complite").next().removeClass("disabled").addClass("active");
    }), $(".js-order-show-reg").on("click", function(a) {
        a.preventDefault(), $("#order-step0-reg").addClass("active").show(), $("#order-step0-login").removeClass("active").hide();
    }), $(".js-order-show-login").on("click", function(a) {
        a.preventDefault(), $("#order-step0-login").addClass("active").show(), $("#order-step0-reg").removeClass("active").hide();
    }), $("#order-delivery-city").on("click", function(a) {
        a.preventDefault(), $("#order-delivery-type").show();
    }), $(".js-order-delivery-type").on("change", function() {
        $("#order-delivery-courier").hide(), $("#order-delivery-pickup").hide(), $("#order-delivery-company").hide(), 
        $("" + $(this).val()).show();
    }), $(".order-payment-time").on("change", function() {
        $("#order-payment-now").hide(), $("#order-payment-ondelivery").hide(), $("#order-payment-credit").hide(), 
        $("" + $(this).val()).show();
    }), $(".product-faq__item__header").on("hover", function() {
        $(this).find(".button").addClass("hover");
    }), $('.js-dropdown-search input[type="text"]').on("input focus", function() {
        $.trim(this.value).length ? $(".header__search-form__results").addClass("active") : $(".header__search-form__results").removeClass("active");
    }).on("blur", function() {
        $(".header__search-form__results").removeClass("active");
    }), $("#menu__catalog").click(function(a) {
        return a.preventDefault(), $(this).hasClass("active") ? ($("#catalog-menu").slideUp(200).removeClass("active"), 
        $(this).toggleClass("active"), !1) : ($(this).toggleClass("active"), void $("#catalog-menu").show().slideUp(0).addClass("active").slideDown(200));
    }), $(".catalog-filter__item__header .button").click(function(a) {
        return a.preventDefault(), $(this).parent().parent().hasClass("active") ? ($(this).parent().next().slideUp(200).parent().removeClass("active"), 
        !1) : void $(this).parent().next().show().slideUp(0).slideDown(200).parent().addClass("active");
    }), $(".order-list__item__header .button").click(function(a) {
        return a.preventDefault(), $(this).parent().parent().hasClass("active") ? ($(this).parent().next().slideUp(200).parent().removeClass("active"), 
        !1) : void $(this).parent().next().show().slideUp(0).slideDown(200).parent().addClass("active");
    }), $("#catalog-menu .js-menu-category").click(function(a) {
        a.preventDefault();
        var b = $(this).attr("href");
        $(this).parents(".js-block-level").find(".active").removeClass("active"), $(this).parent().addClass("active"), 
        $("#catalog-menu").find(b).show().slideUp(0).slideDown(250);
    }), $(".js-tabs-switcher a").click(function(a) {
        a.preventDefault(), b($(this));
    }), $(".home-catalog__group-title a").click(function(a) {
        a.preventDefault(), $(this).parent().parent().parent().find(".home-catalog__full").show();
    }), $(".home-catalog__full__title a").click(function(a) {
        a.preventDefault(), $(this).parent().parent().hide();
    }), $(".home-catalog__item ~ .home-catalog__item").hover(function() {
        $(this).parent().addClass("hover");
    }, function() {
        $(this).parent().removeClass("hover");
    });
});