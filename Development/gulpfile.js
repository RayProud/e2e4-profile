
var gulp = require('gulp');
var styl = require('gulp-stylus');
var connect = require('gulp-connect');

gulp.task('connect', function() {
  connect.server({
    root: '../Production/',
    livereload: true,
    port: 2334
  });
});

gulp.task('styl', function () {
    gulp.src('./styl/common.styl')
        .pipe(styl())
        .pipe(gulp.dest('../Production/assets/styles/'))
        .pipe(connect.reload());
});


gulp.task('watch', function() {
  gulp.watch('./styl/*.styl', ['styl']);
  gulp.watch('../Production/*.html', ['styl']);
});

gulp.task('default', ['styl', 'connect', 'watch']);

